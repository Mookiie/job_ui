<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  // login

  $lang['confirm'] = 'ยืนยันตัวตนเรียบร้อย';
  $lang['setpassword'] = 'เปลี่ยนรหัสผ่านเพื่อเข้าสู่ระบบ';
  $lang['password'] = 'รหัสผ่าน';
  $lang['change_password'] = 'เปลี่ยนรหัสผ่าน';
  $lang['fname'] = 'ชื่อ';
  $lang['lname'] = 'สกุล';
  $lang['tel'] = 'เบอร์โทรศัพท์';
  $lang['email'] = 'อีเมล์';
  $lang['citizen'] = 'เลขประจำตัวประชาชน';
  $lang["mobile"] ="เบอร์มือถือ";
?>
