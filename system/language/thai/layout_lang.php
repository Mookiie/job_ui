<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  // GLOBAL
  $lang['wait'] = 'กรุณารอสักครู่';
  $lang['signout'] = 'ออกจากระบบ';
  $lang['editApplicant'] = 'แก้ไขข้อมูลส่วนตัว';
  $lang['account'] = 'บัญชีผู้ใช้';
  $lang['load'] = "กำลังโหลดข้อมูล";
  $lang['dismiss'] = "ปฏิเสธ";
  $lang['edit'] = "แก้ไข";
  $lang['close'] = "ปิด";
  $lang['copy'] = "คัดลอก";

  // login
  $lang['signin'] = 'ลงชื่อเข้าระบบ';
  $lang['email'] = 'อีเมล์';
  $lang['email_request'] = 'กรุณากรอกอีเมล์';
  $lang['password'] = 'รหัสผ่าน';
  $lang['password_request'] = 'กรุณากรอกรหัสผ่าน';
  $lang['signup'] = 'ลงทะเบียน';
  $lang['invite'] = 'เชิญผู้ใช้';
  $lang['noaccount'] = 'คุณยังไม่มีชื่อผู้ใช้?';//Don't have an account?
  $lang['forget'] = 'ลืมรหัสผ่าน?';
  $lang['login'] = 'เข้าสู่ระบบ';
  $lang['remember'] = 'จดจำฉัน';

  // Nav
  $lang['nav_homepage'] = 'หน้าหลัก';
  $lang['nav_announcepage'] = 'แผงควบคุม';
  $lang['nav_announceMepage'] = 'ประกาศรับสมัครงาน';
  $lang['nav_administratorpage'] = 'จัดการบริษัท';
  $lang['nav_systempage'] = 'ระบบ';

  // register
  $lang['fname'] = 'ชื่อ';
  $lang["lname"] ="นามสกุล";
  $lang['fullname'] = 'ชื่อ - สกุล';
  $lang['tel'] = 'เบอร์โทรศัพท์';
  $lang['name_company'] = 'ชื่อบริษัท';
  $lang['tin'] = 'เลขประจำตัวผู้เสียภาษีอากร (13หลัก)';
  $lang['confirm'] = 'ยืนยันตัวตนเรียบร้อย';
  $lang['setpassword'] = 'กรุณาระบุรหัสผ่านที่ท่านต้องการ';

  // announce
  $lang['announce_header'] = 'งานที่แนะนำ';
  $lang['announce_header_similar'] = 'งานที่เกี่ยวข้อง';
  $lang['add_announce'] = 'เพิ่มประกาศ';
  $lang['save'] = 'ลงประกาศ';
  $lang['income_min'] = 'เงินเดือนเริ่มต้น';
  $lang['income_max'] = 'เงินเดือนเดือนสูงสุด';
  $lang['income'] = 'เงินเดือนต่อรองได้ตามความสามารถ';
  $lang['income_label'] = 'เงินเดือน';
  $lang['experience'] = 'ประสบการณ์ทำงาน';
  $lang['property'] = 'คุณสมบัติที่เกี่ยวข้องอื่นๆ';
  $lang['property_label'] = 'คุณสมบัติ';
  $lang['benefits'] = 'สวัสดิการ';
  $lang['type_job'] = 'ประเภทงาน';
  $lang['announce_title'] = 'หัวข้อประกาศ';
  $lang['education'] = 'วุฒิการศึกษา';
  $lang['detail'] = 'รายละเอียดงาน';
  $lang['age'] = 'อายุ';
  $lang['age_min'] = 'อายุเริ่มต้น';
  $lang['age_max'] = 'อายุสูงสุด';
  $lang['gender'] = 'เพศ';
  $lang['license'] = 'ใบอนุญาตขับขี่';
  $lang['age'] = 'ช่วงอายุ';
  $lang['zone'] = 'พื้นที่ทำงาน';
  $lang['remark'] = 'หมายเหตุ';
  $lang['working_day'] = 'วันทำงาน';
  $lang['working_start'] = 'เวลาเข้างาน';
  $lang['working_end'] = 'เวลาออกงาน';
  $lang['working_start_date'] = 'วันเริ่มงาน';
  $lang['working_end_date'] = 'วันสิ้นสุดงาน';
  $lang['shift_work'] = 'ทำงานเป็นกะ';
  $lang['work_full_time'] = 'ทำงานเต็มเวลา';
  $lang['job_description_1'] = 'ตำแหน่งงาน';
  $lang['job_description_2'] = 'ลักษณะงาน';
  $lang['edit'] = 'แก้ไข';
  $lang['job_position'] = 'ตำแหน่งงาน';
  $lang['staff'] = 'ผู้ดูแลประกาศ';
  $lang['infor'] = 'ช่องทางการประกาศ';


  // company
  $lang['company'] = 'บริษัท';
  $lang['add_detail'] = 'เพิ่มรายละเอียด';
  $lang['edit_detail'] = 'แก้ไขรายละเอียด';
  $lang['savedetail'] = 'บันทึก';
  $lang['company_name'] = 'ชื่อบริษัท';
  $lang['company_kind_detail'] = 'อื่นๆ(ระบุ)';
  $lang['company_kind'] = 'ประเภทธุรกิจ';
  $lang['address'] = 'ที่อยู่';
  $lang['company_address'] = 'ที่อยู่บริษัท';
  $lang['province'] = 'จังหวัด';
  $lang['aumphur'] = 'เขต/อำเภอ';
  $lang['tumbon'] = 'แขวง/ตำบล';
  $lang['map'] = 'แผนที่';
  $lang['travel'] = 'การเดินทาง';
  $lang['web'] = 'เว็บไซต์';
  $lang['company_tel'] = 'เบอร์โทร';
  $lang['count_announce'] = 'ประกาศทั้งหมด';
  $lang['count_announce_me'] = 'ประกาศของฉัน';
  $lang['status_all'] = 'ทั้งหมด';
  $lang['status_online'] = 'ACTIVE';
  $lang['status_offline'] = 'UNACTIVE';
  $lang['apply'] = 'รายชื่อผู้สมัคร';
  $lang['view'] = 'รายชื่อผู้เข้าชม';
  $lang['profile'] = 'ประวัติ';
  $lang['activity'] = 'กิจกรรม';
  $lang['file'] = 'ไฟล์';
  $lang['file_image'] = 'ภาพประกอบ';


  // user
  $lang['users'] = 'ผู้ใช้ระบบ';
  $lang["table_label_show"] = "แสดง";
  $lang["table_label_row"] = "แถว";
  $lang["table_search"] = "ค้นหาชื่อหัวข้อ";
  $lang["fnamelname"] = "ชื่อ - สกุล";
  $lang["userdetail"] = "รายละเอียด";
  $lang["level"] = "สิทธิ์การใช้งาน";


  // profile

  $lang["fname"] ="ชื่อ";
  $lang["lname"] ="นามสกุล";
  $lang["invite_by"] ="เชิญเข้าใช้งานโดย";
  $lang["invite_at"] ="เชิญเข้าใช้งานเมื่อ";
  $lang["register_at"] ="สมัครเข้าใช้งานเมื่อ";
  $lang["update_by"] ="แก้ไขข้อมูลโดย";
  $lang["update_at"] ="แก้ไขข้อมูลเมื่อ";
  $lang["level"] ="ตำแหน่ง";
  $lang["change"] ="เปลี่ยนรูป";

  // announce
  $lang["filter"] ="ค้นหาอย่างละเอียด";
  $lang["PleaseLogin"] ="กรุณาเข้าสู่ระบบก่อนสมัครงาน";
  $lang["typeSearch"] ="ประเภทค้นหา";
  $lang["search"] ="ค้นหา";
  $lang["about"] ="เกี่ยวกับบริษัท";
  $lang["job"] ="เกี่ยวกับงาน";
  $lang["contact"] ="สอบถามเพิ่มเติม";
  $lang["qualification"] ="คุณสมบัติ";
  $lang["none"] ="ไม่ระบุ";

  // profile applicant
  $lang["birth_date"] ="วัน/เดือน/ปีเกิด";
  $lang["age"] ="อายุ";
  $lang["age_type"] ="ปี";
  $lang["mobile"] ="เบอร์มือถือ";
  $lang["pfix"] ="คำนำหน้า";
  $lang["married_status"] ="สถานะสมรส";
  $lang["apply_register"] ="สมัครงาน";
  $lang["degree"] ="วุฒิการศึกษา";
  $lang["institute"] ="สถานศึกษา";
  $lang["major"] ="สาขา";
  $lang["finish_year"] ="ปีที่จบ";
  $lang["grade"] ="เกรดเฉลี่ย";




?>
