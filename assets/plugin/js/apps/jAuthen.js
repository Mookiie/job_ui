$(document).ready(function() {
  closeloader();
  chkPassCode();
})

// $("form#loginform").submit(function(ev){
//     ev.preventDefault();
//     var dataform = { "username":$('#username').val(),
//                      "password":md5($('#password').val()),
//                    };
//     var api_url = getApi("login");
//     $.ajax({
//        url: api_url,
//        type: 'POST',
//        data: dataform,
//        beforeSend:function(){
//          preloader();
//        }
//      })
//      .done(function(data) {
//        closeloader()
//        var obj = JSON.parse(data);
//        if (obj.status==200) {
//          $(".loader").show();
//          var autoken = obj["token"];
//          var connect = getUrl('home');
//          localStorage.setItem("token",autoken);
//          localStorage.setItem('channel',obj.channel);
//          window.location = connect;
//          preloader();
//        }else if (obj.status==402) {
//          addErr("password",lang.error.login_password);
//        }else if (obj.status==501) {
//          addErr("username",lang.error.login_waitconfirm);
//        }else if (obj.status==502) {
//          addErr("username",lang.error.login_noactive);
//        }else if (obj.status==404) {
//          addErr("username",lang.error.login_notfound);
//        }
//
//      })
//
//   })

// $("#loginform").keypress(function(event) {
//     var code = event.keyCode;
//     if (code == 13) {
//         $(".btn#login").trigger('click');
//     }
// });

$("#change_confirm").click(function() {
console.log("change_confirm");
  if ($('#password').val() == '') {
    addErr("password",lang.error.input_blank);
  }else if (!chkpassword($('#password').val())) {
    addErr("password",lang.error.guide_password);
  }
  if (($('#password').val() != '') && (chkpassword($('#password').val()))) {
    var api_url = getApi('auth/comfirm');
    var formData = { "uid":$('#id').val(),
                      "code":$('#code').val(),
                      "password":md5($('#password').val())
                    };
                    console.log(formData);
    $.ajax({
      url: api_url,
      type: 'POST',
      data: formData,
      beforeSend:function(){
        preloader();
      }
    })
    .done(function(data) {
        closeloader()
        var obj = JSON.parse(data);
        if (obj.status == 200){
          // var connect = getUrl('');
          log_user('0','','Home','','','','','','','','')
          setTimeout(function(){
              // window.location = connect
              window.location = window.location.href
          }, 2000);
          Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.success, 40000);

        }else if (obj.status == 304) {
          Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.confirm, 40000);
        }
    })
  }
})

function chkPassCode() {
  var api_url = getApi('auth/chkPassCode');
  var formData = { "uid":$('#id').val(),
                    "code":$('#code').val(),
                  };
  $.ajax({
    url: api_url,
    type: 'POST',
    data: formData,
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
    closeloader();
    var obj = JSON.parse(data);
    if (obj.status == 304) {
      var connect = getUrl('');
      log_user('0','','Home','','','','','','','','')
      setTimeout(function(){
          window.location = connect
      }, 2000);
    }
  })
}
