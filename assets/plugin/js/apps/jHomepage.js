$(document).ready(function() {
  closeloader();
  $('select').material_select();
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    $("#nav-search").removeClass("display-disable");
  }
  getAnnounce_content()
  getAnnounceTable()
  getHiringCarousel()
  getCountAnnounce()
});

$(".carousel-hidden").click(function() {
  $(".carousel").hide('slow');
  $(".carousel-hidden").hide('slow');
})

function getAnnounce_content() {
  // var api_url = getApi('getannounce_content');
  var api_url;
  var token = localStorage.getItem('token');
  if(token == null){
    api_url = getApi('getannounce');
  }else {
    api_url = getApi('getannounce_content');
  }
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
      closeloader()
      var obj = JSON.parse(data);
      console.log(obj);
      $(".closemodal").trigger('click');
        for (var i = 0; i < obj.data.length; i++) {
            var row = genCardAnnounce(obj.data[i]);
            $(".content-announce").append(row)
        }
  })
}

// function getAnnounceMatch(data) {
//   console.log("getAnnounceMatch");
//   var api_url = getApi('applicant/getAnnounceMe');
//   var token = localStorage.getItem('token');
//   if (data) {
//     var formData = {
//                       "type_job":$('#type_job').val(),
//                       "job_description_1":$('#job_description_1').val(),
//                       "job_description_2":$('#job_description_2').val(),
//                       "education":$('#education').val(),
//                       "zone":$('#zone').val(),
//                       "company_name":$('#company_name').val(),
//                       "offset":'0'
//                     };
//   }else {
//     var formData = {
//                       "type_job":'',
//                       "job_description_1":'',
//                       "job_description_2":'',
//                       "education":'',
//                       "zone":'',
//                       "company_name":'',
//                       "offset":'0'
//                     };
//   }
//   $.ajax({
//     url: api_url,
//     type: 'GET',
//     headers:{"token":token},
//     data:formData,
//     beforeSend:function(){
//       preloader();
//     }
//   })
//   .done(function(data) {
//       closeloader()
//       var obj = JSON.parse(data);
//       $(".closemodal").trigger('click');
//       if (obj.count == 0) {
//         getAnnounceMatch(0)
//       }else {
//         for (var i = 0; i < obj.data.length; i++) {
//             var row = genCardAnnounce(obj.data[i]);
//             $(".content-announce").append(row)
//             $('.tooltipped').tooltip();
//         }
//       }
//   })
// }


function getAnnounceTable() {
  var select = $("#select-table").val();
  $("#table_content").html('')
  $("#table_content_mobile").html('')
  var token = localStorage.getItem('token');
  var api_url ;
  var formData  = {
                    "type_job":'',
                    "job_description_1":'',
                    "job_description_2":'',
                    "education":'',
                    "zone":'',
                    "company_name":'',
                    "offset":'0'
                  };
    switch(select) {
      case "0":
      //ประกาศที่มีคนดูสูงสุด
        api_url = getApi('getannounce_topview');
        break;
      case "1":
      //ประกาศที่มีคนสนใจสูงสุด
        api_url = getApi('getannounce_topfavorite');
        break;
      case "2":
      //ประกาศที่มีเงินเดือนสูงสุด
        api_url = getApi('getannounce_topincome');
        break;
      case "3":
      //ประกาศที่ท่านสนใจ
        api_url = getApi('getannounce_myfavorite');
        break;
      case "4":
      //ประกาศของเดือนนี้
        api_url = getApi('getannounce_month');
        break;
      default:
      // ประกาศทั้งหมด
        api_url = getApi('getannounce');
    }
    console.log(api_url);
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data:formData,
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
      closeloader()
      var obj = JSON.parse(data);
      console.log(obj);
      if (obj.status == 200) {
        for (var i = 0; i < obj.data.length; i++) {
            var row = genTableAnnounce(obj.data[i],(i+1));
            $("#table_content").append(row)
            var row_mobile = genAnnounce_table_mobile(obj.data[i]);
            $("#table_content_mobile").append(row_mobile)
        }
      }else {
        $("#table_content").append("<tr><td colspan='8'>ไม่พบข้อมูล</td></tr>");
        $("#table_content_mobile").append("<div class='table-mobile-header'><div class='row reset-magin'><span><h5 class='center'>ไม่พบข้อมูล</h5></span></div></div>");
      }

  })

}

function myfavorite() {
  $("#table_content").html('')
  $("#table_content_mobile").html('')
  var token = localStorage.getItem('token');
  api_url = getApi('getannounce_myfavorite');
  var formData  = {
                    "type_job":'',
                    "job_description_1":'',
                    "job_description_2":'',
                    "education":'',
                    "zone":'',
                    "company_name":'',
                    "offset":'0'
                  };
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data:formData,
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
      closeloader()
      var obj = JSON.parse(data);
      console.log(obj);
      if (obj.status == 200) {
        for (var i = 0; i < obj.data.length; i++) {
            var row = genTableAnnounce(obj.data[i],(i+1));
            $("#table_content").append(row)
            var row_mobile = genAnnounce_table_mobile(obj.data[i]);
            $("#table_content_mobile").append(row_mobile)
        }
      }else {
        $("#table_content").append("<tr><td colspan='8'>ไม่พบข้อมูล</td></tr>");
        $("#table_content_mobile").append("<div class='table-mobile-header'><div class='row reset-magin'><span><h5 class='center'>ไม่พบข้อมูล</h5></span></div></div>");
      }

  })

}

function count_view(announce_id,link_name) {
  var link = "announce?announce="+btoa(btoa(btoa(announce_id)))
    link += "&link_name="+btoa(btoa(btoa(link_name)));
    link += "&link_infor="+btoa(btoa(btoa('99')));
    var connect = getUrl(link);
    window.open(connect)
}

// function view_detail(announce_id) {
//   log_user('1',announce_id,'','','','','','','','','')
//   var authtoken = localStorage.getItem('token');
//   var url = getApi('getAnnounceById');
//   $.ajax({
//     url: url,
//     type: 'GET',
//     headers:{"token":authtoken},
//     data:{"announce_id":announce_id},
//   })
//   .done(function(data) {
//     var obj = JSON.parse(data);
//     var data_detail = obj.data[0];
//     console.log(data_detail);
//     var connectURL = getUrl("DetailDialog");
//     var formData = {"ajax":true};
//         formData["data"] = data_detail ;
//     $.ajax({
//             url: connectURL,
//             type: 'POST',
//             data: formData
//           }).done(function(data) {
//             closeloader()
//             var obj_modal = JSON.parse(data);
//             $(".modal-area").append(obj_modal["modal"]);
//             $("#DetailDialog").Modal({"backdrop":true,"static":true});
//           })
//   })
// }
//
// function apply_job(announce_id) {
//   log_user('2',announce_id,'','','','','','','','','')
//   var authtoken = localStorage.getItem('token');
//   if(authtoken == null){
//     Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.PleaseLogin,6000);
//   }else {
//     var api_url = getApi('applicant/apply_job');
//     $.ajax({
//       url: api_url,
//       type: 'POST',
//       headers:{"token":authtoken},
//       data:{"announce_id":announce_id},
//     })
//     .done(function(data) {
//       $(".closemodal").trigger("click")
//       var obj = JSON.parse(data);
//       if (obj.status==200) {
//         Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
//       }else if (obj.status==202) {
//         Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.duplicate,6000);
//       }
//       else {
//         Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
//       }
//     })
//   }
//
// }
//
function favorite_job(announce_id) {
  console.log(announce_id+'// Favorite');
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.PleaseLogin,6000);
  }else {
    if (localStorage.getItem('data') == 'users') {
      Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.permission,6000);
    }else {
      var api_url = getApi('applicant/favorite_job');
      $.ajax({
        url: api_url,
        type: 'POST',
        headers:{"token":authtoken},
        data:{"announce_id":announce_id},
      })
      .done(function(data) {
        var obj = JSON.parse(data);
        console.log(obj);
        if (obj.status==200) {
          getAnnounceTable()
          getCountAnnounce()
          log_user('4',announce_id,'','','','','','','','','')
          Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
        }else if (obj.status==202) {
          getAnnounceTable()
          getCountAnnounce()
          log_user('7',announce_id,'','','','','','','','','')
          Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success_cancle,6000);
        }else {
          Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
        }
      })
    }
  }
}

function unfavorite_job(announce_id) {
  console.log(announce_id+'// unFavorite');
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.PleaseLogin,6000);
  }else {
    Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.success,6000);
  }
}

function getHiringCarousel() {
  // getHiringCarousel
  var api_url = getApi('getHiringCarousel');
  $.ajax({
    url: api_url,
    type: 'GET',
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
    closeloader();
    console.log(data);
    var obj = JSON.parse(data);
    for (var i = 0; i < obj.data.length; i++) {
        var row = genCarouselList(obj.data[i]);
        $(".carousel.carousel-slider").append(row)
    }
    $('.carousel.carousel-slider').carousel({
      fullWidth: true,
      time_constant: 100
    });
    setInterval(function(){$('.carousel.carousel-slider').carousel('next')},10000)
  })
}
// function close_detail() {
//   $(".announce_detail").css({"display":"none"})
//   $(".announce").css({"display":""})
//   $("#announce_id").val('')
//   $("#company_name").html('');
//   $("#logo_company").removeAttr("src");
//   $("#company_kind").html('');
//   $("#location").html('');
//   $("#job_description_1").html('');
//   $("#announce_title").html('');
//
//   $("#type_job").html('');
//   $("#income").html('');
//   $("#gender").html('');
//   $("#age").html('');
//   $("#education").html('');
//   $("#license").html('');
//   $("#working_day").html('');
//   $("#working_start").html('');
//   $("#working_end").html('');
//   $("#property").html('');
//   $("#working_start_date").html('');
//
//   $("#benefits").html('');
//
//   $("#fullname").html('');
//   $("#email_contact").html('');
//   $("#tel").html('');
//
//   $("#company_detail").html('');
//   $("#company_address").html('');
//   $("#map").removeAttr("src");
//
//
// }
