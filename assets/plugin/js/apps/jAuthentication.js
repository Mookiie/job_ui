$(document).ready(function() {
  console.log("authentication");
  var authtoken = localStorage.getItem('token');
  $("#li-announce-me").addClass("display-disable");
  $("#li-announce-favorite").removeClass("display-disable");
  if(authtoken == null){
    $("#nav_profile").addClass("display-disable");
    $("#logout").addClass("display-disable");
    $("#editApplicant").addClass("display-disable");
    $("#get-profile").addClass("display-disable");
    $("#get-login").removeClass("display-disable");
    closeloader();
  }else {
    $("#nav_profile").removeClass("display-disable");
    $("#nav_login_user").addClass("display-disable");
    $("#nav_login_comp").addClass("display-disable");
    $("#logout").removeClass("display-disable");
    $("#get-profile").removeClass("display-disable");
    $("#get-login").addClass("display-disable");
    Getprofile()
    closeloader();
  }

})

$(".auth-logout").click(function(event) {
    var api_url = getApi('logout');
    var token = localStorage.getItem('token');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":token},
      beforeSend:function(){
        preloader();
      }
    })
    .done(function(data) {
        closeloader()
        var obj = JSON.parse(data);
        if (obj.status == 200) {
          localStorage.clear();
          var connect = getUrl('');
          window.location = connect;
        }
    })
});

function log_user(action_id,passive,page,job_type,job_description_1,job_description_2,province,aumphur,education,company,infor) {
  // auth/logUser
  console.log("log_user");
  var token = localStorage.getItem('token');
  if (token == null) {
    token = "";
  }else {
    token = token;
  }
  var formData = {
                    "token":token,
                    "uid":$("#uid_log").val(),
                    "action_id":action_id,
                    "passive":passive,
                    "page":page,
                    "job_type":job_type,
                    "job_description_1":job_description_1,
                    "job_description_2":job_description_2,
                    "zone":"",
                    "province":province,
                    "aumphur":aumphur,
                    "education":education,
                    "company":company,
                    "infor":infor
                 }
    console.log(formData);
    var api_url = getApi('auth/logUser');
    $.ajax({
     url: api_url,
     type: 'POST',
     headers:{"token":token},
     data:formData
     })
      .done(function(data) {
        console.log(data);
     })
}

$(".auth-editApplicant").click(function() {
  editProfileApplicant()
})
function editProfileApplicant() {
    var authtoken = localStorage.getItem('token');
    if(authtoken == null){
      Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.PleaseLogin,6000);
    }else {
      var api_url = getApi('applicant/getprofile');
      $.ajax({
        url: api_url,
        type: 'GET',
        headers:{"token":authtoken},
      })
      .done(function(data){
        console.log(data);
        var obj = JSON.parse(data);
          // modal
          var connectURL = getUrl("getEditApplicantDialog");
          var formData = {"ajax":true};
              formData["data"] = "" ;
          $.ajax({
                  url: connectURL,
                  type: 'POST',
                  data: formData
                }).done(function(data) {
                  closeloader()
                  var obj_modal = JSON.parse(data);
                  $(".modal-area").append(obj_modal["modal"]);
                  $("#citizenDialog").Modal({"backdrop":true,"static":true});

                    $('#citizen').val(obj.data.id_card);
                    $('#tel').val(obj.data.tel);
                    $('#email').val(obj.data.email);
                    ms_province_get(obj.data.province);
                    ms_aumphur_get(obj.data.province,obj.data.aumphur);
                    // if (obj.data.birth_date != ""|| obj.data.birth_date != "0000-00-00") {
                    //   console.log("no 0000-00-00");
                    //   $('#birth_date').val(formatDateToDmy(obj.data.birth_date))
                    //   $('#birth_date').datepicker({
                    //     setDate:obj.data.birth_date,
                    //     format: 'dd/mm/yyyy',
                    //     date: new Date(obj.data.birth_date),
                    //     startDate:new Date(1970,1,1),
                    //     endDate:new Date()
                    //   });
                    // }else {
                    //   console.log("0000-00-00");
                    //   $('#birth_date').datepicker({
                    //     setDate:obj.data.birth_date,
                    //     format: 'dd/mm/yyyy',
                    //     startDate:new Date(1970,1,1),
                    //     endDate:new Date()
                    //   });
                    // }
                    var date = new Date(obj.data.birth_date);
                      var dd = date.getDate();
                      var mm = date.getMonth();
                      var yy = date.getFullYear();
                    //   $('.datepicker').pickadate({
                    //     selectMonths: true,
                    //     clear:'',
                    //     cancel:'Cencel',
                    //     defaultDate:obj.data.birth_date,
                    //     closeOnSelect: false,
                    //     setDefaultDate:true,
                    //     format : "dd-mm-yyyy",
                    //   })
                  $('input.datepicker').val(formatDateToDmy(obj.data.birth_date));
                  if (obj.data.birth_date == '0000-00-00') {
                    var datePicker = $('input.datepicker').Zebra_DatePicker({
                      format : "d/m/Y",
                      show_select_today: false,
                      show_week_number:false,
                      current_date: new Date(1900,0,1),
                    });
                  }else {
                    var datePicker = $('input.datepicker').Zebra_DatePicker({
                      format : "d/m/Y",
                      show_select_today: false,
                      show_week_number:false,
                      current_date: new Date(obj.data.birth_date)
                    });
                  }
                  // ms_province_get();
                  $("#user_province").change(function() {
                    ms_aumphur_get($("#user_province").val(),'');
                  })
                  $("#update_applicant").click(function() {
                    update_applicant();
                  })
                  $("#citizen").keyup(function(event) {
                    if (event.keyCode === 13) {
                     event.preventDefault();

                   }else if (event.keyCode === 8) {

                   }else {
                    autoTab($("#citizen")[0]);
                   }
                  });
                })

      })
    }

}
function EditApplicant() {
  console.log('EditApplicant');
  var str = $("#citizen").val().trim();
  var regex = /-/gi;
  var res = str.replace(regex, "");
  console.log(res);
  var authtoken = localStorage.getItem('token');
  var birth_date = formatDateToYmd($("#birth_date").val());
  var tel = $("#tel").val();
  if (tel == "") {addErr("tel",lang.error.input_blank)}
  else if (!phonenumber(tel)) {addErr("tel",lang.error.data_worng)}
  if ($('#email').val() == '') { addErr("email",lang.error.input_blank);}
  else if (!validateEmail($('#email').val())) { addErr("email",lang.error.email_worng) }
  if (!checkID(res)) {
    addErr("citizen",lang.error.data_worng)
  }
  if (checkID(res)
      && tel != ""
      && phonenumber(tel)
      && $('#email').val() != ''
      && validateEmail($('#email').val())
     ) {
    var api_url = getApi('applicant/edit_applicant');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":authtoken},
      data:{"citizen":res,
            "province":$("#user_province").val(),
            "aumphur":$("#user_aumphur").val(),
            "birth_date":birth_date,
            "tel":$("#tel").val(),
            "email":$("#email").val()
           },
    })
    .done(function(data) {
      console.log(data);
      $(".closemodal").trigger("click")
      var obj = JSON.parse(data);
      if (obj.status==200) {
        Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
      }else {
        Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.error,6000);
      }
    })
  }
}
// $("#profileUpload").change(function(event) {
//   var files = $(this)[0].files;
//   var type = files[0].name;
//       type = type.split(".");
//       type = type[type.length - 1];
//       type = type.toLowerCase();
//       if (type == "png" || type == "jpg" || type == "jpeg" || type == "gif" ) {
//           var submitUrl = getApi('users/profileimg');
//           var ajax = new XMLHttpRequest();
//           var token = localStorage.getItem('token');
//           var formData = new FormData();
//               formData.append('file[]',files[0]);
//           ajax.upload.addEventListener("progress",function(event){
//               if(event.lengthComputable){
//                   preloader();
//                 }
//           });
//           ajax.addEventListener("readystatechange",function(event){
//             if(this.readyState ==4){
//                 if(this.status == 200){
//                    var obj = JSON.parse(this.responseText);
//                    if (obj.status == 200) {
//                       location.reload();
//                    }
//               }
//             }
//           });
//           ajax.open('POST',submitUrl);
//           ajax.setRequestHeader("token",token);
//           ajax.send(formData);
//       }
// });

function pageHome() {
  log_user('0','','Home','','','','','','','','')
  var connect = getUrl('');
  setTimeout(function(){
      window.location = connect
  }, 2000);
  preloader();
}//end pageHome

function pageAdministrator() {
  log_user('0','','Administrator','','','','','','','')
  var connect = getUrl('company/administrator');
  setTimeout(function(){
      window.location = connect
  }, 2000);
  preloader();
}//end pageAdministrator

function pageannounce() {
  log_user('0','','Announce','','','','','','','','');
  var connect = getUrl('company/announce');
  setTimeout(function(){
      window.location = connect
  }, 2000);
  preloader();
}//end pageannounce

function pageAnnounceMe() {
  var connect = getUrl('company/AnnounceMe');
  setTimeout(function(){
    window.location = connect
}, 2000);
  preloader();
}//end pageannounce

function pageSystem() {
  var connect = getUrl('');
  setTimeout(function(){
    window.location = connect
}, 2000);
  preloader();
}//end pageSystem

function modalUserLogin() {
  console.log("modalUserLogin");
  var connect = getUrl("line/login");
  setTimeout(function(){
    // window.open(connect)
    window.location = connect
}, 2000);
}//end modalUserLogin


function modalCompanyLogin() {
  console.log("modalCompanyLogin");
  var connectURL = getUrl("login/company");
  var formData = {"ajax":true};
      formData["data"] = "" ;
  $.ajax({
          url: connectURL,
          type: 'POST',
          data: formData
        }).done(function(data) {
          closeloader()
          var obj_modal = JSON.parse(data);
          $(".modal-area").append(obj_modal["modal"]);
          $("#loginDialog").Modal({"backdrop":true,"static":true});
        })
}//end modalCompanyLogin

function login_company() {
  console.log("login_company");
  var connectURL = getApi("login/company");
  var formData = { "email":$('#email').val(),
                   "password":md5($('#password').val()),
                 };
  if ($('#email').val() == '') {
    addErr("email",lang.error.input_blank);
  }
  if ($('#password').val() == '') {
    addErr("password",lang.error.input_blank);
  }
  if (($('#password').val() != '')&&($('#email').val() != '') ) {
        $.ajax({
          url: connectURL,
          type: 'POST',
          data: formData
        }).done(function(data) {
          closeloader()
          var obj = JSON.parse(data);
           if (obj.status==200) {
             $(".loader").show();
             var autoken = obj["token"];
             localStorage.setItem("token",autoken);
             localStorage.setItem('channel',obj.channel);
             localStorage.setItem('data',"users");
             pageannounce()
           }else if (obj.status==301) {
             addErr("password",lang.error.login_password);
           }else if (obj.status==302) {
             addErr("email",lang.error.login_waitconfirm);
           }else if (obj.status==303) {
             addErr("email",lang.error.login_noactive);
           }else if (obj.status==300) {
             addErr("email",lang.error.login_notfound);
           }
        })
  }
}

function logout() {
  // if (position == 'applicant') {
  //
  // }else {
    var api_url = getApi('logout');
    var token = localStorage.getItem('token');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":token},
      beforeSend:function(){
        preloader();
      }
    })
    .done(function(data) {
      closeloader()
      var obj = JSON.parse(data);
      if (obj.status == 200) {
        localStorage.clear();
        var connect = getUrl('');
        window.location = connect;
      }
  })
  // }
}

function register_company() {
  $(".closemodal").trigger('click');
  var connectURL = getUrl("register/company");
  var formData = {"ajax":true};
      formData["data"] = "" ;
  $.ajax({
          url: connectURL,
          type: 'POST',
          data: formData
        }).done(function(data) {
          closeloader()
          var obj_modal = JSON.parse(data);
          $(".modal-area").append(obj_modal["modal"]);
          $("#RegistorDialog").Modal({"backdrop":true,"static":true});
          $("#tel").keyup(function(event){
             if(event.which != 10 && isNaN(String.fromCharCode(event.which))){
              event.preventDefault(); //stop character from entering input
              addErr("tel",lang.error.pattern_worng)
             }
          })
          $("#tin").keyup(function(event){
             if(event.which != 13 && isNaN(String.fromCharCode(event.which))){
              event.preventDefault(); //stop character from entering input
              addErr("tin",lang.error.pattern_worng)
             }
          })
        })
}

$("#tel").keyup(function(event){
   if(event.which != 10 && isNaN(String.fromCharCode(event.which))){
    event.preventDefault(); //stop character from entering input
    addErr("tel",lang.error.pattern_worng)
   }
})
$("#tin").keyup(function(event){
   if(event.which != 13 && isNaN(String.fromCharCode(event.which))){
    event.preventDefault(); //stop character from entering input
    addErr("tin",lang.error.pattern_worng)
   }
})

function company_register() {
  var connect = getUrl('company/register');
  preloader();
}

function Register() {
  console.log("register");
  if ($('#fname').val() == '') { addErr("fname",lang.error.input_blank);}
  if ($('#lname').val() == '') { addErr("lname",lang.error.input_blank);}
  if ($('#email').val() == '') { addErr("email",lang.error.input_blank);}
  else if (!validateEmail($('#email').val())) { addErr("email",lang.error.email_worng) }
  if ($('#tel').val() == '') { addErr("tel",lang.error.input_blank);}
  else if (!phonenumber($('#tel').val())) { addErr("tel",lang.error.phone_worng);}
  if ($('#cname').val() == '') { addErr("cname",lang.error.input_blank);}
  if ($('#tin').val() == '') { addErr("tin",lang.error.input_blank);}
  else if (!number13($('#tin').val())) { addErr("tin",lang.error.tin_worng);}
  if (($('#fname').val() != '')
      &&($('#lname').val() != '')
      &&($('#email').val() != '')
      &&($('#tel').val() != '')
      &&($('#cname').val() != '')
      &&($('#tin').val() != '')
      &&(validateEmail($('#email').val()))
      &&(phonenumber($('#tel').val()))
      &&(number13($('#tin').val()))
     ) {
        var formData1 = { "fname":$('#fname').val(),
                          "lname":$('#lname').val(),
                          "email":$('#email').val(),
                          "tel":$('#tel').val()
                       };

      var connectURL = getApi("auth/createuser");
        $.ajax({
          url: connectURL,
          type: 'POST',
          data: formData1,
          beforeSend:function(){
            preloader();
          }
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
             var formData2 = { "company_name":$('#cname').val(),
                               "tin":$('#tin').val(),
                               "uid":obj.id,
                             };
             var connectURL = getApi("auth/createcompany");
             $.ajax({
               url: connectURL,
               type: 'POST',
               data: formData2
             }).done(function(data) {
               closeloader()
               var obj = JSON.parse(data);
                if (obj.status==200) {
                  $(".closemodal").trigger('click');
                  Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success, 40000);
                }else {
                  Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning, 40000);
                }
             })
           }
           else if (obj.status==202) {
             addErr("email",lang.error.email_duplicate);
           }
        })
  }
}

function pageProfile(id){
  var connectURL = getUrl("profile");
  var formData = {"ajax":true};
      formData["data"] = profile_me.data ;
      // formData["data"] = obj['data'] ;
  $.ajax({
          url: connectURL,
          type: 'POST',
          data: formData
        }).done(function(data) {
          var obj_modal = JSON.parse(data);
          $(".modal-area").append(obj_modal["modal"]);
          $("#ProfileDialog").Modal({"backdrop":true,"static":true});
          $('.tabs-member').tabs();
          $(".details").click(function () {
            $('#editProfile').css({"display":"none"})
          })
          $(".edit").click(function () {
            $('#editProfile').css({"display":""})
          })
          $("#editProfile").click(function () {
            editProfile()
          })

        })// load modal
}//end pageProfile

function editProfile() {
  var api_url = getApi('users/editUser');
  var token = localStorage.getItem('token');
  var formData = {  "fname":$("#fname").val(),
                    "lname":$("#lname").val(),
                    "tel":$("#tel").val(),
                    "uid":""
                 }
                    console.log(formData);
  $.ajax({
          url: api_url,
          type: 'POST',
          data: formData,
          headers:{"token":token},
          beforeSend:function(){
            preloader();
          }
        }).done(function(data) {
          closeloader();
          var obj = JSON.parse(data);
           if (obj.status==200) {
             if ($("#file_profile").val() != '') {
               upload_profile(obj.id)
             }else {
               $(".closemodal").trigger('click');
               getCompanyDetail()
               Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
             }
           }else {
             Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
           }
        })
}
function upload_profile(id) {
  var files = $("#file_profile")[0].files;
  var type = files[0].name;
      type = type.split(".");
      type = type[type.length - 1];
      type = type.toLowerCase();
  var submitUrl = getApi('users/profile_files');
  var ajax = new XMLHttpRequest();
  var token = localStorage.getItem('token');
  var formData = new FormData();
      formData.append('file[]',files[0]);
      formData.append('id',id)
  ajax.upload.addEventListener("progress",function(event){
      if(event.lengthComputable){
          //
        }
  });
  ajax.addEventListener("readystatechange",function(event){
    if(this.readyState ==4){
        if(this.status == 200){
          closeloader()
           var obj = JSON.parse(this.responseText);
           if (obj.status == 200) {
             swal({
                    title: 'Success!',
                    text: "",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                 }).then((result) => {
                    if (result.value) {
                      location.reload();
                    }
                })
           }
      }
    }
  });
  ajax.open('POST',submitUrl);
  ajax.setRequestHeader("token",token);
  ajax.send(formData);
}


// master
function ms_job_type_get(type) {
  var connectURL = getApi("master/ms_job_type_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".type_job").html("<option value='' disabled selected>"+lang.data.type_job+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['id'] == type) {
                  $(".type_job").append("<option value="+obj.data[i]['id']+" selected >"+obj.data[i]['name']+"</option>");
                }else {
                  $(".type_job").append("<option value="+obj.data[i]['id']+">"+obj.data[i]['name']+"</option>");
                }
              }
           }
        })
}

function ms_job_description_1_get(job1) {
  var connectURL = getApi("master/ms_job_description_1_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".job_description_1").html("<option value='' disabled selected>"+lang.data.job_description_1+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['job_description_code_1'] == job1) {
                  $(".job_description_1").append("<option value="+obj.data[i]['job_description_code_1']+" selected >"+obj.data[i]['job_description_name']+"</option>");
                }else {
                  $(".job_description_1").append("<option value="+obj.data[i]['job_description_code_1']+">"+obj.data[i]['job_description_name']+"</option>");
                }
              }
           }
        })
}

function ms_job_description_2_get(job1,job2) {
  var connectURL = getApi("master/ms_job_description_2_get");
  $.ajax({
          url: connectURL,
          type: 'GET',
          data: {"job1":job1}
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".job_description_2").html("<option value='' disabled selected>"+lang.data.job_description_2+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['job_description_code_2'] == job2) {
                  $(".job_description_2").append("<option value="+obj.data[i]['job_description_code_2']+" selected>"+obj.data[i]['job_description_name']+"</option>");
                }else {
                  $(".job_description_2").append("<option value="+obj.data[i]['job_description_code_2']+">"+obj.data[i]['job_description_name']+"</option>");
                }
              }
           }
        })
}

function ms_zone_get(zone) {
  var connectURL = getApi("master/ms_zone_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".zone").html("<option value='' disabled selected>"+lang.data.zone+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['id'] == zone) {
                  $(".zone").append("<option value="+obj.data[i]['id']+" selected>"+obj.data[i]['name']+"</option>");
                }else {
                  $(".zone").append("<option value="+obj.data[i]['id']+">"+obj.data[i]['name']+"</option>");
                }
              }
           }
        })
}

function ms_gender_get(gender) {
  var connectURL = getApi("master/ms_gender_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".gender").html("<option value='' disabled selected>"+lang.data.gender+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['id'] == gender) {
                  $(".gender").append("<option value="+obj.data[i]['id']+" selected>"+obj.data[i]['name']+"</option>");
                }else {
                  $(".gender").append("<option value="+obj.data[i]['id']+">"+obj.data[i]['name']+"</option>");
                }
              }
           }
        })
}

function ms_license_get(license) {
  var connectURL = getApi("master/ms_license_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".license").html("<option value='' disabled selected>"+lang.data.license+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['driving_license_type_code'] == license) {
                  $(".license").append("<option value="+obj.data[i]['driving_license_type_code']+" selected >"+obj.data[i]['driving_license_type_name']+"</option>");
                }else {
                  $(".license").append("<option value="+obj.data[i]['driving_license_type_code']+">"+obj.data[i]['driving_license_type_name']+"</option>");
                }
              }
           }
        })
}

function ms_education_get(education) {
  var connectURL = getApi("master/ms_education_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".education").html("<option value='' disabled selected>"+lang.data.education+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['id'] == education) {
                  $(".education").append("<option value="+obj.data[i]['id']+" selected >"+obj.data[i]['name']+"</option>");
                }else {
                  $(".education").append("<option value="+obj.data[i]['id']+">"+obj.data[i]['name']+"</option>");
                }
              }
           }
        })
}

function ms_working_day_get(working_day) {
  var connectURL = getApi("master/ms_working_day_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".working_day").html("<option value='' disabled selected>"+lang.data.working_day+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['id'] == working_day) {
                  $(".working_day").append("<option value="+obj.data[i]['id']+" selected >"+obj.data[i]['name']+"</option>");
                }else {
                  $(".working_day").append("<option value="+obj.data[i]['id']+" >"+obj.data[i]['name']+"</option>");
                }
              }
           }
        })
}

function ms_company_kind_get(id) {
  var connectURL = getApi("master/ms_company_kind_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".company_kind").html("<option value='' disabled>"+lang.data.company_kind+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['id'] == id) {
                  $(".company_kind").append("<option value="+obj.data[i]['id']+" selected >"+obj.data[i]['name']+"</option>");
                }else {
                  $(".company_kind").append("<option value="+obj.data[i]['id']+">"+obj.data[i]['name']+"</option>");
                }
              }
           }
        })
}

function ms_company_name_get() {
  var connectURL = getApi("master/ms_company_name_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".company_name").html("<option value='' selected disabled>"+lang.data.company_name+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                  $(".company_name").append("<option value="+obj.data[i]['company_name']+">"+obj.data[i]['company_name']+"</option>");
              }
           }
        })
}

function ms_position(position_id) {
  var connectURL = getApi("master/ms_position_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".job_position").html("<option value='' disabled selected>"+lang.data.job_position+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['position_id'] == position_id) {
                  $(".job_position").append("<option value="+obj.data[i]['position_id']+" selected>"+obj.data[i]['position_name']+"</option>");
                }else {
                  $(".job_position").append("<option value="+obj.data[i]['position_id']+">"+obj.data[i]['position_name']+"</option>");
                }
              }
           }
        })
}

function ms_staff(staff_id) {
  var connectURL = getApi("master/ms_staff_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".staff").html("<option value='' disabled selected>"+lang.data.staff+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['staff_id'] == staff_id) {
                  $(".staff").append("<option value="+obj.data[i]['staff_id']+" selected>"+obj.data[i]['fname']+" "+obj.data[i]['lname']+"</option>");
                }else {
                  $(".staff").append("<option value="+obj.data[i]['staff_id']+">"+obj.data[i]['fname']+" "+obj.data[i]['lname']+"</option>");
                }
              }
           }
        })
}
function ms_information() {
  var connectURL = getApi("master/ms_information_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".infor").html("<option value='' disabled selected>"+lang.data.infor+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                  $(".infor").append("<option value="+obj.data[i]['infor_id']+">"+obj.data[i]['infor_name']+"</option>");
              }
           }
        })
}

function ms_province_get(pro_id) {
  var connectURL = getApi("master/ms_province_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".province").html("<option value='' disabled selected>"+lang.data.province+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['id'] == pro_id) {
                  $(".province").append("<option value="+obj.data[i]['id']+" selected>"+obj.data[i]['name']+"</option>");
                }else {
                  $(".province").append("<option value="+obj.data[i]['id']+">"+obj.data[i]['name']+"</option>");
                }
              }
           }
        })
}

function ms_aumphur_get(pro_id,aumphur_id) {
  var connectURL = getApi("master/ms_aumphur_get");
  $.ajax({
          url: connectURL,
          type: 'GET',
          data: {"pro_id":pro_id}
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".aumphur").html("<option value='' disabled selected>"+lang.data.aumphur+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['id'] == aumphur_id) {
                  $(".aumphur").append("<option value="+obj.data[i]['id']+" selected>"+obj.data[i]['name']+"</option>");
                }else {
                  $(".aumphur").append("<option value="+obj.data[i]['id']+">"+obj.data[i]['name']+"</option>");
                }
              }
           }
        })
}

function ms_tumbon_get(pro_id,aumphur_id,tumbon_id) {
  var connectURL = getApi("master/ms_tumbon_get");
  $.ajax({
          url: connectURL,
          type: 'GET',
          data: {"pro_id":pro_id,"aumphur_id":aumphur_id}
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".tumbon").html("<option value='' disabled selected>"+lang.data.tumbon+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                if (obj.data[i]['id']==tumbon_id) {
                  $(".tumbon").append("<option value="+obj.data[i]['id']+" selected>"+obj.data[i]['name']+"</option>");
                }else {
                  $(".tumbon").append("<option value="+obj.data[i]['id']+">"+obj.data[i]['name']+"</option>");
                }
              }
           }
        })
}
