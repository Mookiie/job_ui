$(document).ready(function() {
  var uid = $("#uid").val()
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    var connect = getUrl('');
    window.location = connect;
  }else {
    getprofile_applicant(uid);
    getactivity_applicant(uid);
    Countactivity_applicant(uid);
  }
})

$("#row_show").change(function () {
  var uid = $("#uid").val()
  getactivity_applicant(uid)
})

function getprofile_applicant(uid) {
    console.log(`getprofile_applicant : ${uid}`);
    var connectURL = getApi("applicant/getProfileID");
    var formData = {"uid":uid}
    var token = localStorage.getItem('token');
    $.ajax({
      url: connectURL,
      type: 'POST',
      headers:{"token":token},
      data: formData,
      beforeSend:function(){
        preloader();
      }
    }).done(function(data) {
      closeloader()
      var obj = JSON.parse(data);
      console.log(obj);
      if (obj.status == '200') {
        $("#h_fname").text(obj.data.fname)
        $("#h_lname").text(obj.data.lname)
        $("#fname").text(obj.data.fname)
        $("#lname").text(obj.data.lname)
        $("#mobile").text(obj.data.tel)
        $("#email").text(obj.data.email)
        if ("birth_date" in obj.data) {
          console.log("ok");
          $('.birth_date').removeClass('disable-display')
          $("#birth_date").text(formatDate(obj.data.birth_date))
          $("#age").text(obj.data.ages+" ปี")
        }
        if ("job_description_1" in obj.data) {
          if (obj.data.job_description_1 != undefined) {
            $('.job_description_1').removeClass('disable-display')
            $("#job_description_1").text(obj.data.job_description_1[0]['job_description_name'])
          }
        }
        if ("sex" in obj.data) {
          $('.sex').removeClass('disable-display')
          $("#sex").text(obj.data.sex[0].name)
        }
        if ("pfix_code" in obj.data) {
          $('.pfix').removeClass('disable-display')
          $("#pfix").text(obj.data.pfix_code[0].name)
        }
        if ("married_status" in obj.data) {
          $('.married_status').removeClass('disable-display')
          $("#married_status").text(obj.data.married_status[0].name)
        }
        if ("degree" in obj.data) {
          $('.degree').removeClass('disable-display')
          $("#degree").text(obj.data.degree.name)
          $("#institute").text(obj.data.degree.institute)
          $("#major").text(obj.data.degree.major)
          $("#finish_year").text(obj.data.degree.finish_year)
          $("#grade").text(obj.data.degree.grade)
        }

      }else if (obj.status == '202') {
        $("#h_fname").text(obj.data.fname)
        $("#h_lname").text(obj.data.lname)
        $("#fname").text(obj.data.fname)
        $("#lname").text(obj.data.lname)
        $("#mobile").text(obj.data.tel)
        $("#email").text(obj.data.email)
      }
    })

}
var data_activity;
function getactivity_applicant(uid) {
  console.log(`getactivity_applicant : ${uid}`);
  // applicant/GetActivityByUid
  var connectURL = getApi("applicant/GetActivityByUid");
  var formData = {"uid":uid}
  var token = localStorage.getItem('token');
  $.ajax({
    url: connectURL,
    type: 'POST',
    headers:{"token":token},
    data: formData,
    beforeSend:function(){
      preloader();
    }
  }).done(function(data) {
    closeloader()
    var obj = JSON.parse(data);
    console.log(obj);
    if (obj.status == '200') {
      data_activity = obj.data
      getviewTables()
    }
  })
}

function Countactivity_applicant(uid) {
  var connectURL = getApi("applicant/CountActivityByUid");
  var formData = {"uid":uid}
  var token = localStorage.getItem('token');
  $.ajax({
    url: connectURL,
    type: 'POST',
    headers:{"token":token},
    data: formData,
    beforeSend:function(){
      preloader();
    }
  }).done(function(data) {
    closeloader()
    var obj = JSON.parse(data);
    console.log(obj);
    $("#count-apply").text(obj.regis)
    $("#count-login").text(obj.login)
    $("#last_login").text(formatDateFull(obj.last_login.log_date))
  })
}
// Table
function getviewTables(){
  $(".file-container").html("")
  $(".file-container").getTables({"data" : data_activity ,"page": 0 , "view" : $("#row_show").val() });
  $("#nowpage").html(1);
}

function nextpage(el){
  console.log("nextpage");
  var has = 0;
  var tr = $(".file-container tr");
  tr.each(function(index, el) {
      if($(el).css('display') != "none"){
         has++;
      }
  });
  var next = $("#nowpage")[0].innerHTML;
      next = parseFloat(next);
      next ++;
  var start = (next-1) * has;
  if (next <= parseFloat($(".totalpage")[0].innerHTML)) {
             $("#nowpage").html(next);
             $(".file-container").html("")
             $(".file-container").getTables({"data" : data_activity ,"page": start , "view" : $("#row_show").val() });
   }
}

function prevpage(el) {
  console.log("prevpage");
  var has = 0;
  var tr = $(".file-container tr");
  tr.each(function(index, el) {
      if($(el).css('display') != "none"){
         has++;
      }
  });
  var prev = $("#nowpage")[0].innerHTML;
      prev = parseFloat(prev);
      prev --;
  var start = (prev-1) * has;
  if (prev >  0) {
              $("#nowpage").html(prev);
              $(".file-container").html("")
              $(".file-container").getTables({"data" : data_activity ,"page": start , "view" : $("#row_show").val() });
  }
}

$.fn.getTables = function (obj){
  console.log(obj);
    var view = obj["view"];
        view = parseFloat(view);
    var data = obj["data"];
    var page = obj["page"];
    var row = data.length;
    for (var i = 0; i < data.length; i++) {
         var display = "";
         if (i <  (page)) {
             display = "none";
         }
         if (i > (view - 1)+page) {
             display = "none";
         }
         var row = genActivity_table(i,data[i],display);
         $(this).append(row);
    }
    var has = 0;
        var tr = $(".file-container tr");
        tr.each(function(index, el) {
            if($(el).css('display') != "none"){
               has++;
            }
        });
    var total = Math.ceil((tr.length)/view);
    $(".totalpage").html(total);
  }

// function getviewTables(ele){
//   var view = ele.val();
//   $(".file-container").html("")
//   $(".file-container").getTables({"data" : service_me ,"page": 0 , "view" : $("#row_show").val() });
//   $("#nowpage").html(1);
// }

function searchInTables(ele){

  var filter = ele.val().toUpperCase();
  var filter_obj = data_activity;
  var tr = $(".file-container tr");
  for (var r = 0; r < tr.length; r++) {
    var row = tr[r].dataset.row;
    var td = $("#tr_file_"+row+" td.sort");
    var str = "";
      for (var i = 0; i < td.length; i++) {
        str += td[i].innerHTML;
      }
      if(str.toUpperCase().indexOf(filter) > -1){
        $("#tr_file_"+row).css({"display":""});
      }else{
        $("#tr_file_"+row).css({"display":"none"});
      }
  }
}
