$(document).ready(function() {
  closeloader();
  $("#li-announce-me").removeClass("display-disable");
  $("#li-announce-favorite").addClass("display-disable");
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    var connect = getUrl('');
    window.location = connect;
  }
  loaddata();
  loadTop10View();
  loadTop10Apply()
  loaddatame();
});
function loaddata() {
  var connectURL = getApi('company/DashboardCount');
  var token = localStorage.getItem('token');
  $.ajax({
    url: connectURL,
    type: 'GET',
    headers:{"token":token}
  })
  .done(function(data) {
    var obj = JSON.parse(data);
    $("#count-announce").html(obj.data.announce);
    $("#count-announce-me").html(obj.data.announce_me);
    $(".advert_count_all").text(obj.data.announce)
    $(".advert_count_me").text(obj.data.announce_me)
  })
}
function loadTop10View() {
  $(".announce-tables").html("")
  var connectURL = getApi('company/DashboardTop10view');
  var token = localStorage.getItem('token');
  $.ajax({
    url: connectURL,
    type: 'GET',
    headers:{"token":token}
  })
  .done(function(data) {
    var obj = JSON.parse(data);
    for (var i = 0; i < obj.data.length; i++) {
        var row = genAnnounce_table(obj.data[i],(i+1));
        $(".announce-tables").append(row)
    }
  })
}
function loadTop10Apply() {
  $(".announce_me-table").html("")
  var connectURL = getApi('company/DashboardTop10Apply');
  var token = localStorage.getItem('token');
  $.ajax({
    url: connectURL,
    type: 'GET',
    headers:{"token":token}
  })
  .done(function(data) {
    var obj = JSON.parse(data);
    for (var i = 0; i < obj.data.length; i++) {
        var row = genAnnounce_me_table(obj.data[i],(i+1));
        $(".announce_me-table").append(row)
    }
  })
}
function loaddatame() {
  var connectURL = getApi('company/getannounceByCreate');
  var token = localStorage.getItem('token');
  $.ajax({
    url: connectURL,
    type: 'GET',
    headers:{"token":token}
  })
  .done(function(data) {
      $(".announceme").html('')
      $(".announcemefooter").html('')
    var obj = JSON.parse(data);
    console.log(obj);
    for (var i = 0; i < obj.data.length; i++) {
        var row = genCardAnnounceMe(obj.data[i]);
        var row_footer = genCardAnnounceMe(obj.data[i])
        $(".announceme").append(row)
        $(".announcemefooter").append(row_footer)
        $('.tooltipped').tooltip();
    }
  })
}
function checkIncome() {
  if ($("#income").is(':checked')) {
      rmErr('income');
      $("#income_min_group").css({"display":"none"});
      $("#income_max_group").css({"display":"none"});
  }else {
      $("#income_min_group").css({"display":""});
      $("#income_max_group").css({"display":""});
  }
}
function checkEditIncome() {
  if ($("#edit_income").is(':checked')) {
      rmErr('income');
      $("#edit_income_min_group").css({"display":"none"});
      $("#edit_income_max_group").css({"display":"none"});
  }else {
      $("#edit_income_min_group").css({"display":""});
      $("#edit_income_max_group").css({"display":""});
  }
}

function check_age() {
  if ($('#age_min').val()>99) {
    $('#age_min').val('99');
  }
  if ($('#age_max').val()>99) {
    $('#age_max').val('99');
  }
}
function check_edit_age() {
  if ($('#edit_age_min').val()>99) {
    $('#edit_age_min').val('99');
  }
  if ($('#edit_age_max').val()>99) {
    $('#edit_age_max').val('99');
  }
}
// function checkWork_full_time() {
//   if ($("#work_full_time").is(':checked') && $("#shift_work").is(':checked')) {
//     addErr("work_full_time",lang.error.select_blank_one)
//     addErr("shift_work",lang.error.select_blank_one)
//   }else if ($("#work_full_time").is(':checked')) {
//       $("#working_group").css({"display":""})
//       rmErr("work_full_time")
//       rmErr("shift_work")
//   }else {
//       $("#working_group").css({"display":"none"})
//   }
// }
function check_work() {
  if ($("#work_full_time").is(':checked') && $("#shift_work").is(':checked')) {
    addErr("work_full_time",lang.error.select_blank_one)
    addErr("shift_work",lang.error.select_blank_one)
  }else if (($("#shift_work").is(':checked'))||($("#work_full_time").is(':checked'))) {
    if ($("#shift_work").is(':checked')) {
      $("#working_group").css({"display":"none"})
    }
    if ($("#work_full_time").is(':checked')) {
      $("#working_group").css({"display":""})
    }
      rmErr("work_full_time")
      rmErr("shift_work")
  }
}
function edit_check_work() {
  if ($("#edit_work_full_time").is(':checked') && $("#edit_shift_work").is(':checked')) {
    addErr("edit_work_full_time",lang.error.select_blank_one)
    addErr("edit_shift_work",lang.error.select_blank_one)
  }else if (($("#edit_shift_work").is(':checked'))||($("#edit_work_full_time").is(':checked'))) {
    if ($("#edit_shift_work").is(':checked')) {
      $("#edit_working_group").css({"display":"none"})
    }
    if ($("#edit_work_full_time").is(':checked')) {
      $("#edit_working_group").css({"display":""})
    }
      rmErr("edit_work_full_time")
      rmErr("edit_shift_work")
  }
}

function add_announce() {
  // console.log("add");
  // company/AnnounceDialog
  var connectURL = getUrl("company/AnnounceDialog");
  var formData = {"ajax":true};
      formData["data"] = "" ;
  $.ajax({
          url: connectURL,
          type: 'POST',
          data: formData
        }).done(function(data) {
          closeloader()
          // console.log(data);
          var obj_modal = JSON.parse(data);
          $(".modal-area").append(obj_modal["modal"]);
          $("#AnnounceDialog").Modal({"backdrop":true,"static":true});
          ms_job_type_get();
          ms_job_description_1_get();
          ms_zone_get();
          ms_gender_get();
          ms_license_get();
          ms_education_get();
          ms_working_day_get();
          ms_position();
          ms_province_get();
          ms_staff();
          $("#add_province").change(function() {
            ms_aumphur_get($("#add_province").val(),'');
          })

          $('input#announce_title').characterCounter();
          $(".character-counter").css({"font-size":"15px","margin-bottom":"20px"})
          // $("#type_job,#job_description_1,#job_description_2,#zone,#gender,#license,#experience,#education,#working_day").material_select();
          // $('.datepicker').datepicker({
          //
          // });
          $('#working_start_date').bootstrapMaterialDatePicker({
            weekStart : 0,
            time: false,
            format : 'DD/MM/YYYY',
            setDate: moment()
          }).on('change', function(e, date) {
            $('#working_end_date').bootstrapMaterialDatePicker('setMinDate', date);
          });
          // $('.timepicker').pickatime({
          //     default: 'now',
          //     twelvehour: false, // change to 12 hour AM/PM clock from 24 hour
          //     donetext: 'OK',
          //     autoclose: true,
          //     // vibrate: true // vibrate the device when dragging clock hand
          // });
          $('#working_start').bootstrapMaterialDatePicker({
            date: false,
            format : 'HH:mm'
          });
          $('#working_end').bootstrapMaterialDatePicker({
             date: false,
             format : 'HH:mm'
          });

          $("#job_description_1").change(function() {
            ms_job_description_2_get($("#job_description_1").val());
            $("#job_description_2").removeAttr("disabled","");
          })
        })

}

function View_announce(announce_id) {
  // log_user('1',announce_id,'','','','','','','')
  var authtoken = localStorage.getItem('token');
  var url = getApi('getAnnounceById');
  $.ajax({
    url: url,
    type: 'GET',
    headers:{"token":authtoken},
    data:{"announce_id":announce_id},
  })
  .done(function(data) {
    var obj = JSON.parse(data);
    var data_detail = obj.data[0];
    console.log(data_detail);
    var connectURL = getUrl("DetailDialog");
    var formData = {"ajax":true};
        formData["data"] = data_detail ;
    $.ajax({
            url: connectURL,
            type: 'POST',
            data: formData
          }).done(function(data) {
            closeloader()
            var obj_modal = JSON.parse(data);
            $(".modal-area").append(obj_modal["modal"]);
            $("#DetailDialog").Modal({"backdrop":true,"static":true});
            $("#apply_job").css({"display":"none"})
          })
  })
}

function Trash_announce(announce_id) {
  var connectURL = getApi('company/updateStatusTrash');
  var token = localStorage.getItem('token');
  Swal({
  title: lang.error.warning_title,
  text: lang.error.warning_delete,
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: lang.error.warning_btn_confirm,
  cancelButtonText: lang.error.warning_btn_cancel,
}).then((result) => {
  if (result.value) {
    $.ajax({
      url: connectURL,
      type: 'POST',
      headers:{"token":token},
      data:{'announce_id':announce_id}
    })
    .done(function(data) {
      var obj = JSON.parse(data);
      $(".closemodal").trigger('click');
       if (obj.status==200) {
         log_user('6',announce_id,'','','','','','','','','','')
         Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
         loaddatame()
       }else {
         Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
       }
    })
  }
})
}

function Edit_announce(announce_id) {
  var authtoken = localStorage.getItem('token');
  var url = getApi('getAnnounceById');
  $.ajax({
    url: url,
    type: 'GET',
    headers:{"token":authtoken},
    data:{"announce_id":announce_id},
  })
  .done(function(data) {
    var obj = JSON.parse(data);
    var data_detail = obj.data[0];
    console.log(data_detail);
    var connectURL = getUrl("EditDetailDialog");
    var formData = {"ajax":true};
        formData["data"] = data_detail ;
    $.ajax({
            url: connectURL,
            type: 'POST',
            data: formData
          }).done(function(data) {
            closeloader()
            var obj_modal = JSON.parse(data);
            $(".modal-area-2").append(obj_modal["modal"]);
            $("#DetailDialog").Modal({"backdrop":true,"static":true});
            ms_job_type_get(data_detail.type_job[0]['id'])
            // ms_job_description_1_get(data_detail.job_description_1[0]['job_description_code_1'])
            // ms_job_description_2_get(data_detail.job_description_2[0]['job_description_code_1'],data_detail.job_description_2[0]['job_description_code_2'])
            // ms_zone_get(data_detail.zone[0]['id'])
            ms_gender_get(data_detail.gender[0]['id'])
            ms_license_get(data_detail.license[0]['driving_license_type_code'])
            ms_working_day_get(data_detail.working_day[0]['id'])
            ms_education_get(data_detail.education[0]['id'])
            ms_position(data_detail.job_description_1[0]['position_id']);
            ms_province_get(data_detail.zone.data[0]['pro_id']);
            ms_aumphur_get(data_detail.zone.data[0]['pro_id'],data_detail.zone_detail.data[0]['district_id']);
            ms_staff(data_detail.staff_id);
            $("#edit_province").change(function() {
              ms_aumphur_get($("#edit_province").val(),'');
            })
            if (data_detail.income_min == data_detail.income_max) {
                $("#edit_income").attr('checked','checked');
                checkEditIncome();
            }else {
              $("#edit_income_min").val(data_detail.income_min)
              $("#edit_income_max").val(data_detail.income_max)
            }
            console.log(data_detail.working_day[0].id);
            console.log(data_detail.working_end == data_detail.working_start && data_detail.working_day[0].id != '99');
            if (data_detail.working_end == data_detail.working_start && data_detail.working_day[0].id != '99') {
              $("#edit_shift_work").attr('checked','checked');
              edit_check_work()
            }else {
              $("#edit_work_full_time").attr('checked','checked');
              $("#edit_working_start").val(data_detail.working_start)
              $("#edit_working_end").val(data_detail.working_end)
              edit_check_work()
            }


            $('#edit_working_start_date').bootstrapMaterialDatePicker({
              weekStart : 0,
              time: false,
              format : 'DD/MM/YYYY',
              setDate: moment(data_detail.work_start_date)
            }).on('change', function(e, date) {
              $('#edit_working_end_date').bootstrapMaterialDatePicker({
                weekStart : 0,
                time: false,
                format : 'DD/MM/YYYY',
                setDate: moment(data_detail.work_end_date),
                setMinDate: date
              });
            });
            // $('.datepicker').datepicker({
            //   format:'dd/mm/yy'
            // });
            // $('.timepicker').pickatime({
            //     default: 'now',
            //     twelvehour: false, // change to 12 hour AM/PM clock from 24 hour
            //     donetext: 'OK',
            //     autoclose: true,
            //     // vibrate: true // vibrate the device when dragging clock hand
            // })
            $('#edit_working_start').bootstrapMaterialDatePicker({
              date: false,
              format:'HH:mm'
            });
            $('#edit_working_end').bootstrapMaterialDatePicker({
              date: false,
              format:'HH:mm'
            });
;
          })
  })
}

function Applicant_announce(announce_id) {
  var authtoken = localStorage.getItem('token');
  var url_regis = getApi('company/getApplicantByAnnounce');
  var url_view = getApi('company/getViewByAnnounce');
  $.ajax({
          url: url_view,
          type: 'GET',
          headers:{"token":authtoken},
          data:{"announce_id":announce_id},
          beforeSend:function(){
            preloader();
          }
        }).done(function(data) {
          var obj_view = JSON.parse(data);
          console.log(obj_view);
          var obj_announce ;
          $.ajax({
                  url: getApi('getAnnounceById'),
                  type: 'GET',
                  data:{"announce_id":announce_id},
                }).done(function(data) {
                  obj_announce = JSON.parse(data);
                $.ajax({
                        url: url_regis,
                        type: 'GET',
                        headers:{"token":authtoken},
                        data:{"announce_id":announce_id},
                      }).done(function(data) {
                        var obj_regis = JSON.parse(data);
                        console.log(obj_regis);
                        var connectURL = getUrl("countViewRegisDialog");
                        var formData = {"ajax":true};
                            formData["data"] = {"obj_view":obj_view.data,"obj_regis":obj_regis.data};
                            console.log(formData["data"]);
                        $.ajax({
                                url: connectURL,
                                type: 'POST',
                                data: formData
                              }).done(function(data) {
                                closeloader()
                                var obj_modal = JSON.parse(data);
                                $(".modal-area").append(obj_modal["modal"]);
                                $("#countViewRegisDialog").Modal({"backdrop":true,"static":true});
                                $('.tabs').tabs();
                                $('.collapsible').collapsible();
                                console.log(obj_view.count);
                                console.log(obj_regis.count);
                                console.log(profile_me);
                                ms_information();
                                console.log(obj_announce.data[0].staff_id);
                                $("#infor").change(function() {
                                  var link = getUrl("announce?announce="+btoa(btoa(btoa(announce_id))));
                                  link += "&link_name="+btoa(btoa(btoa(obj_announce.data[0].staff_id)));
                                  link += "&link_infor="+btoa(btoa(btoa($("#infor").val())));
                                  $(".row.link").removeClass("display-disable");
                                  $("#link").text(link);
                                })
                                if (obj_view.count>0) {
                                  for (var i = 0; i < obj_view.count; i++) {
                                    var row_view = getApplicantView(obj_view.data[i])
                                    $(".collapsible#content-view").append(row_view)
                                  }
                                }else {
                                  var row_none = getListNone()
                                  $(".collapsible#content-view").append(row_none)
                                }
                                if (obj_regis.count>0) {
                                  for (var i = 0; i < obj_regis.count; i++) {
                                    var row_apply = getApplicantApply(obj_regis.data[i])
                                    $(".collapsible#content-apply").append(row_apply)
                                  }
                                }else {
                                  var row_none = getListNone()
                                  $(".collapsible#content-apply").append(row_none)
                                }

                                $("#edit").click(function() {
                                  Edit_announce(announce_id)
                                })

                                $("#trash").click(function() {
                                  // console.log('trash');
                                  Trash_announce(announce_id)
                                })
                              })

                })
        })
  })
}

function view_applicant(uid) {
  var connect = getUrl("View?U="+btoa(btoa(btoa(uid))));
  window.open(connect)
}

function announce() {
  console.log("add announce");
  console.log($("#file_image")[0].files.length);
  var str = $("#announce_title").val();
  if (str.length > 100) {
    addErr("announce_title",lang.error.data_length)
  }
  if ($('#age_min').val()== "" || $('#age_max').val() == "") {
    addErr("age_min",lang.error.input_blank)
    addErr("age_max",lang.error.input_blank)
  }
  // else if ($('#age_min').val()>$('#age_max').val()) {
  //   addErr("age_min",lang.error.data_worng)
  //   addErr("age_max",lang.error.data_worng)
  // }else if (isNaN($('#age_min').val())) {
  //   addErr("age_min",lang.error.pattern_worng)
  // }else if (isNaN($('#age_max').val())) {
  //   addErr("age_max",lang.error.pattern_worng)
  // }
  if ($("#announce_title").val()=="") {
    addErr("announce_title",lang.error.input_blank)
  }
  // if ($("#type_job").val()==null) {
  //   addErrSelect("type_job",lang.error.select_blank)
  // }
  if ($("#detail").val()=="") {
    addErr("detail",lang.error.input_blank)
  }
  if ((!$("#work_full_time").is(':checked')) && (!$("#shift_work").is(':checked'))) {
    addErr("work_full_time",lang.error.select_blank_one)
    addErr("shift_work",lang.error.select_blank_one)
  }else if(($("#work_full_time").is(':checked')) && ($("#shift_work").is(':checked'))) {
    addErr("work_full_time",lang.error.select_blank)
    addErr("shift_work",lang.error.select_blank)
  }
  // if ($("#gender").val()==null) {
  //   addErrSelect("gender",lang.error.select_blank)
  // }
  // if ($("#zone").val()==null) {
  //   addErrSelect("zone",lang.error.select_blank)
  // }
  // if ($("#license").val()==null) {
  //   addErrSelect("license",lang.error.select_blank)
  // }
  // if ($("#education").val()==null) {
  //   addErrSelect("education",lang.error.select_blank)
  // }
  // if ($("#experience").val()==null) {
  //   addErrSelect("experience",lang.error.select_blank)
  // }
  if ($("#property").val()=="") {
    addErr("property",lang.error.input_blank)
  }
  if ($("#benefits").val()=="") {
    addErr("benefits",lang.error.input_blank)
  }
  if (!$("#income").is(':checked')) {
    // addErr("income",lang.error.input_blank)
  }else {
    if ($('#income_min').val()== "" || $('#income_max').val() == "") {
      addErr("income_min",lang.error.input_blank)
      addErr("income_max",lang.error.input_blank)
    }
    // else if ($('#income_min').val()>$('#income_max').val()) {
    //   addErr("income_min",lang.error.data_worng)
    //   addErr("income_max",lang.error.data_worng)
    // } else if (isNaN($('#income_min').val())) {
    //   addErr("income_min",lang.error.pattern_worng)
    // }else if (isNaN($('#income_max').val())) {
    //   addErr("income_max",lang.error.pattern_worng)
    // }
    if ($("#announce_title").val()=="") {
      addErr("announce_title",lang.error.input_blank)
    }

  }
  var staff_id ;
  if ($("#staff").val() == "") {
    staff_id = '1101401852618';
  }else {
    staff_id = $("#staff").val()
  }
  var formData = {  "announce_title":$("#announce_title").val(),
                    "type_job":$('#type_job').val(),
                    "working_start_date":$('#working_start_date').val(),
                    "working_end_date":$('#working_end_date').val(),
                    "detail":$('#detail').val(),
                    "working_day":$('#working_day').val(),
                    "working_start":($('#working_start').val() == '') ? "00:00" : $('#working_start').val(),
                    "working_end":($('#working_end').val() == '') ? "00:00" : $('#working_end').val(),
                    "age_min":$('#age_min').val(),
                    "age_max":$('#age_max').val(),
                    "gender":$('#gender').val(),
                    // "job_description_1":$('#job_description_1').val(),
                    "job_description_1":$('#job_position').val(),
                    "job_description_2":'',
                    // "job_description_2":$('#job_description_2').val(),
                    "license":$('#license').val()==null ? "99":$('#license').val(),
                    "education":$('#education').val(),
                    "property":$('#property').val(),
                    "experience":$('#experience').val(),
                    "income_min":($('#income_min').val() == '') ? "0" : $('#income_min').val(),
                    "income_max":($('#income_max').val() == '') ? "0" : $('#income_max').val(),
                    "benefits":$('#benefits').val(),
                    "remark":$('#remark').val(),
                    "zone_detail":$('#add_aumphur').val(),
                    "zone":$('#add_province').val(),
                    "staff_id":staff_id
                  };

    console.log(formData);

  if ((str.length < 100)
  && ($("#work_full_time").is(':checked') || $("#shift_work").is(':checked'))
  && $('#detail').val() != ""
  && $('#zone').val() != ""
  && $('#age_min').val() != ""
  && $('#age_max').val() != ""
  && $('#gender').val() != ""
  && $('#license').val() != ""
  && $('#education').val() != ""
  && $('#property').val() != ""
  && $('#experience').val() != ""
  && $('#benefits').val() != ""
  && $('#job_position').val() != ""
  // && $('#job_description_1').val() != ""
  // && $('#job_description_2').val() != ""
  && ($("#income").is(':checked')||($("#income_min").val()!="" && $("#income_max").val()!=""))
  ) {
    var connectURL = getApi("company/AnnounceAdd");
    var token = localStorage.getItem('token');
    $.ajax({
            url: connectURL,
            type: 'POST',
            data: formData,
            headers:{"token":token},
            beforeSend:function(){
              preloader();
            }
          }).done(function(data) {
            closeloader();
            var obj = JSON.parse(data);
             if (obj.status==200) {
               console.log(obj);
               if ($("#file_image")[0].files.length > 0) {
                 upload_file_image(obj.id)
               }else {
                 $(".closemodal").trigger('click');
                 loaddata();
                 loadTop10View();
                 loadTop10Apply()
                 loaddatame();
                 Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
               }
             }else {
               Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
             }
          })
  }
}

function edit_job(announce_id) {
  console.log(announce_id);
  console.log("edit announce");
  console.log($('#edit_income_max').val());
  var str = $("#edit_announce_title").val();
  if (str.length > 100) {
    addErr("edit_announce_title",lang.error.data_length)
    console.log(formData);
  }
  if ($('#edit_age_min').val()== "" || $('#edit_age_max').val() == "") {
    addErr("edit_age_min",lang.error.input_blank)
    addErr("edit_age_max",lang.error.input_blank)
  }
  // else if ($('#edit_age_min').val()>$('#edit_age_max').val()) {
  //   addErr("edit_age_min",lang.error.data_worng)
  //   addErr("edit_age_max",lang.error.data_worng)
  // }else if (isNaN($('#edit_age_min').val())) {
  //   addErr("edit_age_min",lang.error.pattern_worng)
  // }else if (isNaN($('#edit_age_max').val())) {
  //   addErr("edit_age_max",lang.error.pattern_worng)
  // }
  if ($("#edit_announce_title").val()=="") {
    addErr("edit_announce_title",lang.error.input_blank)
  }
  // if ($("#type_job").val()==null) {
  //   addErrSelect("type_job",lang.error.select_blank)
  // }
  if ($("#edit_detail").val()=="") {
    addErr("edit_detail",lang.error.input_blank)
  }
  if ((!$("#edit_work_full_time").is(':checked')) && (!$("#edit_shift_work").is(':checked'))) {
    addErr("edit_work_full_time",lang.error.select_blank_one)
    addErr("edit_shift_work",lang.error.select_blank_one)
  }else if(($("#edit_work_full_time").is(':checked')) && ($("#edit_shift_work").is(':checked'))) {
    addErr("edit_work_full_time",lang.error.select_blank)
    addErr("edit_shift_work",lang.error.select_blank)
  }
  // if ($("#gender").val()==null) {
  //   addErrSelect("gender",lang.error.select_blank)
  // }
  // if ($("#zone").val()==null) {
  //   addErrSelect("zone",lang.error.select_blank)
  // }
  // if ($("#license").val()==null) {
  //   addErrSelect("license",lang.error.select_blank)
  // }
  // if ($("#education").val()==null) {
  //   addErrSelect("education",lang.error.select_blank)
  // }
  // if ($("#experience").val()==null) {
  //   addErrSelect("experience",lang.error.select_blank)
  // }
  if ($("#edit_property").val()=="") {
    addErr("edit_property",lang.error.input_blank)
  }
  if ($("#edit_benefits").val()=="") {
    addErr("edit_benefits",lang.error.input_blank)
  }
  if (!$("#edit_income").is(':checked')) {
    // addErr("income",lang.error.input_blank)
  }else {
    if ($('#edit_income_min').val()== "" || $('#edit_income_max').val() == "") {
      addErr("edit_income_min",lang.error.input_blank)
      addErr("edit_income_max",lang.error.input_blank)
    }
    // else if ($('#edit_income_min').val()>$('#edit_income_max').val()) {
    //   addErr("edit_income_min",lang.error.data_worng)
    //   addErr("edit_income_max",lang.error.data_worng)
    // }else if (isNaN($('#edit_income_min').val())) {
    //   addErr("edit_income_min",lang.error.pattern_worng)
    // }else if (isNaN($('#edit_income_max').val())) {
    //   addErr("edit_income_max",lang.error.pattern_worng)
    // }
    if ($("#edit_announce_title").val()=="") {
      addErr("edit_announce_title",lang.error.input_blank)
    }

  }
  var formData = {  "announce_id":announce_id,
                    "announce_title":$("#edit_announce_title").val(),
                    "type_job":$('#edit_type_job').val(),
                    "working_start_date":$('#edit_working_start_date').val(),
                    "working_end_date":$('#edit_working_end_date').val(),
                    "detail":$('#edit_detail').val(),
                    "working_day":$('#edit_working_day').val(),
                    "working_start":($('#edit_working_start').val() == '') ? "00:00" : $('#edit_working_start').val(),
                    "working_end":($('#edit_working_end').val() == '') ? "00:00" : $('#edit_working_end').val(),
                    "age_min":$('#edit_age_min').val(),
                    "age_max":$('#edit_age_max').val(),
                    "gender":$('#edit_gender').val(),
                    "job_description_1":$('#job_position').val(),
                    "job_description_2":'',
                    "license":$('#edit_license').val(),
                    "education":$('#edit_education').val(),
                    "property":$('#edit_property').val(),
                    "experience":$('#edit_experience').val(),
                    "income_min":($('#edit_income_min').val() == '') ? "0" : $('#edit_income_min').val(),
                    "income_max":($('#edit_income_max').val() == '') ? "0" : $('#edit_income_max').val(),
                    "benefits":$('#edit_benefits').val(),
                    "remark":$('#edit_remark').val(),
                    "zone_detail":$('#edit_aumphur').val(),
                    "zone":$('#edit_province').val(),
                    "staff_id":$("#staff").val(),
                  };
  console.log(formData);


  if ((str.length < 100)
  && ($("#edit_work_full_time").is(':checked') || $("#edit_shift_work").is(':checked'))
  && $('#edit_detail').val() != ""
  && $('#edit_zone').val() != ""
  && $('#edit_age_min').val() != ""
  && $('#edit_age_max').val() != ""
  && $('#edit_gender').val() != ""
  && $('#edit_license').val() != ""
  && $('#edit_education').val() != ""
  && $('#edit_property').val() != ""
  && $('#edit_experience').val() != ""
  && $('#edit_benefits').val() != ""
  && ($("#edit_income").is(':checked')||($("#edit_income_min").val()!="" && $("#edit_income_max").val()!=""))
  ) {

    var connectURL = getApi("company/AnnounceEdit");
    var token = localStorage.getItem('token');
    $.ajax({
            url: connectURL,
            type: 'POST',
            data: formData,
            headers:{"token":token},
            beforeSend:function(){
              preloader();
            }
          }).done(function(data) {
            closeloader();
            var obj = JSON.parse(data);
             if (obj.status==200) {
               $(".closemodal").trigger('click');
               loaddata();
               loadTop10View();
               loadTop10Apply()
               loaddatame();
               log_user('5',announce_id,'','','','','','','','','')
               Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
             }else {
               Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
             }
          })
  }
}

function upload_file_image(id) {
  var files = $("#file_image")[0].files;
  var type = files[0].name;
      type = type.split(".");
      type = type[type.length - 1];
      type = type.toLowerCase();
  var submitUrl = getApi('company/announce_files');
  var ajax = new XMLHttpRequest();
  var token = localStorage.getItem('token');
  var formData = new FormData();
      formData.append('file[]',files[0]);
      formData.append('id',id)
  ajax.upload.addEventListener("progress",function(event){
      if(event.lengthComputable){
          //
        }
  });
  ajax.addEventListener("readystatechange",function(event){
    if(this.readyState ==4){
        if(this.status == 200){
          closeloader()
          console.log(this.responseText);
           var obj = JSON.parse(this.responseText);
           if (obj.status == 200) {
             // swal({
             //        title: 'Success!',
             //        text: "",
             //        type: 'success',
             //        showCancelButton: false,
             //        confirmButtonColor: '#3085d6',
             //        confirmButtonText: 'OK!'
             //     }).then((result) => {
             //        if (result.value) {
             //          location.reload();
             //        }
             //    })
             $(".closemodal").trigger('click');
             loaddata();
             loadTop10View();
             loadTop10Apply()
             loaddatame();
             Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
           }
      }
    }
  });
  ajax.open('POST',submitUrl);
  ajax.setRequestHeader("token",token);
  ajax.send(formData);
}

function copyLink(element) {
  var $temp = $("<input>");
 $("body").append($temp);
 $temp.val($(element).text()).select();
 document.execCommand("copy");
 $temp.remove();
}
