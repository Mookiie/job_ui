$(document).ready(function() {
getCompanyDetail()
$('.tabs').tabs();
UserByCompany();
DashboardCount()
loaddata();
});

function loaddata() {
  var connectURL = getApi('company/DashboardCount');
  var token = localStorage.getItem('token');
  $.ajax({
    url: connectURL,
    type: 'GET',
    headers:{"token":token}
  })
  .done(function(data) {
    var obj = JSON.parse(data);
    $(".advert_count_all").text(obj.data.announce)
    $(".advert_count_me").text(obj.data.announce_me)
  })
}

function getCompanyDetail() {
  var connectURL = getApi("company/getCompanyDetail");
  var token = localStorage.getItem('token');
  $.ajax({
          url: connectURL,
          type: 'GET',
          headers:{"token":token}
        }).done(function(data) {
          closeloader();
          var obj = JSON.parse(data);
           if (obj.status==200) {
            if (obj.count == 0) {
              $("#add_detail_group").css({"display":""})
              $("#edit_detail_group").css({"display":"none"})
              $("#data_not_found").css({"display":""})
              $("#data_found").css({"display":"none"})
            }else {
              console.log(obj.data);
              $("#add_detail_group").css({"display":"none"})
              $("#edit_detail_group").css({"display":""})
              $("#data_not_found").css({"display":"none"})
              $("#data_found").css({"display":""})
              $('#company_name').html(profile_me.data.company.company_name);
              $('#tin').html(profile_me.data.company.tin);
              $('#company_kind').html(obj.data.company_kind.name);
              $('#company_kind_detail').html(obj.data.company_kind_detail);
              $('#company_tel').html(obj.data.company_tel);
              var address = "";
              if (obj.data.company_address != '') {
                 address +=  obj.data.company_address
              }if (obj.data.tumbon_id != '') {
                 address += " "+lang.data.tumbon+" "+obj.data.tumbon_id.name
              }if (obj.data.aumphur_id != '') {
                address += " "+lang.data.aumphur+" "+obj.data.aumphur_id.name
              }if (obj.data.pro_id != '') {
                address += " "+lang.data.province+" "+obj.data.pro_id.name
              }if ((obj.data.company_address != '')&&(obj.data.tumbon_id != '')&&(obj.data.aumphur_id != '')&&(obj.data.pro_id != '')) {
                address += " "+obj.data.tumbon_id.zipcode
              }
              $('#company_address').html(address);
              $('#web').html(nl2br(obj.data.web_site));
              $('#travel').html(nl2br(obj.data.travel));
              $("#map").attr("src",obj.data.map);
            }
           }
        })
}

function add_detailDialog() {
  var connectURL = getUrl("company/DetailDialog");
  var formData = {"ajax":true};
      formData["data"] = "" ;
  $.ajax({
          url: connectURL,
          type: 'POST',
          data: formData
        }).done(function(data) {
          closeloader()
          var obj_modal = JSON.parse(data);
          $(".modal-area").append(obj_modal["modal"]);
          $("#DetailDialog").Modal({"backdrop":true,"static":true});
          ms_company_kind_get('');
          ms_province_get('')
          console.log(profile_me);
          console.log(profile_me.data.company);
          $("#comp_name").val(profile_me.data.company.company_name);
          $("#comp_tin").val(profile_me.data.company.tin);
          $('input#comp_name,input#tin,input#comp_tel').characterCounter();
          $(".character-counter").css({"font-size":"15px","margin-bottom":"20px"})
          $("#comp_kind").change(function () {
            if ($("#comp_kind").val() == '999') {
              $("#comp_kind_detail_group").css("display","")
            }else {
              $("#comp_kind_detail_group").css("display","none")
            }
          })
          $("#comp_province").change(function() {
            ms_aumphur_get($("#comp_province").val(),'');
          })
          $("#comp_aumphur").change(function() {
            ms_tumbon_get($("#comp_province").val(),$("#comp_aumphur").val(),'')
          })

        })
}

function add_detail() {
  console.log("add detail");
  console.log($('#comp_tel').val());
  if ($("#comp_name").val()=="") {addErr("comp_name",lang.error.input_blank)}
  // if ($('#tin').val() == '') { addErr("tin",lang.error.input_blank);}
  // else if (!number13($('#tin').val())) { addErr("tin",lang.error.tin_worng);}
  if ($('#comp_tel').val() == '') { addErr("comp_tel",lang.error.input_blank);}
  else if (!phonenumber($('#comp_tel').val())) { addErr("comp_tel",lang.error.phone_worng);}
  // if ($('#province').val() == '') { addErr("province",lang.error.input_blank);}
  // if ($('#aumphur').val() == '') { addErr("aumphur",lang.error.input_blank);}
  // if ($('#tumbon').val() == '') { addErr("tumbon",lang.error.input_blank);}
  if ($("#comp_kind").val() == '999') {
    if($("#comp_kind_detail").val()==""){addErr("comp_kind_detail",lang.error.input_blank);}
  }

  if ($("#comp_name").val()!=""
      && $('#comp_tin').val() != ''
      && $('#comp_tel').val() != ''
      && ( ($("#comp_kind").val() != '999') || ($("#comp_kind_detail").val() != ""))
     ) {
    var formData = { "company_name":$('#comp_name').val(),
                         "tin":$('#comp_tin').val(),
                         "company_tel":$('#comp_tel').val(),
                         "web_site":$('#comp_web').val(),
                         "company_address":$('#comp_address').val(),
                         "pro_id":$('#comp_province').val(),
                         "aumphur_id":$('#comp_aumphur').val(),
                         "tumbon_id":$('#comp_tumbon').val(),
                         "company_kind":$('#comp_kind').val(),
                         "company_kind_detail":$('#comp_kind_detail').val(),
                         "travel":$('#comp_travel').val(),
                         "company_detail":""
                      };
    console.log(formData);
    console.log($("#file_map").val());
    var connectURL = getApi("company/detail");
    var token = localStorage.getItem('token');
    $.ajax({
            url: connectURL,
            type: 'POST',
            data: formData,
            headers:{"token":token},
            beforeSend:function(){
              preloader();
            }
          }).done(function(data) {
            closeloader();
            var obj = JSON.parse(data);
             if (obj.status==200) {
               if ($("#file_map").val() != '') {
                 upload_file(obj.id)
               }
               $(".closemodal").trigger('click');
               Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
             }else {
               Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
             }
          })
  }
}

function upload_file(id) {
  var files = $("#file_map")[0].files;
  var type = files[0].name;
      type = type.split(".");
      type = type[type.length - 1];
      type = type.toLowerCase();
  var submitUrl = getApi('company/company_detail_files');
  var ajax = new XMLHttpRequest();
  var token = localStorage.getItem('token');
  var formData = new FormData();
      formData.append('file[]',files[0]);
      formData.append('id',id)
  ajax.upload.addEventListener("progress",function(event){
      if(event.lengthComputable){
          //
        }
  });
  ajax.addEventListener("readystatechange",function(event){
    if(this.readyState ==4){
        if(this.status == 200){
          closeloader()
           var obj = JSON.parse(this.responseText);
           if (obj.status == 200) {
             swal({
                    title: 'Success!',
                    text: "",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                 }).then((result) => {
                    if (result.value) {
                      location.reload();
                    }
                })
           }
      }
    }
  });
  ajax.open('POST',submitUrl);
  ajax.setRequestHeader("token",token);
  ajax.send(formData);
}

function edit_detailDialog() {
console.log("edit_detailDialog");
var connectURL = getApi("company/getCompanyDetail");
var token = localStorage.getItem('token');
$.ajax({
        url: connectURL,
        type: 'GET',
        headers:{"token":token}
      }).done(function(data) {
        closeloader();
        var obj = JSON.parse(data);
         if (obj.status==200) {
           console.log(obj);
           var connectURL = getUrl("company/DetailEditDialog");
           var formData = {"ajax":true};
               formData["data"] = obj["data"] ;
           $.ajax({
                   url: connectURL,
                   type: 'POST',
                   data: formData
                 }).done(function(data) {
                   closeloader()
                   var obj_modal = JSON.parse(data);
                   $(".modal-area").append(obj_modal["modal"]);
                   $("#DetailEditDialog").Modal({"backdrop":true,"static":true});
                   $("#etravel").val(obj["data"]['travel']);
                   ms_company_kind_get(obj["data"]['company_kind']['id']);
                   ms_province_get(obj["data"]['pro_id']['id']);
                   ms_aumphur_get(obj["data"]['pro_id']['id'],obj["data"]['aumphur_id']['id'])
                   ms_tumbon_get(obj["data"]['pro_id']['id'],obj["data"]['aumphur_id']['id'],obj["data"]['tumbon_id']['id'])
                   $("#province").change(function() {
                     ms_aumphur_get($("#province").val(),'');
                   })
                   $("#aumphur").change(function() {
                     ms_tumbon_get($("#province").val(),$("#aumphur").val(),'')
                   })

                 })
         }
       })

}

function edit_detail() {
  if ($("#ecompany_name").val()=="") {addErr("ecompany_name",lang.error.input_blank)}
  // if ($('#tin').val() == '') { addErr("tin",lang.error.input_blank);}
  // else if (!number13($('#tin').val())) { addErr("tin",lang.error.tin_worng);}
  if ($('#ecompany_tel').val() == '') { addErr("ecompany_tel",lang.error.input_blank);}
  else if (!phonenumber($('#ecompany_tel').val())) { addErr("ecompany_tel",lang.error.phone_worng);}
  // if ($('#province').val() == '') { addErr("province",lang.error.input_blank);}
  // if ($('#aumphur').val() == '') { addErr("aumphur",lang.error.input_blank);}
  // if ($('#tumbon').val() == '') { addErr("tumbon",lang.error.input_blank);}
  if ($("#ecompany_kind").val() == '999') {
    if($("#ecompany_kind_detail").val()==""){addErr("ecompany_kind_detail",lang.error.input_blank);}
  }

  if ($("#ecompany_name").val()!=""
      && $('#etin').val() != ''
      && $('#ecompany_tel').val() != ''
      && ( ($("#ecompany_kind").val() != '999') || ($("#ecompany_kind_detail").val() != ""))
     ) {
    var formData = { "company_name":$('#ecompany_name').val(),
                         "tin":$('#etin').val(),
                         "company_tel":$('#ecompany_tel').val(),
                         "web_site":$('#eweb').val(),
                         "company_address":$('#ecompany_address').val(),
                         "pro_id":$('#province').val(),
                         "aumphur_id":$('#aumphur').val(),
                         "tumbon_id":$('#tumbon').val(),
                         "company_kind":$('#ecompany_kind').val(),
                         "company_kind_detail":$('#ecompany_kind_detail').val(),
                         "travel":$('#etravel').val(),
                         "company_detail":""
                      };
    console.log(formData);
    var connectURL = getApi("company/EditCompany");
    var token = localStorage.getItem('token');
    $.ajax({
            url: connectURL,
            type: 'POST',
            data: formData,
            headers:{"token":token},
            beforeSend:function(){
              preloader();
            }
          }).done(function(data) {
            closeloader();
            var obj = JSON.parse(data);
             if (obj.status==200) {
               if ($("#file_map").val() != '') {
                 upload_file(obj.id)
               }else {
                 $(".closemodal").trigger('click');
                 getCompanyDetail()
                 Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
               }
             }else {
               Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
             }
          })
  }
}

function invite_Dialog() {
  console.log("invite_Dialog");
  var connectURL = getUrl("company/getInviteDialog");
  var formData = {"ajax":true};
      formData["data"] = "" ;
  $.ajax({
          url: connectURL,
          type: 'POST',
          data: formData
        }).done(function(data) {
          closeloader()
          var obj_modal = JSON.parse(data);
          $(".modal-area").append(obj_modal["modal"]);
          $("#InviteDialog").Modal({"backdrop":true,"static":true});
          ms_level_get();
        })
}

function Invite() {
console.log("invite");
if ($('#fname').val() == '') { addErr("fname",lang.error.input_blank);}
if ($('#lname').val() == '') { addErr("lname",lang.error.input_blank);}
if ($('#email').val() == '') { addErr("email",lang.error.input_blank);}
else if (!validateEmail($('#email').val())) { addErr("email",lang.error.email_worng) }
if ($('#tel').val() == '') { addErr("tel",lang.error.input_blank);}
else if (!phonenumber($('#tel').val())) { addErr("tel",lang.error.phone_worng);}
if (($('#fname').val() != '')
    &&($('#lname').val() != '')
    &&($('#email').val() != '')
    &&($('#tel').val() != '')
    &&(validateEmail($('#email').val()))
    &&(phonenumber($('#tel').val()))
   ) {
      var formData = { "fname":$('#fname').val(),
                        "lname":$('#lname').val(),
                        "email":$('#email').val(),
                        "tel":$('#tel').val(),
                        "level":$('#level').val()
                     };

    var connectURL = getApi("users/InviteUsers");
    var token = localStorage.getItem('token');
      $.ajax({
        url: connectURL,
        type: 'POST',
        data: formData,
        headers:{"token":token},
        beforeSend:function(){
          preloader();
        }
      }).done(function(data) {
        var obj = JSON.parse(data);
        closeloader()
         if (obj.status==200) {
           $(".closemodal").trigger('click');
           UserByCompany();
           Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success, 40000);
         }else if (obj.status==202) {
           addErr("email",lang.error.email_duplicate);
         }else {
           Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning, 40000);
         }
      })
    }
}

function edit(id) {
  console.log("edit");
  console.log(id);
}

function edit_user(id) {
  var api_url = getApi('users/editUser');
  var token = localStorage.getItem('token');
  var formData = {  "fname":$("#fname").val(),
                    "lname":$("#lname").val(),
                    "tel":$("#tel").val(),
                    "uid":id
                 }
                    console.log(formData);
  $.ajax({
          url: api_url,
          type: 'POST',
          data: formData,
          headers:{"token":token},
          beforeSend:function(){
            preloader();
          }
        }).done(function(data) {
          closeloader();
          var obj = JSON.parse(data);
           if (obj.status==200) {
             $(".closemodal").trigger('click');
             getCompanyDetail()
             Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
           }else {
             Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
           }
        })
}

function view(id) {
  console.log("view");
  console.log(id);
  var profile = getApi('users/getProfileID');
  var token = localStorage.getItem('token');
  $.ajax({
    url: profile,
    type: 'GET',
    data:{"uid":id},
  })
  .done(function(data) {
    var obj = JSON.parse(data);
    var connectURL = getUrl("profile");
    var formData = {"ajax":true};
        formData["data"] = obj.data ;
        // formData["data"] = obj['data'] ;
    $.ajax({
            url: connectURL,
            type: 'POST',
            data: formData
          }).done(function(data) {
            var obj_modal = JSON.parse(data);
            $(".modal-area").append(obj_modal["modal"]);
            $("#ProfileDialog").Modal({"backdrop":true,"static":true});
            $('.tabs-member').tabs();
            $(".details").click(function functionName() {
              $('#editProfile').css({"display":"none"})
            })
            $(".edit").click(function functionName() {
              $('#editProfile').css({"display":""})
            })
            $("#editProfile").click(function () {
              console.log("save");
              console.log(id);
              edit_user(id)
            })

          })// load modal
  })
}

function loaddata(id) {
  var api_url = getApi('users/UserByCompany');
  var token = localStorage.getItem('token');
  $.ajax({
    url: api_url,
    type: 'POST',
    data: {"id":id},
    headers:{"token":token},
  })
  .done(function(data) {
    var obj = JSON.parse(data);
    console.log(obj);
  })
}

function trash(id) {
  console.log("trash");
  console.log(id);
  var connectURL = getApi("users/updateStatus");
  var token = localStorage.getItem('token');
  $.ajax({
          url: connectURL,
          type: 'POST',
          headers:{"token":token},
          data: {"uid":id,"status":'0'}
        }).done(function(data) {
          closeloader()
          var obj = JSON.parse(data);
          if (obj.status==200) {
            UserByCompany();
            Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
          }else {
            Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
          }
        })
}

function restore(id) {
  console.log("restore");
  console.log(id);
  var connectURL = getApi("users/updateStatus");
  var token = localStorage.getItem('token');
  $.ajax({
          url: connectURL,
          type: 'POST',
          headers:{"token":token},
          data: {"uid":id,"status":'1'},
          beforeSend:function(){
            preloader();
          }
        }).done(function(data) {
          closeloader()
          var obj = JSON.parse(data);
          if (obj.status==200) {
            UserByCompany();
            Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
          }else {
            Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
          }
        })
}

function resend(id) {
  console.log("resend");
  console.log(id);
  var connectURL = getApi("users/resendConfirm");
  var token = localStorage.getItem('token');
  $.ajax({
          url: connectURL,
          type: 'POST',
          headers:{"token":token},
          data: {"uid":id},
          beforeSend:function(){
            preloader();
          }
        }).done(function(data) {
          closeloader()
          var obj = JSON.parse(data);
          if (obj.status==200) {
            Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
          }else {
            Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
          }
        })
}

function ms_level_get() {
  var connectURL = getApi("master/ms_level_get");
  $.ajax({
          url: connectURL,
          type: 'GET'
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
              $(".level").html("<option value='' disabled selected>"+lang.data.level+"</option>");
              for (var i = 0; i < obj.data.length; i++) {
                $(".level").append("<option value="+obj.data[i]['id']+">"+obj.data[i]['name']+"</option>");
              }
           }
        })
}

function DashboardCount() {
  var connectURL = getApi("users/DashboardCount");
  var token = localStorage.getItem('token');
  $.ajax({
          url: connectURL,
          type: 'GET',
          headers:{"token":token}
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
             console.log(obj.data);
             $("#count-all").html(obj.data.all);
             $("#count-online").html(obj.data.online);
             $("#count-offline").html(obj.data.offline);
            }
        })
}

function Copylink() {
  var copyText = document.getElementById("link");
  copyText.select();
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}
// data users
var userdata;
function UserByCompany() {
  // users/UserByCompany
  var connectURL = getApi("users/UserByCompany");
  var token = localStorage.getItem('token');
  $.ajax({
          url: connectURL,
          type: 'POST',
          headers:{"token":token}
        }).done(function(data) {
          var obj = JSON.parse(data);
           if (obj.status==200) {
             userdata = obj.data
             console.log(userdata);
             getviewTables()
            }
        })

}

// Table
function getviewTables(){
  $(".file-container").html("")
  $(".file-container").getTables({"data" : userdata ,"page": 0 , "view" : $("#row_show").val() });
  $("#nowpage").html(1);
}

function nextpage(el){
  console.log("nextpage");
  var has = 0;
  var tr = $(".file-container tr");
  tr.each(function(index, el) {
      if($(el).css('display') != "none"){
         has++;
      }
  });
  var next = $("#nowpage")[0].innerHTML;
      next = parseFloat(next);
      next ++;
  var start = (next-1) * has;
  if (next <= parseFloat($(".totalpage")[0].innerHTML)) {
             $("#nowpage").html(next);
             $(".file-container").html("")
             $(".file-container").getTables({"data" : userdata ,"page": start , "view" : $("#row_show").val() });
   }
}

function prevpage(el) {
  console.log("prevpage");
  var has = 0;
  var tr = $(".file-container tr");
  tr.each(function(index, el) {
      if($(el).css('display') != "none"){
         has++;
      }
  });
  var prev = $("#nowpage")[0].innerHTML;
      prev = parseFloat(prev);
      prev --;
  var start = (prev-1) * has;
  if (prev >  0) {
              $("#nowpage").html(prev);
              $(".file-container").html("")
              $(".file-container").getTables({"data" : userdata ,"page": start , "view" : $("#row_show").val() });
  }
}

$.fn.getTables = function (obj){
    var view = obj["view"];
        view = parseFloat(view);
    var data = obj["data"];
    var page = obj["page"];
    var row = data.length;
    for (var i = 0; i < data.length; i++) {
         var display = "";
         if (i <  (page)) {
             display = "none";
         }
         if (i > (view - 1)+page) {
             display = "none";
         }
         var row = genRowUser(i,data[i],display);
         $(this).append(row);
    }
    var has = 0;
        var tr = $(".file-container tr");
        tr.each(function(index, el) {
            if($(el).css('display') != "none"){
               has++;
            }
        });
    var total = Math.ceil((tr.length)/view);
    $(".totalpage").html(total);
  }

// function getviewTables(ele){
//   var view = ele.val();
//   $(".file-container").html("")
//   $(".file-container").getTables({"data" : service_me ,"page": 0 , "view" : $("#row_show").val() });
//   $("#nowpage").html(1);
// }

function searchInTables(ele){
  var filter = ele.val().toUpperCase();
  var filter_obj = userdata;
  var tr = $(".file-container tr");
  for (var r = 0; r < tr.length; r++) {
    var row = tr[r].dataset.row;
    var td = $("#tr_file_"+row+" td.sort");
    var str = "";
      for (var i = 0; i < td.length; i++) {
        str += td[i].innerHTML;
      }
      if(str.toUpperCase().indexOf(filter) > -1){
        $("#tr_file_"+row).css({"display":""});
      }else{
        $("#tr_file_"+row).css({"display":"none"});
      }
  }
}
