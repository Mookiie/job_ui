var data_user
var data_announce
var data_announce_id
$(document).ready(function() {
  var link_infor = $("#link_infor").val();
  var infor = localStorage.getItem('infor');
  var authtoken = localStorage.getItem('token');
  if (infor == null) {
    localStorage.setItem('infor',link_infor);
  }
  if (authtoken == null) {
    localStorage.setItem('infor',link_infor);
  }
  data_user = localStorage.getItem('data');
    closeloader();
    getCountAnnounce()
    data_announce_id = $("#announce_id").val();
    count($("#announce_id").val())
    view_detail($("#announce_id").val())
    $("#nav-search").addClass("display-disable");
    
})
$(".advert .content").scroll(function () {
  var scroll_top = $(".advert .content").scrollTop();
  if(scroll_top > 0 ){
    if (!$(".advert .menu").hasClass("opopacity-bg")) {
      $(".advert .menu").addClass("opopacity-bg")
    }
  }else{
    $(".advert .menu").removeClass("opopacity-bg")
  }
})
$("#regis_job").click(function() {
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.PleaseLogin,6000);
  }else {
    if (data_user == "applicant") {
      apply_job($("#announce_id").val())
    }else {
      Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.permission,6000);
    }
  }
})
$("#favorite_job").click(function() {
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.PleaseLogin,6000);
  }else {
    if (data_user == "Applicant") {
      favorite_job(  $("#announce_id").val())
    }else {
      Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.permission,6000);
    }
  }
})
function count(announce_id) {
  var authtoken = localStorage.getItem('token');
  var api_url = getApi('countView');
  var infor = localStorage.getItem('infor');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":authtoken},
      data:{"announce_id":announce_id,"infor":infor},
    })
    .done(function(data) {
    })
}
function getAnnounce(data) {
  var token = localStorage.getItem('token');
  if (token == null) {
    token = "";
  }else {
    token = token;
  }
      var api_url = getApi('getannounceconnect');
        var formData = {
                          "type_job":'',
                          "job_description_1":data,
                          "job_description_2":'',
                          "education":'',
                          "zone":'',
                          "company_name":'',
                          "offset":'0'
                        };
      $.ajax({
        url: api_url,
        type: 'GET',
        headers:{"token":token},
        data:formData,
        beforeSend:function(){
          preloader();
        }
      })
      .done(function(data) {
          closeloader()
          var obj = JSON.parse(data);
          for (var i = 0; i < obj.data.length; i++) {
              var row = genCardAnnounce(obj.data[i]);
              $(".content-announce").append(row)
          }
      })
    }
function count_view(announce_id,link_name) {
  var token = localStorage.getItem('token');
  if (token == null) {
    token = "";
  }else {
    token = token;
  }
  var api_url = getApi('countView');
  $.ajax({
    url: api_url,
    type: 'POST',
    headers:{"token":token},
    data:{"announce_id":announce_id,"infor":$('#link_infor').val()},
  })
  .done(function(data) {
    var link = "announce?announce="+btoa(btoa(btoa(announce_id)))
        link += "&link_name="+btoa(btoa(btoa(link_name)));
        link += "&link_infor="+btoa(btoa(btoa('99')));
    var connect = getUrl(link);
    window.open(connect)
  })
}
function view_detail(announce_id) {
      if (data_user != 'users') {
        log_user('1',announce_id,'','','','','','','','',$('#link_infor').val())
      }
      var authtoken = localStorage.getItem('token');
      var url = getApi('getAnnounceById');
      $.ajax({
        url: url,
        type: 'GET',
        headers:{"token":authtoken},
        data:{"announce_id":announce_id},
      })
      .done(function(data) {
        var obj = JSON.parse(data);
        console.log(obj);
        var data_detail = obj.data[0];
        data_announce = obj.data[0];
        getAnnounce(data_detail.job_description_1[0].position_id);
        $('#company_name').text(data_detail.company.company.company_name);
        $('#company_detail').text(data_detail.company.company_detail);
        $('#company_update').text("แก้ไขล่าสุด "+formatDate(data_detail.company.company.update_at));
        $("#company_logo").attr("src",data_detail.company.company.image);
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/JOB_API/assets/documents/Announce/";
        if (data_detail.image != baseUrl) {
          $("#announce_image").attr("src",data_detail.image);
        }
        var location_name = "";
        if (data_detail.zone_detail.status == "200") {
          location_name += data_detail.zone_detail.data[0].district_thlist+" ";
        }
        if (data_detail.zone.status == "200") {
          location_name += data_detail.zone.data[0].pro_thname;
        }
        $("#location_name").text(location_name);
        if (data_detail.income_max == data_detail.income_min) {
          $('#salary').text(lang.announce.income);
        }else {
          $('#salary').text(numberWithCommas(data_detail.income_min)+" - "+numberWithCommas(data_detail.income_max)+" บาท");
        }
        var job_description_1 = data_detail.job_description_1[0].position_name;
        $("#job_description").text(job_description_1);
        $("#type_job").text(data_detail.type_job[0].name);
        $("#gender").text(data_detail.gender[0].name);
        $("#age").text(data_detail.age_min+" - "+data_detail.age_max+" ปี");
        $("#education").text(data_detail.education[0].name);
        $("#license").text(data_detail.license[0].driving_license_type_name);
        $("#working_day").text(data_detail.working_day[0].name);
        var work_start_date
        if (data_detail.work_start_date == '0000-00-00 00:00:00') {
          work_start_date = lang.announce.none;
        }else {
          work_start_date = data_detail.work_start_date;
        }
        $("#working_start_date").text(work_start_date);
        $("#announce_detail").html(nl2br(data_detail.detail));
        $("#announce_benefits").html(nl2br(data_detail.benefits))
        $("#announce_property").html(nl2br(data_detail.property))
      })
  }
function apply_job(announce_id) {
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.PleaseLogin,6000);
  }else {
    var api_url = getApi('applicant/chk_citizen');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":authtoken},
    })
    .done(function(data){
      console.log(data);
      var obj = JSON.parse(data);
      if (obj.status == 200) {
        register_iRecruit()
        // apply($("#announce_id").val())
      }else {
        // modal
        var connectURL = getUrl("getCitizenDialog");
        var formData = {"ajax":true};
            formData["data"] = "" ;
        $.ajax({
                url: connectURL,
                type: 'POST',
                data: formData
              }).done(function(data) {
                closeloader()
                var obj_modal = JSON.parse(data);
                $(".modal-area").append(obj_modal["modal"]);
                $("#citizenDialog").Modal({"backdrop":true,"static":true});
                if (obj.citizen != "") {
                  $('#citizen').val(obj.citizen);
                  $('#citizen_group').addClass('display-disable');
                  $('#head_citizen').addClass('display-disable');
                }else {
                  $('#citizen_group').removeClass('display-disable');
                  $('#head_citizen').removeClass('display-disable');
                }

                if (obj.province != "" && obj.aumphur != "") {
                  ms_province_get(obj.province);
                  ms_aumphur_get(obj.province,obj.aumphur);
                }else if (obj.province != "") {
                  ms_province_get(obj.province);
                }else {
                  ms_province_get();
                }
                $('input.datepicker').val(formatDateToDmy(obj.birth_date));
                if (obj.birth_date == '0000-00-00') {
                  var datePicker = $('input.datepicker').Zebra_DatePicker({
                    format : "d/m/Y",
                    show_select_today: false,
                    show_week_number:false,
                    current_date: new Date(1900,0,1),
                  });
                }else {
                  var datePicker = $('input.datepicker').Zebra_DatePicker({
                    format : "d/m/Y",
                    show_select_today: false,
                    show_week_number:false,
                    current_date: new Date(obj.birth_date)
                  });
                }
                $('#tel').val(obj.tel);
                // ms_province_get();
                $("#user_province").change(function() {
                  ms_aumphur_get($("#user_province").val(),'');
                })

                $("#update_applicant").click(function() {
                  console.log("update_applicant");
                  update_applicant();
                })

                $("#citizen").keyup(function(event) {
                  if (event.keyCode === 13) {
                   event.preventDefault();
                   update_applicant();
                 }else if (event.keyCode === 8) {

                 }else {
                  autoTab($("#citizen")[0]);
                 }
                });
              })
      }
    })
  }
}
function update_applicant() {
  console.log("update_applicant");
  var str = $("#citizen").val();
  console.log(str);
  var regex = /-/gi;
  var res = str.replace(regex, "");
  console.log(res);
  var authtoken = localStorage.getItem('token');
  var tel = $("#tel").val();
  var birth_date = formatDateToYmd($("#r_birth_date").val());
  console.log(birth_date);
  if (tel == "") {addErr("tel",lang.error.input_blank)}
  else if (!phonenumber(tel)) {addErr("tel",lang.error.data_worng)}

  if (checkID(res)
      && $("#user_province").val() != ""
      && $("#user_aumphur").val() != ""
      && phonenumber(tel)
     ) {
    var api_url = getApi('applicant/update_applicant');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":authtoken},
      data:{"citizen":res,
            "province":$("#user_province").val(),
            "aumphur":$("#user_aumphur").val(),
            "birth_date":birth_date,
            "tel" : tel
           },
    })
    .done(function(data) {
      console.log(data);
      var obj = JSON.parse(data);
      if (obj.status==200) {
        // apply($("#announce_id").val())
        register_iRecruit()
      }
    })
  }else {
    addErr("citizen",lang.error.data_worng)
  }

}
function register_iRecruit() {
  var authtoken = localStorage.getItem('token');
  var api_url = getApi('irecruit/chk_iRecruit_get');
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":authtoken},
  })
  .done(function(data) {
    console.log(data);
    var obj = JSON.parse(data);
    var reg_id;
    console.log(profile_me.data);
    console.log(data_announce);
    if (obj.status == 200) {
      reg_id = obj.reg_id;
      console.log("chk_position_iRecruit_post");
      var formData={"announce_id":data_announce_id,"reg_id":reg_id,"position":data_announce.job_description_1[0].position_id}
      $.ajax({
        url: getApi('irecruit/chk_position_iRecruit_post'),
        type: 'POST',
        headers:{"token":authtoken},
        data: formData
      }).done(function(data) {
        var obj = JSON.parse(data);
        chk_announce_iRecruit(reg_id)
      });
    }else {
      $.ajax({
        // url: "http://192.168.90.88:88/saveRegisOnline",
        url: getApi('irecruit/register_iRecruit_post'),
        type: 'POST',
        headers:{"token":authtoken},
        data: {"infor_id":$("#link_infor").val(),
               "staff_id":$("#link_name").val(),
               "position": data_announce.job_description_1[0].position_id
             },
        headers:{"token":authtoken},
      })
      .done(function(data) {
        var obj = JSON.parse(data);
        if (obj.status==200) {
          reg_id = obj.data
          chk_announce_iRecruit(reg_id)
        }
      })
    }
  });
}
function chk_announce_iRecruit(reg_id) {
  var authtoken = localStorage.getItem('token');
  $.ajax({
    url: getApi('irecruit/chk_announce_iRecruit_post'),
    type: 'POST',
    headers:{"token":authtoken},
    data: {"announce_id":data_announce_id,"reg_id":reg_id}
  }).done(function(data) {
    var obj = JSON.parse(data);
    if (obj.status==200) {
      apply(data_announce_id)
    }else {
      $(".closemodal").trigger("click")
      Swal.fire({
          title: lang.error.duplicate,
          type: 'error',
          confirmButtonText: lang.error.ok
        })
    }
  })
}
function apply(announce_id) {
  var authtoken = localStorage.getItem('token');
  var api_url = getApi('applicant/apply_job');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":authtoken},
      data:{"announce_id":announce_id},
    })
    .done(function(data) {
      $(".closemodal").trigger("click")
      var obj = JSON.parse(data);
      if (obj.status==200) {
        if (data_user != 'users') {
          log_user('2',announce_id,'','','','','','','','',$('#link_infor').val())
        }
        // Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
        Swal.fire({
            title: lang.error.success,
            type: 'success',
            confirmButtonText: lang.error.ok
          })
      }else if (obj.status==202) {
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.duplicate,6000);
      }
      else {
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
      }
    })
  }
function favorite_job(announce_id) {
  console.log(announce_id+'// Favorite');
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
    Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.PleaseLogin,6000);
  }else {
    var api_url = getApi('applicant/favorite_job');
    $.ajax({
      url: api_url,
      type: 'POST',
      headers:{"token":authtoken},
      data:{"announce_id":announce_id},
    })
    .done(function(data) {
      var obj = JSON.parse(data);
      if (obj.status==200) {
        getAnnounceTable()
        getCountAnnounce()
        if (data_user != 'users') {
          log_user('4',announce_id,'','','','','','','','',$('#link_infor').val())
        }
        Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.success,6000);
      }else if (obj.status==202) {
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.duplicate,6000);
      }else {
        Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.warning,6000);
      }
    })
  }
}

function autoTab(obj){
    /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย
    หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น  รูปแบบเลขที่บัตรประชาชน
    4-2215-54125-6-12 ก็สามารถกำหนดเป็น  _-____-_____-_-__
    รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____
    หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__
    ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบเลขบัตรประชาชน
    */
            var pattern=new String("_-____-_____-__-_"); // กำหนดรูปแบบในนี้
            var pattern_ex=new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้
            var returnText=new String("");
            var obj_l=obj.value.length;
            var obj_l2=obj_l-1;
            for(i=0;i<pattern.length;i++){
                if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){
                    returnText+=obj.value+pattern_ex;
                    obj.value=returnText;
                }
            }
            if(obj_l>=pattern.length){
                obj.value=obj.value.substr(0,pattern.length);
            }
}
