<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_controller extends MY_Controller {
  public function __construct(){
			 	 parent::__construct();
	}

  public function index()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'job/company/index';
    $this->data["title"]  = "Dashboard";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/waypoints/lib/jquery.waypoints.js")."'></script>";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/counterup/jquery.counterup.min.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jCompany.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jTable.js")."'></script>";
    $this->layouts();
  }

  public function administrator()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'job/company/administrator';
    $this->data["title"]  = "Dashboard";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/waypoints/lib/jquery.waypoints.js")."'></script>";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/counterup/jquery.counterup.min.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jCompany.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jAdministrator.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jTable.js")."'></script>";
    $this->layouts();
  }
  public function Announce()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'job/company/Announce';
    $this->data["title"]  = "Dashboard";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/waypoints/lib/jquery.waypoints.js")."'></script>";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/counterup/jquery.counterup.min.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jCompany.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jAdministrator.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jTable.js")."'></script>";
    $this->layouts();
  }

  public function getInviteDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["profile"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("job/company/invitedialog",$data,true);
				}
		}
		echo json_encode($callback);
		return;
  }
  public function getAnnounceDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["profile"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("job/company/announcedialog",$data,true);
				}
		}
		echo json_encode($callback);
		return;
  }
  public function getDetailDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["profile"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("job/company/detaildialog",$data,true);
				}
		}
		echo json_encode($callback);
		return;
  }
  public function getDetailEditDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["profile"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("job/company/detailEditDialog",$data,true);
				}
		}
		echo json_encode($callback);
		return;
  }

  public function EditDetailDialog()
  {
    $callback = array(
      "success" => false
    );
    if (isset($_POST["ajax"])) {
        if ($_POST["ajax"]) {
            $callback["success"] = true;
            $data["profile"] = $_POST["data"];
            $this->lang->load('layout', $this->language);
            $callback["modal"] = $this->load->view("job/DetailEditDialog",$data,true);
        }
    }
    echo json_encode($callback);
    return;
  }

  public function countViewRegisDialog()
  {
    $callback = array(
      "success" => false
    );
    if (isset($_POST["ajax"])) {
        if ($_POST["ajax"]) {
            $callback["success"] = true;
            $data["profile"] = $_POST["data"];
            $this->lang->load('layout', $this->language);
            $callback["modal"] = $this->load->view("job/company/countViewRegisdialog",$data,true);
        }
    }
    echo json_encode($callback);
    return;
  }

  public function applicant_view()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'job/company/applicant_view';
    $this->data["title"]  = "Activity";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/waypoints/lib/jquery.waypoints.js")."'></script>";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/counterup/jquery.counterup.min.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jCompanyApplicant.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jTable.js")."'></script>";
    $this->layouts();
  }


}
