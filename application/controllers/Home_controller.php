<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_controller extends MY_Controller {
  public function __construct(){
			 	 parent::__construct();
	}
  public function index()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'job/index';
    $this->data["title"]  = "Homepage";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/waypoints/lib/jquery.waypoints.js")."'></script>";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/counterup/jquery.counterup.min.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jHomepage.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jTable.js")."'></script>";
    $this->layouts();
  }

  public function announce()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'job/announce';
    $this->data["title"]  = "Announce";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/waypoints/lib/jquery.waypoints.js")."'></script>";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/counterup/jquery.counterup.min.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jAnnounce.js")."'></script>";
    $this->data["js"]  .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jTable.js")."'></script>";
    $this->layouts();
  }

  public function getCompanyLoginPage()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'job/login/login_company_page';
    $this->data["title"]  = "Login";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/waypoints/lib/jquery.waypoints.js")."'></script>";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/counterup/jquery.counterup.min.js")."'></script>";
    $this->layout();
  }
  public function getCompanyRegisterPage()
  {
    $this->lang->load('layout', $this->language);
    $this->middle = 'job/login/register_company_page';
    $this->data["title"]  = "Login";
    $this->data["css"]  = "";
    $this->data["js"]  = "";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/waypoints/lib/jquery.waypoints.js")."'></script>";
    // $this->data["js"]  .= "<script src='".base_url("assets/plugin/js/counterup/jquery.counterup.min.js")."'></script>";
    $this->layout();
  }

  public function getCompanyLoginDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["profile"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("job/login/login_company",$data,true);
				}
		}
		echo json_encode($callback);
		return;
  }

  public function getRegisterDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["profile"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("job/login/RegisterDialog",$data,true);
				}
		}
		echo json_encode($callback);
		return;
  }

  public function confirmUser()
  {
    // $this->load->view('job/login/confirm');
    $this->lang->load('confirm', $this->language);
    $this->middle = 'job/login/confirm';
    $this->data["css"]  = "";
    $this->data["css"]  .= "<link type='text/css' rel='stylesheet' href='".base_url("assets/plugin/css/jStyle.css")."'/>";
    $this->data["js"]  = "";
    $this->data["js"] .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jAuthen.js")."'></script>";
    $this->layout();

  }
  public function registerUser()
  {
    // test
    $userInfo = Array ( "userId" => "Ue3e02af8ce65bc04c17233daaa205e90",
                        "displayName" => " MooK ",
                        "pictureUrl" => "https://profile.line-scdn.net/0hc_Uy_46GPHBoABB27GlDJ1RFMh0fLjo4EGB7F0tQZEFEN3IkAzVyQ0oCYUEQY3snVW4kE04GMEBB",
                        "statusMessage" => "1 : 6 : 0 (peace sign) "
                      );
    $this->lang->load('confirm', $this->language);
    $this->middle = 'job/login/registerLine';
    $this->data["css"]  = "";
    $this->data["css"]  .= "<link type='text/css' rel='stylesheet' href='".base_url("assets/plugin/css/jStyle.css")."'/>";
    $this->data["info"] = $userInfo;
    $this->data["js"]  = "";
    $this->data["js"] .= "<script type='text/javascript' src='".base_url("assets/plugin/js/apps/jRegister.js")."'></script>";
    $this->layout();

  }

  public function getProfileDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["profile"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("job/ProfileDialog",$data,true);
				}
		}
		echo json_encode($callback);
		return;
  }

  public function filterDialog()
  {
    $callback = array(
      "success" => false
    );
    if (isset($_POST["ajax"])) {
        if ($_POST["ajax"]) {
            $callback["success"] = true;
            $data["profile"] = $_POST["data"];
            $this->lang->load('layout', $this->language);
            $callback["modal"] = $this->load->view("job/filterDialog",$data,true);
        }
    }
    echo json_encode($callback);
    return;
  }
  public function DetailDialog()
  {
    $callback = array(
      "success" => false
    );
    if (isset($_POST["ajax"])) {
        if ($_POST["ajax"]) {
            $callback["success"] = true;
            $data["profile"] = $_POST["data"];
            $this->lang->load('layout', $this->language);
            $callback["modal"] = $this->load->view("job/DetailDialog",$data,true);
        }
    }
    echo json_encode($callback);
    return;
  }

  public function getCitizenDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["profile"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("job/citizenDialog",$data,true);
				}
		}
		echo json_encode($callback);
		return;
  }
  public function getEditApplicantDialog()
  {
    $callback = array(
			"success" => false
		);
		if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]) {
						$callback["success"] = true;
						$data["profile"] = $_POST["data"];
						$this->lang->load('layout', $this->language);
						$callback["modal"] = $this->load->view("job/editprofile",$data,true);
				}
		}
		echo json_encode($callback);
		return;
  }


}
