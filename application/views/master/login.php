<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      <div class="form-group center">
        <img src="<?php echo  base_url("assets/images/logo/logo_text.png")?>" height="30" >
      </div>
      <form class="form-horizontal form-material" id="loginform" >
        <h3 class="box-title m-b-20"><?php echo $this->lang->line('signin') ?></h3>
        <div class="form-group ">
          <div class="col s12" id="username_group">
            <input id="username" name="username" type="text" placeholder="<?php echo $this->lang->line('username') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelusername" for="user" ></small>
            <!-- <input class="form-control" type="text" name="username" id='username' onkeypress="return onremove_validate(this);" required="" placeholder="<?php echo $this->lang->line('username') ?>">
            <div class='feedback' id="username_feedback"></div> -->
          </div>
        </div>
        <div class="form-group">
          <div class="col s12" id="password_group">
            <!-- <input class="form-control" type="password" name="password" id='password' onkeypress="return onremove_validate(this);" required="" placeholder="<?php echo $this->lang->line('password') ?>">
            <div class='feedback' id="password_feedback"></div> -->
            <input id="password" name="password" type="password" placeholder="<?php echo $this->lang->line('password') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelpassword" for="password" ></small>
        </div>
        <div class="form-group">
          <div class="col s12">
            <!-- <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"><?php echo $this->lang->line('remember') ?></label>
            </div> -->
            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> <?php echo $this->lang->line('forget') ?></a>
          </div>
        </div>
        <div class="form-group text-center ">
          <div class="col s12">
            <button class="btn waves-effect waves-light btn-login" id="login"><?php echo $this->lang->line('login') ?></button>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col s12 text-center">
            <p><?php echo $this->lang->line('noaccount') ?><a class="text-primary m-l-5"><b class="pink-text"> <?php echo $this->lang->line('signup') ?></b></a></p>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
