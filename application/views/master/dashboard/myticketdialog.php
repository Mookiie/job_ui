<div id="MyTicketDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span><?php echo $ticket["ticket_title"]." (".$ticket["ticket_id"].")" ?></span>
    </div>
    <div>
      <div class="row">
        <ul class="tabs tabs-member tabs-fixed-width">
          <li class="tab waves-effect details"><a class="details" href="#ticket_profile"><span class='fas fa-user-circle'></span> <span class='hide-on-small-only'></span></a></li>
          <li class="tab waves-effect attached" style="display:none"><a class="attached" href="#ticket_attached"><span class='fas fa-paperclip'></span> <span class='hide-on-small-only'> </span></a></li>
          <li class="tab waves-effect track"><a id="tracking" href="#ticket_tracking"><span class='fas fa-tasks'></span> <span class='hide-on-small-only'> </span></a></li>
          <li class="tab waves-effect detail" style="display:none"><a href="#ticket_detail"><span class='fas fa-info'></span> <span class='hide-on-small-only'> </span></a></li>
          <li class="tab waves-effect edit" style="display:"><a id="edit_config" class="edit_ticket" href="#ticket_edit"><span class='fas fa-edit'></span> <span class='hide-on-small-only'> </span></a></li>
        </ul>
      </div>
      <div class="col s12">
        <div id="ticket_profile">
          <div class="row">
            <div class="row card-container">
              <div class="right ticket-menu" style="display:none">
                <span>คุณต้องการ </span>
                <button class="btn waves-effect waves-light from btn-danger  " type="button" id="dismiss" name="button"> <?php echo $this->lang->line('dismiss') ?></button>
                <button class="btn waves-effect waves-light from btn-warning " type="button" id="edit" name="button"> <?php echo $this->lang->line('edit') ?></button>
              </div>
              <div class="right reticket-menu" style="display:none">
                <span>คุณต้องการ </span>
                <button class="btn waves-effect waves-light from btn-warning " type="button" id="reticket" name="button"> <?php echo $this->lang->line('reticket') ?></button>
              </div>
            </div>
            <div class="card-container">
              <div class="col s12">
                <div class="card">
                  <div class="card-action">
                    <u>ข้อมูลคำขอ <?php echo $ticket["ticket_id"] ?></u>
                  </div>
                  <div class="card-content ">
                    <dt>
                      ชื่อ : <?php echo $ticket["ticket_title"] ?> <br>
                      สาขา : <?php echo ($ticket['branch_name'] == '') ? "ไม่ระบุ" : $ticket['branch_name']; ?> <br>
                      แผนก : <?php echo ($ticket['dep_name'] == '') ? "ไม่ระบุ" : $ticket['dep_name']; ?> <br>
                      ไซต์งาน : <?php echo ($ticket['site_name'] == '') ? "ไม่ระบุ" : $ticket['site_name']; ?> <br>
                      รายละเอียด : <br>
                    </dt>
                    <dd>
                        <?php echo nl2br($ticket['ticket_detail']) ?>
                    </dd>
                    <dt>
                      ขอเมื่อ :<br>
                      แก้ไข :<br>
                      แก้ไขเมื่อ :<br>
                    </dt>
                  </div>
                </div>
              </div>

              <div class="col s12">
                <div class="card">
                  <div class="card-action">
                    <u>ข้อมูลผู้ขอ</u>
                  </div>
                  <div class="card-content ">
                    <dt>
                      ชื่อ : <?php echo $ticket["fname"]." ".$ticket["lname"] ?> <br>
                      สาขา : <?php echo ($ticket['user_branch_name'] == '') ? "ไม่ระบุ" : $ticket['user_branch_name']; ?> <br>
                      แผนก : <?php echo ($ticket['user_dep_name'] == '') ? "ไม่ระบุ" : $ticket['user_dep_name']; ?> <br>
                      ไซต์งาน : <?php echo ($ticket['user_site_name'] == '') ? "ไม่ระบุ" : $ticket['user_site_name']; ?> <br>
                      ทีม : <?php echo ($ticket['user_team_name'] == '') ? "ไม่ระบุ" : $ticket['user_team_name']; ?> <br>
                    </dt>
                  </div>
                </div>
              </div>

              <div class="col s12" style="display:">
                <div class="card">
                  <div class="card-action">
                    <u>ข้อมูลการให้บริการ</u>
                  </div>
                  <div class="card-content ">
                    <dt>
                      ชื่อเจ้าหน้าที่ : <br>
                      ดำเนินการเมื่อ : <br>
                      เริ่มงาน :<br>
                      วันครบกำหนด : <br>
                      ดำเนินการโดย :<br>
                      <!-- <a class="right">เพิ่มเติม >></a> -->
                    </dt>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="ticket_attached">
          <?php
          $doc = $ticket["ticket_image"];
          if($doc == ""){
              echo "<div class='card-block' align='center'><h2><span class='	fa fa-exclamation-triangle'></span> Not Found Document</h2></div>";
          }else{
            $file = explode(".",$doc);
            $type = $file[count($file)-1];
            if($type == "pdf"){
              echo "<input type='hidden' id='pathtxt' value='".$doc."'>";
              echo "<div id='framPDF' style='height:800px;'></div>";
            }else{ ?>
              <input type='hidden' id='pathtxt' value='' >
                  <a class="nav-link" href="<?php echo $doc;?>" download="<?php echo $ticket["ticket_id"];?>">
                    <span class='fa fa-download'></span>
                  </a>
                  <a  class="nav-link"  onclick="VoucherPrint('<?php echo $doc;?>')">
                         <span class='fa fa-print'></span>
                  </a>
              <img width='50%' class='bkImg' src='<?php echo $doc;?>'>
          <?php  }
          }
          ?>
        </div>
        <div id="ticket_tracking">
          <div class="card-container">
            <div class="col s12">
              <div class="card-content ">
                <table>
                  <tr style="border-bottom:solid 1px #ddd;">
                    <td>
                      <label for="detail"><span class='fas fa-list'></span> <?php echo $this->lang->line('tracking'); ?> </label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div id='tracking_list'></div>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div id="ticket_detail">
        <div id="ticket_edit">
          ticket_edit
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat closemodal">OK</a>
    </div>
  </div>
</div>
