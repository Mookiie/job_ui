<!DOCTYPE html>
  <html>
  <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/materialize/css/materialize.min.css"); ?>"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/animate.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/sweetalert2/dist/sweetalert2.min.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/font-awesome-animation.min.css"); ?>"/>
      <!-- <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/datepicker/dist/datepicker.css"); ?>"/> -->
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/datetimepicker/css/bootstrap-material-datetimepicker.css"); ?>"/>
      <!-- <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/timepicker/css/timepicki.css"); ?>"/> -->
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/bower_components/sidebar-nav/dist/sidebar-nav.min.css")?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/bower_components/toast-master/css/jquery.toast.css")?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/bower_components/morrisjs/morris.css")?>" />
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/layouts.css"); ?>"/>
      <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/plugin/css/globals.css"); ?>"/>
      <link type='text/css'rel="stylesheet" href='<?php echo base_url("assets/plugin/Zebra_Datepicker-master/dist/css/bootstrap/zebra_datepicker.css"); ?>'/>

      <?php if (isset($css)){echo $css;} ?>
      <link rel="icon" href="<?php echo base_url("assets/images/logo/logo.png") ?>" type="image/gif" sizes="16x16">
      <title id='app_title'> JOBs  - <?php if (isset($title)){echo $title;} ?></title>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
      <!-- <script type="text/javascript" src="<?php echo base_url("assets/plugin/js/jquery-3.2.1.min.js"); ?>"></script> -->
      <!-- <script type="text/javascript" src="<?php echo base_url("assets/plugin/fontawesome/svg-with-js/js/fontawesome-all.min.js"); ?>"></script> -->
      <!-- <script type="text/javascript" src="<?php echo base_url("assets/plugin/materialize/js/materialize.min.js"); ?>"></script>
      <script type='text/javascript' src='<?php echo base_url("assets/plugin/sweetalert2/dist/sweetalert2.js"); ?>'></script> -->

      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body >
    <ul id="slide-out" class="side-nav">
      <li>
        <div id="get-profile" class="user-view  get-profile">
          <div class="background"></div>
          <a><img class="circle side-profile auth_profile" ></a>
          <a><span class="side-name name"></span></a>
          <a><span class="side-email email"></span></a>
        </div>
        <div id="get-login" class="user-view get-login center">
          <img class='login-logo logo' src="<?php echo base_url("assets/images/logo/logo.png");?>">
          <div class="login-logo-text">
            <h5 class="white-text reset-margin">JOBS</h5>
            <h6 class="white-text reset-margin">เข้าสู่ระบบ</h6>
          </div>
          <div class="login-btn">
            <a class="btn btn-j-white waves-effect" onclick="modalUserLogin()" >ผู้สมัคร</a>
            <a class="btn btn-j-white waves-effect" onclick="modalCompanyLogin()" >ผู้ประกอบการ</a>
          </div>
        </div>
      </li>
      <div class="normal_sidebar">
        <li data-href='home' class='home' id='homepage' onclick="pageHome()" >
          <a><i class='fas fa-home'></i> <span class=""><?php echo $this->lang->line('nav_homepage'); ?></span></a>
        </li>
        <li data-href='filter' class='filter' id='filter' onclick="filter_Dialog()" >
          <a><i class="fas fa-search"></i><span class=''>ค้นหาโดยละเอียด</span></a>
        </li>
        <li data-href='announce' class='announce' id='announcepage' onclick="pageannounce()" style="display:none">
          <a><i class='fas fa-building'></i> <span class=""><?php echo $this->lang->line('nav_announcepage'); ?></span></a>
        </li>
        <li data-href='AnnounceMe' class='AnnounceMe' id='AnnounceMepage' onclick="pageAnnounceMe()" style="display:none">
          <a><i class='fas fa-bullhorn'></i> <span class=""><?php echo $this->lang->line('nav_announceMepage'); ?></span></a>
        </li>
        <li data-href='administrator' class='administrator' id='administratorpage' onclick="pageAdministrator()" style="display:none">
          <a><i class='fas fa-users-cog'></i><span class=""><?php echo $this->lang->line('nav_administratorpage'); ?></span></a>
        </li>
        <li data-href='system' class='system' id='systempage' onclick="pageSystem()" style="display:none">
          <a><i class='fas fa-crown'></i><span class=""><?php echo $this->lang->line('nav_systempage'); ?></span></a>
        </li>
      </div>
      <!-- <div class="group_sidebar">
      </div> -->
      <li id="editApplicant" class='auth-editApplicant'><a><i class='fas fa-user-edit'></i> <span><?php echo $this->lang->line('editApplicant'); ?></span></a></li>
      <li id="logout" class='auth-logout'><a><i class='fas fa-sign-out-alt'></i> <span><?php echo $this->lang->line('signout'); ?></span></a></li>
    </ul>

    <div class="document-container">
        <nav class='navbar-top'>
            <ul class="left" id="nav-search">
              <li class="row reset-margin btn_search">
                  <select class="browser-default select_search" id="select_search" name="select_search">
                    <option value="" disabled selected><i class="fas fa-search"></i>ค้นหา</option>
                    <!-- <option value="" disabled selected><?php echo $this->lang->line('type_job')?></option> -->
                  </select>
                  <small id="labeledit_type_job" for="type_job" ></small>
              </li>
              <li class="btn_search_advance">
                <a class='waves-effect' onclick="filter_Dialog()">
                  <i class="fas fa-search"></i>
                  <span class='white-text'>ค้นหาโดยละเอียด</span>
                </a>
              </li>
            </ul>
            <ul class="right">
              <li class='nav-top-dropdown lg-profile' id="nav_profile" data-activates='profile_dropdown'>
                <a class='bar-lg-profile'>
                  <span class='name white-text'></span>
                  <img class="auth_profile">
                  <span class='fas fa-bars'></span>
                </a>
              </li>
              <li class='nav-top-dropdown lg-login' id="nav_login_user" onclick="modalUserLogin()">
                <a class='bar-lg-profile'>
                  <span class="white-text">เข้าสู่ระบบผู้สมัคร</span>
                </a>
              </li>
              <li class='nav-top-dropdown lg-login' id="nav_login_comp" onclick="modalCompanyLogin()">
                <a class='bar-lg-profile'>
                  <span class="white-text">เข้าสู่ระบบผู้ประกอบการ</span>
                </a>
              </li>
              <ul id="notify_dropdown"  class='dropdown-content notibody notify-content'></ul>
              <!-- <ul id="invite_dropdown"  class='dropdown-content invitebody invite-content'> -->

              </ul>
              <ul id='profile_dropdown' class='dropdown-content'>
                <li class='get-profile get-profile-dropdown' onclick="pageProfile($(this),'')"><a><i class='fas fa-user-circle j-text'></i><span class="j-text"><?php echo $this->lang->line('account'); ?></span></a></li>
                <!-- <li><a ><i class='fa fa-cog'></i> <?php echo $this->lang->line('navtop_setting'); ?> </a></li> -->
                <li class="divider get-profile"></li>
                <li class='auth-editApplicant'><a><i class='fas fa-user-edit  j-text'></i> <span class="j-text"><?php echo $this->lang->line('editApplicant'); ?></span></a></li>
                <li class='auth-logout'><a><i class='fas fa-sign-out-alt  j-text'></i> <span class="j-text"><?php echo $this->lang->line('signout'); ?></span></a></li>
              </ul>
            </ul>
            <ul class="right lg-login-mobile">
              <li class='nav-top-dropdown' id="nav_login_user_mobile" onclick="modalUserLogin()">
                <a class=''>
                  <span class="white-text">เข้าสู่ระบบ</span>
                </a>
              </li>
            </ul>
          </nav>
        <div class="sidebar">
              <div class="sidebar-header">
                <ul class="left">
                  <li class='lg-sidbar'>
                    <a class='waves-effect brand-bars'>
                      <span class='fas fa-bars'></span>
                    </a>
                  </li>
                  <li class='md-sidebar'>
                    <a class='waves-effect brand-bars'>
                      <span class='fas fa-bars'></span>
                    </a>
                  </li>
                  <li class='sm-sidebar'  data-activates="slide-out">
                    <a class='waves-effect'>
                      <span class='fas fa-bars'></span>
                    </a>
                  </li>
                </ul>
                  <a class='white-text row'>
                    <img class='sidebar-logo' src="<?php echo base_url("assets/images/logo/logo.png"); ?>" >
                    <img class='sidebar-logo-text' src="<?php echo base_url("assets/images/logo/logo_text.png");?>" width="80" >
                  </a>
              </div>
              <!-- <div class="sidebar-content">

              </div> -->
                <div class="sidebar-content-menu">
                  <ul class="collection">
                    <div class='normal_sidebar'>
                      <li data-href='home' class='home' id='homepage' onclick="pageHome()" >
                        <i class='fas fa-home'></i>
                        <span class='side-menu-text'></span>
                      </li>
                      <li data-href='announce' class='announce' id='announcepage' onclick="pageannounce()" style="display:none">
                        <a><i class='fas fa-building'></i></a>
                      </li>
                      <li data-href='AnnounceMe' class='AnnounceMe' id='announceMepage' onclick="pageAnnounceMe()" style="display:none">
                        <a><i class='fas fa-bullhorn'></i></a>
                      </li>
                      <li data-href='administrator' class='administrator' id='administratorpage' onclick="pageAdministrator()" style="display:none">
                        <a><i class='fas fa-users-cog'></i></a>
                      </li>
                      <li data-href='system' class='system' id='systempage' onclick="pageSystem()" style="display:none">
                        <a><i class='fas fa-crown'></i></a>
                      </li>
                    </div>
                    <!-- <div class="group_sidebar"></div> -->
                  </ul>

                </div>
                <div class="sidebar-content-detail">
                  <ul class="collection">
                    <div class='normal_sidebar'>
                      <li>
                        <span class='sidebar-content-detail-text'>งานทั้งหมด</span>
                        <small class='sidebar-content-detail-count advert_count_all'></small>
                      </li>
                      <li id="li-announce-favorite">
                        <span class='sidebar-content-detail-text'>ประกาศที่สนใจ</span>
                        <small class='sidebar-content-detail-count advert_count_favorite'></small>
                      </li>
                      <li class="display-disable" id="li-announce-me">
                        <span class='sidebar-content-detail-text'>ประกาศของฉัน</span>
                        <small class='sidebar-content-detail-count advert_count_me'></small>
                      </li>
                    </div>
                    <!-- <div class="group_sidebar"></div> -->
                  </ul>
                </div>
          </div>
        <div class="main">
            <?php if ($middle) echo $middle; ?>
        </div>
      </div>

      <div class="loader">
        <!-- <div class="loader-inner">
          <div class="loader-line-wrap">
            <div class="loader-line"></div>
          </div>
          <div class="loader-line-wrap">
            <div class="loader-line"></div>
          </div>
          <div class="loader-line-wrap">
            <div class="loader-line"></div>
          </div>
          <div class="loader-line-wrap">
            <div class="loader-line"></div>
          </div>
          <div class="loader-line-wrap">
            <div class="loader-line"></div>
          </div>
        </div> -->
        <div class="loader-inner">
          <div class="lds-dual-ring"></div>
        </div>
        <div class="white-text loader-inner-text"><?php echo $this->lang->line('load'); ?><span class="white-text point-wait"></span></div>
     </div>

    <div class="modal-info"></div>
    <div class="modal-area"></div>
    <div class="modal-area-2"></div>
    <div class="modal-bottom"></div>
    <div class="modal-backdrop"></div>
    <div class="preload-backdrop"></div>
    <!-- <div class="by-footer">
      <a href="https://line.me/R/ti/p/%40sew4082b"><img height="20" border="0" alt="เพิ่มเพื่อน" src="https://scdn.line-apps.com/n/line_add_friends/btn/en.png"></a>
    </div> -->

      <code>
            <script id="jsonLang" type="application/json">
                  <?php include("assets/json/lang/thailand.json"); ?>
                  <?php include("assets/json/lang/english.json"); ?>
            </script>
      </code>
    <script type="text/javascript" src="<?php echo base_url("assets/plugin/js/jquery-3.2.1.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/plugin/materialize/js/materialize.min.js"); ?>"></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/sweetalert2/dist/sweetalert2.js"); ?>'></script>
    <!-- <script type="text/javascript" src="<?php echo base_url("assets/plugin/fontawesome/svg-with-js/js/fontawesome-all.min.js"); ?>"></script> -->

    <script type="text/javascript" src="<?php echo base_url("assets/plugin/js/apps/jHelper.js"); ?>"></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/js/apps/jLayouts.js"); ?>'></script>
    <script type="text/javascript" src="<?php echo base_url("assets/plugin/js/apps/jAuthentication.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/plugin/bower_components/counterup/jquery.counterup.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/plugin/bower_components/waypoints/lib/jquery.waypoints.min.js"); ?>"></script>
    <script type="text/javascript" src='<?php echo base_url('assets/plugin/js/pretty.js'); ?>'></script>
    <script type="text/javascript" src='<?php echo base_url('assets/plugin/js/md5/md5.js'); ?>'></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/chart.js/dist/Chart.js"); ?>'></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/chart.js/dist/Chart.min.js"); ?>'></script>
    <!-- <script type='text/javascript' src='<?php echo base_url("assets/plugin/datepicker/dist/datepicker.js"); ?>' ></script> -->
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/datetimepicker/js/moment.min.js"); ?>' ></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/datetimepicker/js/bootstrap-material-datetimepicker.js"); ?>' ></script>
    <!-- <script type='text/javascript' src='<?php echo base_url("assets/plugin/timepicker/js/timepicki.js"); ?>' ></script> -->
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/js/PDFObject/pdfobject.js"); ?>' ></script>
    <script type='text/javascript' src='<?php echo base_url("assets/plugin/Zebra_Datepicker-master/dist/zebra_datepicker.min.js"); ?>' ></script>

    <!-- <script src="https://js.pusher.com/4.1/pusher.min.js"></script> -->

    <?php if (isset($js)){echo $js;} ?>


    </body>
</html>
