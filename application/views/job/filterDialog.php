<div id="filterDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span class="white-text"><?php echo $this->lang->line('filter') ?></span>
    </div>
    <div>
    <div class="row container">
      <div class="form-group ">
        <h6></h6>
        <div class="row col s12" id="type_job_group">
            <select class="browser-default type_job" id="type_job" name="type_job">
              <option value="" disabled selected><?php echo $this->lang->line('type_job')?></option>
            </select>
            <small id="labeltype_job" for="type_job" ></small>
        </div>
        <!-- <div class="row col s12" style="padding: 0 !important;">
          <div class="col s12 l6" id="job_description_1_group">
              <select class="browser-default job_description_1" id="job_description_1">
                <option value="" disabled selected><?php echo $this->lang->line('job_description_1') ?></option>
              </select>
              <small id="labeljob_description_1" for="job_description_1" ></small>
          </div>
          <div class="col s12 l6" id="job_description_2_group">
              <select class="browser-default job_description_2" id="job_description_2" disabled="disabled">
                <option value="" disabled selected><?php echo $this->lang->line('job_description_2') ?></option>
              </select>
              <small id="labeljob_description_2" for="job_description_2" ></small>
          </div>
        </div> -->
        <div class="row col s12" id="job_position_group">
            <select class="browser-default job_position" id="job_position">
              <option value="" disabled selected><?php echo $this->lang->line('job_position') ?></option>
            </select>
            <small id="labeljob_position" for="job_position" ></small>
        </div>
        <!-- <div class="row col s12" id="zone_group">
            <select class="browser-default zone" id="zone">
              <option value="" disabled selected><?php echo $this->lang->line('zone') ?></option>
            </select>
            <small id="labelzone" for="zone" ></small>
        </div> -->
        <div class="row col s12" id="search_province_group">
            <select class="browser-default province" id="search_province" name="province">
              <option value="" disabled selected><?php echo $this->lang->line('province')?></option>
            </select>
            <small id="labelsearch_province" for="province" ></small>
        </div>
        <div class="row col s12" id="search_aumphur_group">
            <select class="browser-default aumphur" id="search_aumphur" name="aumphur">
              <option value="" disabled selected><?php echo $this->lang->line('aumphur')?></option>
            </select>
            <small id="labelsearch_aumphur" for="aumphur" ></small>
        </div>
        <div class="row col s12" id="education_group">
            <select class="browser-default education" id="education">
              <option value="" disabled selected><?php echo $this->lang->line('education') ?></option>
            </select>
            <small id="labeleducation" for="education" ></small>
        </div>

        <div class="row col s12" id="company_name_group">
          <select class="browser-default company_name" id="company_name">
            <option value="" disabled selected><?php echo $this->lang->line('company_name') ?></option>
          </select>
          <small id="labelcompany_name" for="company_name" ></small>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat" id="filter_search" ><?php echo $this->lang->line('search') ?></a>
      <a class="modal-close waves-effect btn-flat closemodal"><?php echo $this->lang->line('close') ?></a>
    </div>
  </div>
</div>
