<div id="ProfileDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span><?php echo $profile["fname"]." ".$profile["lname"] ?></span>
    </div>
    <div>
      <div class="row">
        <ul class="tabs tabs-member tabs-fixed-width">
          <li class="tab waves-effect"><a class="details" href="#user_profile"><span class='fas fa-user-circle'></span> <span class='hide-on-small-only'></span></a></li>
          <li class="tab waves-effect"><a class="edit" href="#user_edit"><span class='fas fa-edit'></span> <span class='hide-on-small-only'> </span></a></li>
        </ul>
      </div>
      <div class="col s12">
        <div id="user_profile">
          <div class="center">
            <img src="<?php echo $profile["image"]?>" alt="Avatar" width="100px">
          </div>
          <!-- <table class="center">
            <tbody>
              <tr>
                <td class="center"><?php echo $this->lang->line('fname')." - ".$this->lang->line('lname') ?></td>
                <td class=""><?php echo $profile["fname"]." ".$profile["lname"]?></td>
              </tr>
              <tr>
                <td class="center"><?php echo $this->lang->line('tel') ?></td>
                <td class=""><?php echo $profile["tel"]?></td>
              </tr>
            </tbody>
          </table> -->
          <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> ข้อมูลทั่วไป <i class="fas fa-angle-double-left"></i></h6>
          <div class="center">
            <span><?php echo $this->lang->line('fname')." - ".$this->lang->line('lname') ?></span>
            <span>:</span>
            <span><?php echo $profile["fname"]." ".$profile["lname"] ?></span>
            <br>
            <span><?php echo $this->lang->line('tel')?></span>
            <span>:</span>
            <span><?php echo $profile["tel"]?></span>
            <br>
            <span><?php echo $this->lang->line('level')?></span>
            <span>:</span>
            <span><?php echo $profile["level"]["name"]?></span>
          </div>
        </div>
        <div id="user_edit">
          <div class="center">
            <img src="<?php echo $profile["image"]?>" alt="Avatar" width="100px">
          </div>
          <div class="container">
            <div class="file-field input-field">
             <div class="btn">
               <span style="color:#ffffff;"><?php echo $this->lang->line('change')?></span>
               <input type="file" id="file_profile" name="file_profile">
             </div>
             <div class="file-path-wrapper">
               <input class="form-control file-path" type="text" id="nfile" name="nfile">
             </div>
            </div>
            <div class="col s6" id="fname_group">
                <small><?php echo $this->lang->line('fname') ?></small>
                <input id="fname" name="fname" type="text" placeholder="<?php echo $this->lang->line('fname') ?>" value="<?php echo $profile["fname"]?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                <small id="labelfname" for="fname" ></small>
            </div>
            <div class="col s6" id="lname_group">
              <small><?php echo $this->lang->line('lname') ?></small>
                <input id="lname" name="lname" type="text" placeholder="<?php echo $this->lang->line('lname') ?>" value="<?php echo $profile["lname"]?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                <small id="labellname" for="lname" ></small>
            </div>
            <div class="col s12" id="tel_group">
              <small><?php echo $this->lang->line('tel') ?></small>
                <input id="tel" name="tel" type="tel" placeholder="<?php echo $this->lang->line('tel') ?>" value="<?php echo $profile["tel"]?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                <small id="labeltel" for="tel" ></small>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat" id="editProfile" style="display:none" >บันทึก</a>
      <a class="modal-close waves-effect btn-flat closemodal">ปิด</a>
    </div>
  </div>
</div>
