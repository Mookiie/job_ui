<div id="DetailDialog"  class="modal ">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span class="white-text"><?php echo $this->lang->line('edit');echo $profile["announce_title"]?></span><br />
      <?php
      $job_description_1 = $profile['job_description_1'][0]['position_name'];
      // if (array_key_exists("job_description_2",$profile)){
      //   if( count($profile['job_description_2']) && $profile['job_description_2'][0]['job_description_code_2'] != '999') {
      //     $job_description_1 .= " (".$profile['job_description_2'][0]['job_description_name'].")";
      // }}
       ?>
      <span class="white-text"><?php echo $this->lang->line('job_description_1')." : "; ?></span><span class="white-text" id="job_description_1"><?php echo $job_description_1 ;?></span>
    </div>
    <div>
      <!-- <div class="container content-detail">
        <input type="hidden" id="announce_id" value="">
        <?php $logo_company = $profile['company']['company']['image'] ?>
        <div class='row col s12 center' style='margin-bottom: 10px;'><img height="50" id='logo_company' scr="<?php echo $logo_company; ?>" ></div>
        <h6 class="center"><span id="company_name"><?php echo $profile['company']['company']['company_name']?></span></h6>
        <div class='row col s12 center' style='margin-bottom: 10px;'>
        <h6>
          <i class="fas fa-building"></i>
          &nbsp
          <span id="company_kind"><?php echo $profile['company']['company_kind']['name']?></span>
          &nbsp&nbsp&nbsp&nbsp
          <i class="fas fa-map-marker-alt"></i>
          &nbsp
          <span id="location"><?php echo $profile['company']['pro_id']['name']?></span>
        </h6>
        </div>
        <div class="divider"></div>
        <h6 class="center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('qualification'); ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="divider"></div>
        <div class="row col s12 container-detail">
          <?php echo $this->lang->line('type_job'); ?> : <span id="type_job"><?php echo $profile['type_job'][0]['name']?></span> <br />
          <?php
          $income;
          if ($profile['income_max'] != $profile['income_min']) {
            $income = $profile['income_min']+" - "+$profile['income_max'];
          }else {
            $income = $this->lang->line('income');
          }
           ?>
          <?php echo $this->lang->line('income_label'); ?> : <span id="income"><?php echo $income?></span> <br />
          <?php echo $this->lang->line('gender'); ?> : <span id="gender"><?php echo $profile['gender'][0]['name']?></span> <br />
          <?php echo $this->lang->line('age'); ?> : <span id="age"><?php echo $profile['age_min']." - ".$profile['age_max']." ปี"?></span> <br />
          <?php echo $this->lang->line('education'); ?> : <span id="education"><?php echo $profile['education'][0]['name']?></span> <br />
          <?php echo $this->lang->line('license'); ?> : <span id="license"><?php echo $profile['license'][0]['driving_license_type_name']?></span> <br />
          <?php
            if ($profile['working_start'] != '00:00' && $profile['working_end'] != '00:00') {
              $working_time = "(".$profile['working_start']." - ".$profile['working_end'].")";
            }else {
              $working_time = "";
            }
          ?>
          <?php echo $this->lang->line('working_day'); ?> : <span id="working_day"><?php echo $profile['working_day'][0]['name']?></span> <span id="working_time"><?php echo $working_time?></span><br />
          <?php echo $this->lang->line('property'); ?> : <br /><dd>  <span id="property"><?php echo nl2br($profile['property'])?></span> </dd><br />
          <?php
              $work_start_date;
              if ($profile['work_start_date'] == '0000-00-00 00:00:00') {
                $work_start_date = $this->lang->line('none');
              }else {
                $work_start_date = $profile['work_start_date'];
              }
          ?>
          <?php echo $this->lang->line('working_start_date'); ?> : <span id="working_start_date"><?php echo $work_start_date?></span> <br />
        </div>
        <div class="divider"></div>
        <h6 class="center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('benefits'); ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="divider"></div>
        <div class="row col s12">
          <span id="benefits"><?php echo nl2br($profile['benefits'])?></span> <br />
        </div>
      </div> -->

      <div class="row container">
        <div class="form-group ">
          <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> ข้อมูลงาน <i class="fas fa-angle-double-left"></i></h6>
          <div class="col s12" id="edit_announce_title_group">
              <input id="edit_announce_title" name="announce_title" type="text" data-length="50" placeholder="<?php echo $this->lang->line('announce_title') ?>" value="<?php echo $profile["announce_title"]?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
              <small id="labeledit_announce_title" for="announce_title" ></small>
          </div>
          <div class="row col s12" id="edit_type_job_group">
              <select class="browser-default type_job" id="edit_type_job" name="type_job">
                <option value="" disabled selected><?php echo $this->lang->line('type_job')?></option>
              </select>
              <small id="labeledit_type_job" for="type_job" ></small>
          </div>
          <!-- <div class="row col s12" style="padding: 0 !important;">
            <div class="col s6" id="edit_job_description_1_group">
                <select class="browser-default job_description_1" id="edit_job_description_1">
                  <option value="" disabled selected><?php echo $this->lang->line('job_description_1') ?></option>
                </select>
                <small id="labeledit_job_description_1" for="job_description_1" ></small>
            </div>
            <div class="col s6" id="edit_job_description_2_group">
                <select class="browser-default job_description_2" id="edit_job_description_2" >
                  <option value="" disabled selected><?php echo $this->lang->line('job_description_2') ?></option>
                </select>
                <small id="labeledit_job_description_2" for="job_description_2" ></small>
            </div>
          </div> -->
          <div class="row col s12" id="job_position_group">
              <select class="browser-default job_position" id="job_position">
                <option value="" disabled selected><?php echo $this->lang->line('job_position') ?></option>
              </select>
              <small id="labeljob_position" for="job_position" ></small>
          </div>
          <div class="col s12" id="edit_detail_group">
              <textarea id="edit_detail" class="materialize-textarea" placeholder="<?php echo $this->lang->line('detail') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required><?php echo $profile['detail']?></textarea>
              <small id="labeledit_detail" for="detail" ></small>
          </div>
          <!-- <div class="row col s12" id="edit_zone_group">
              <select class="browser-default zone" id="edit_zone">
                <option value="" disabled selected><?php echo $this->lang->line('zone') ?></option>
              </select>
              <small id="labeledit_zone" for="zone" ></small>
          </div> -->
          <div class="row col s12" id="edit_province_group">
              <select class="browser-default province" id="edit_province" name="province">
                <option value="" disabled selected><?php echo $this->lang->line('province')?></option>
              </select>
              <small id="labeladd_province" for="province" ></small>
          </div>
          <div class="row col s12" id="edit_aumphur_group">
              <select class="browser-default aumphur" id="edit_aumphur" name="aumphur">
                <option value="" disabled selected><?php echo $this->lang->line('aumphur')?></option>
              </select>
              <small id="labeladd_aumphur" for="aumphur" ></small>
          </div>
          <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('working_day') ?> <i class="fas fa-angle-double-left"></i></h6>
          <div class="row center">
            <div class="col s6" id="edit_work_full_time_group" style="margin-bottom: 20px;">
                <input type="checkbox" id="edit_work_full_time" onclick="edit_check_work()"/><label for="edit_work_full_time"><?php echo $this->lang->line('work_full_time') ?></label>
                <small id="labeledit_work_full_time" for="edit_work_full_time" ></small>
            </div>
            <div class="col s6" id="edit_shift_work_group" style="margin-bottom: 20px;">
                <input type="checkbox" id="edit_shift_work" onclick="edit_check_work()"/><label for="edit_shift_work"><?php echo $this->lang->line('shift_work') ?></label>
                <small id="labeledit_shift_work" for="edit_shift_work" ></small>
            </div>
            <small id="labeledit_working" for="working" ></small>
          </div>
          <div id="edit_working_group" style="display:none">
            <div class="row">
              <small class="col s4"><?php echo $this->lang->line('working_day') ?></small>
              <small class="col s4"><?php echo $this->lang->line('working_start') ?></small>
              <small class="col s4"><?php echo $this->lang->line('working_end') ?></small>
            </div>
            <div class="row">
              <div class="col s4" id="edit_working_day_group">
                  <select class="browser-default working_day" id="edit_working_day" onfocus="rmErr(id);" onclick="rmErr(id);">
                    <option value="" disabled selected><?php echo $this->lang->line('working_day') ?></option>
                  </select>
                  <small id="labeledit_working_day" for="edit_working_day" ></small>
              </div>
              <div class="col s4" id="edit_working_start_group">
                  <input id="edit_working_start" name="working_start" class="timepicker" type="text" placeholder="<?php echo $this->lang->line('working_start') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                  <small id="labeledit_working_start" for="edit_working_start" ></small>
              </div>
              <div class="col s4" id="edit_working_end_group">
                  <input id="edit_working_end" name="working_end" type="text" class="timepicker" placeholder="<?php echo $this->lang->line('working_end') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                  <small id="labeledit_working_end" for="edit_working_end" ></small>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s6" id="edit_working_start_date_group">
                <input id="edit_working_start_date" name="working_start_date" class="datepicker" type="text" data-toggle="datepicker" placeholder="<?php echo $this->lang->line('working_start_date') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                <small id="labeledit_working_start_date" for="working_start_date" ></small>
            </div>
            <div class="col s6" id="edit_working_end_date_group">
                <input id="edit_working_end_date" name="working_end_date" type="text" class="datepicker" data-toggle="datepicker" placeholder="<?php echo $this->lang->line('working_end_date') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                <small id="labeledit_working_end_date" for="working_end_date" ></small>
            </div>
          </div>
          <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i><?php echo $this->lang->line('property_label') ?> <i class="fas fa-angle-double-left"></i></h6>
          <div class="row">
            <small class="col s6"><?php echo $this->lang->line('age') ?></small>
            <small class="col s3"><?php echo $this->lang->line('gender') ?></small>
            <small class="col s3"><?php echo $this->lang->line('license') ?></small>
          </div>
          <div class="row">
            <div class="col s3" id="edit_age_min_group">
                <input id="edit_age_min" name="age_min" type="number" min="0" max="99" placeholder="<?php echo $this->lang->line('age_min') ?>" value="<?php echo $profile['age_min']?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" onblur="check_edit_age()" required>
                <small id="labeledit_age_min" for="age_min" ></small>
            </div>
            <div class="col s3" id="edit_age_max_group">
                <input id="edit_age_max" name="age_max" type="number" min="0" max="99" placeholder="<?php echo $this->lang->line('age_max') ?>" value="<?php echo $profile['age_max']?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" onblur="check_edit_age()" required>
                <small id="labeledit_age_max" for="age_max" ></small>
            </div>
            <div class="col s3" id="gender_group">
                <select class="browser-default gender" id="edit_gender" onfocus="rmErr(id);" onclick="rmErr(id);">
                  <option value="" disabled selected><?php echo $this->lang->line('gender') ?></option>
                </select>
                <small id="labeledit_gender" for="gender" ></small>
            </div>
            <div class="col s3" id="edit_license_group">
                <select class="browser-default license" id="edit_license" onfocus="rmErr(id);" onclick="rmErr(id);">
                  <option value="" disabled selected><?php echo $this->lang->line('license') ?></option>
                </select>
                <small id="labeledit_license" for="license" ></small>
            </div>
          </div>
          <div class="row col s12" id="edit_education_group">
              <select class="browser-default education" id="edit_education">
                <option value="" disabled selected><?php echo $this->lang->line('education') ?></option>
              </select>
              <small id="labeledit_education" for="edit_education" ></small>
          </div>
          <div class="col s12" id="edit_property_group">
              <textarea id="edit_property" class="materialize-textarea" placeholder="<?php echo $this->lang->line('property') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required><?php echo $profile['property']?></textarea>
              <small id="labeledit_property" for="edit_property" ></small>
          </div>
          <div class="row col s12" id="edit_experience_group">
              <select class="browser-default experience" id="edit_experience" onfocus="rmErr(id);" onclick="rmErr(id);">
                <option value="" disabled selected><?php echo $this->lang->line('experience') ?></option>
                <?php echo ($profile['experience'] == '99') ? "<option value='99' selected > ไม่ระบุ</option>" : "<option value='99'> ไม่ระบุ</option>"; ?>
                <?php echo ($profile['experience'] == '0') ? "<option value='0' selected > 0 ปี </option>" : "<option value='0'> 0 ปี </option>"; ?>
                <?php echo ($profile['experience'] == '1') ? "<option value='1' selected > 1 ปีขึ้นไป </option>" : "<option value='1'> 1 ปีขึ้นไป </option>"; ?>
                <?php echo ($profile['experience'] == '2') ? "<option value='2' selected > 2 ปีขึ้นไป </option>" : "<option value='2'> 2 ปีขึ้นไป </option>"; ?>
                <?php echo ($profile['experience'] == '3') ? "<option value='3' selected > 3 ปีขึ้นไป </option>" : "<option value='3'> 3 ปีขึ้นไป </option>"; ?>
                <?php echo ($profile['experience'] == '5') ? "<option value='5' selected > 5 ปีขึ้นไป </option>" : "<option value='5'> 5 ปีขึ้นไป </option>"; ?>
                <?php echo ($profile['experience'] == '7') ? "<option value='7' selected > 7 ปีขึ้นไป </option>" : "<option value='7'> 7 ปีขึ้นไป </option>"; ?>
                <?php echo ($profile['experience'] == '10') ? "<option value='10' selected > 10 ปีขึ้นไป </option>" : "<option value='10'> 10 ปีขึ้นไป </option>";  ?>
              </select>
              <small id="labeledit_experience" for="experience" ></small>
          </div>
          <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('income_label') ?> <i class="fas fa-angle-double-left"></i></h6>
          <div class="col s12" id="income_group" style="margin-bottom: 20px;">
              <input type="checkbox" id="edit_income" onclick="checkEditIncome()"/><label for="income"><?php echo $this->lang->line('income') ?></label>
              <small id="labelincome" for="income" ></small>
          </div>
          <div class="row">
            <div class="col s6" id="edit_income_min_group">
                <input id="edit_income_min" name="edit_income_min" type="text" placeholder="<?php echo $this->lang->line('income_min') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                <small id="labeledit_income_min" for="edit_income_min" ></small>
            </div>
            <div class="col s6" id="edit_income_max_group">
                <input id="edit_income_max" name="edit_income_max" type="text"placeholder="<?php echo $this->lang->line('income_max') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                <small id="labelinedit_come_max" for="edit_income_max" ></small>
            </div>
          </div>
          <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('benefits') ?> <i class="fas fa-angle-double-left"></i></h6>
          <div class="col s12" id="edit_benefits_group">
              <textarea id="edit_benefits" class="materialize-textarea" placeholder="<?php echo $this->lang->line('benefits') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required><?php echo $profile['benefits']?></textarea>
              <small id="labeledit_benefits" for="edit_benefits" ></small>
          </div>
          <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('remark') ?> <i class="fas fa-angle-double-left"></i></h6>
          <div class="col s12" id="edit_remark_group">
              <textarea id="edit_remark" class="materialize-textarea" placeholder="<?php echo $this->lang->line('remark') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required><?php echo $profile['remark']?></textarea>
              <small id="labeledit_remark" for="remark" ></small>
          </div>
          <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('staff') ?> <i class="fas fa-angle-double-left"></i></h6>
          <div class="row col s12" id="staff_group">
              <select class="browser-default staff" id="staff" name="staff">
                <option value="" disabled selected><?php echo $this->lang->line('staff')?></option>
              </select>
              <small id="labelstaff" for="staff" ></small>
          </div>
        </div>
      </div>

    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat" id="edit_job" onclick="edit_job(<?php echo "'".$profile["announce_id"]."'" ?>)" >แก้ไข</a>
      <a class="modal-close waves-effect btn-flat closemodal">ปิด</a>
    </div>
  </div>
</div>
