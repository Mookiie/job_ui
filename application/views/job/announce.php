<div class="main-container">
  <?php header('Access-Control-Allow-Origin: *'); ?>
  <div class="row">
    <div class="advert">
      <input type="hidden" id="announce_id" name="announce_id" value="<?php echo base64_decode(base64_decode(base64_decode($_GET["announce"]))); ?>">
      <input type="hidden" id="link_name" name="link_name" value="<?php echo base64_decode(base64_decode(base64_decode($_GET["link_name"]))); ?>">
      <input type="hidden" id="link_infor" name="link_infor" value="<?php echo base64_decode(base64_decode(base64_decode($_GET["link_infor"]))); ?>">
      <div class="menu">
        <div class="row">
          <a class="btn btn-j waves-effect" id="regis_job" >สมัครงาน</a>
          <a class="btn btn-white" id="favorite_job" ><i class="fas fa-star"></i><span>บันทึกงานนี้</span></a>
          <!-- <a class="btn btn-white" id="print_job" ><i class="fas fa-print"></i><span>ปริ้นท์</span></a>
          <a class="btn btn-white" id="share_job" ><i class="fas fa-share-alt"></i><span>แชร์</span></a> -->
        </div>
      </div>
      <div class="content">
        <div class="header">
          <div class="row">
            <div class="col s12 l4 center">
              <img class="header-logo" id="company_logo" >
              <!-- <img class="background-logo" src="assets/images/background/home.png" alt=""> -->
            </div>
            <div class="col s12 l7">
              <span class="text-head" id="company_name"></span>
              <span class="text-wrap" id="company_detail"></span>
              <span id="company_update"></span>
            </div>
          </div>
        </div>
        <div class="title">
          <div class="col s12 l6 ">
            <!-- <span class="title-head white-text text-b" id="announce_title">รับสมัคร</span>
            <div class="divider"></div>
            <span class="title-job_description white-text text-t" id="job_description">งานจัดสวน</span> -->
            <span class="title-head white-text">รับสมัคร </span><span class="title-head white-text" id="job_description"></span>
          </div>
          <div class="col s12 l6 right-align">
            <!-- <img class="title-location" src="assets/images/icon/location.png" alt="location"> -->
            <i class="fas fa-map-marker-alt title-location"></i>
            <span id="location_name" class="title-location-text"></span>
          </div>
        </div>
        <div class="details">
          <div class="col s12 l6">
            <div class="col s12 center">
              <img class="details-image" id="announce_image" src="" alt="">
            </div>
            <div class="col s12">
              <h2 class="details-header text-b">รายละเอียดงาน</h2>
              <span class="details-content"><?php echo $this->lang->line('type_job'); ?> : <span id="type_job"></span></span>
              <span class="details-content"><?php echo $this->lang->line('working_day'); ?> : <span id="working_day"></span><span id="working_time"></span></span>
              <span class="details-content"><?php echo $this->lang->line('working_start_date'); ?> : <span id="working_start_date"></span>
              <span class="details-content"><span id="announce_detail"></span></span>
            </div>
            <div class="col s12">
              <h2 class="details-header text-b"><?php echo $this->lang->line('benefits'); ?></h2>
              <span class="details-content"><span class="details-content" id="announce_benefits"></span></span>
            </div>
          </div>
          <div class="col s12 l6">
            <div class="col s12">
              <img class="salary-image" src="assets/images/icon/salary_text.png" alt="salary">
              <h2 class="details-header reset-margin" id="salary"></h2>
            </div>
            <div class="col s12">
              <h2 class="details-header text-b"><?php echo $this->lang->line('qualification'); ?></h2>
              <span class="details-content"><?php echo $this->lang->line('gender'); ?> : <span id="gender"></span></span>
              <span class="details-content"><?php echo $this->lang->line('age'); ?> : <span id="age"></span></span>
              <span class="details-content"><?php echo $this->lang->line('education'); ?> : <span id="education"></span></span>
              <span class="details-content"><?php echo $this->lang->line('license'); ?> : <span id="license"></span></span>
              <span class="details-content"><?php echo $this->lang->line('property'); ?> : <span id="announce_property"></span></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="announce">
      <div class="announce-header">
        <div class="row">
          <img src="assets/images/icon/announce_header.png" alt="announce header">
          <span class="white-text"><?php echo $this->lang->line('announce_header_similar');?></span>
        </div>
      </div>
      <div class="row" id="data_not_found" style="display:none">
        <h4>ไม่พบข้อมูล</h4>
      </div>
      <div class="row content-announce">
      </div>
    </div>
    </div>
  </div>
</div>
