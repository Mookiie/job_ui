<!-- <div id="DetailDialog"  class="modal ">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span><?php echo $profile["announce_title"]?></span><br />
      <?php
      $job_description_1 = $profile['job_description_1'][0]['job_description_name'];
        if (array_key_exists("job_description_2",$profile)){
          if( count($profile['job_description_2']) && $profile['job_description_2'][0]['job_description_code_2'] != '999') {
            $job_description_1 .= " (".$profile['job_description_2'][0]['job_description_name'].")";
        }}
       ?>
      <span><?php echo $this->lang->line('job_description_1')." : "; ?></span><span id="job_description_1"><?php echo $job_description_1 ;?></span>
    </div>
    <div>
      <div class="container content-detail">
        <input type="hidden" id="announce_id" value="">
        <?php $logo_company = $profile['company']['company']['image'] ?>
        <div class='row col s12 center' style='margin-bottom: 10px;'><img height="50" id='logo_company' scr="<?php echo $logo_company; ?>" ></div>
        <h6 class="center"><span id="company_name"><?php echo $profile['company']['company']['company_name']?></span></h6>
        <div class='row col s12 center' style='margin-bottom: 10px;'>
        <h6>
          <i class="fas fa-building"></i>
          &nbsp
          <span id="company_kind"><?php echo $profile['company']['company_kind']['name']?></span>
          &nbsp&nbsp&nbsp&nbsp
          <i class="fas fa-map-marker-alt"></i>
          &nbsp
          <span id="location"><?php echo $profile['company']['pro_id']['name']?></span>
        </h6>
        </div>
        <div class="divider"></div>
        <h6 class="center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('qualification'); ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="divider"></div>
        <div class="row col s12 container-detail">
          <?php echo $this->lang->line('type_job'); ?> : <span id="type_job"><?php echo $profile['type_job'][0]['name']?></span> <br />
          <?php
          $income;
          if ($profile['income_max'] != $profile['income_min']) {
            $income = $profile['income_min']+" - "+$profile['income_max'];
          }else {
            $income = $this->lang->line('income');
          }
           ?>
          <?php echo $this->lang->line('income_label'); ?> : <span id="income"><?php echo $income?></span> <br />
          <?php echo $this->lang->line('gender'); ?> : <span id="gender"><?php echo $profile['gender'][0]['name']?></span> <br />
          <?php echo $this->lang->line('age'); ?> : <span id="age"><?php echo $profile['age_min']." - ".$profile['age_max']." ปี"?></span> <br />
          <?php echo $this->lang->line('education'); ?> : <span id="education"><?php echo $profile['education'][0]['name']?></span> <br />
          <?php echo $this->lang->line('license'); ?> : <span id="license"><?php echo $profile['license'][0]['driving_license_type_name']?></span> <br />
          <?php
            if ($profile['working_start'] != '00:00' && $profile['working_end'] != '00:00') {
              $working_time = "(".$profile['working_start']." - ".$profile['working_end'].")";
            }else {
              $working_time = "";
            }
          ?>
          <?php echo $this->lang->line('working_day'); ?> : <span id="working_day"><?php echo $profile['working_day'][0]['name']?></span> <span id="working_time"><?php echo $working_time?></span><br />
          <?php echo $this->lang->line('property'); ?> : <br /><dd>  <span id="property"><?php echo nl2br($profile['property'])?></span> </dd><br />
          <?php
              $work_start_date;
              if ($profile['work_start_date'] == '0000-00-00 00:00:00') {
                $work_start_date = $this->lang->line('none');
              }else {
                $work_start_date = $profile['work_start_date'];
              }
          ?>
          <?php echo $this->lang->line('working_start_date'); ?> : <span id="working_start_date"><?php echo $work_start_date?></span> <br />
        </div>
        <div class="divider"></div>
        <h6 class="center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('benefits'); ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="divider"></div>
        <div class="row col s12">
          <span id="benefits"><?php echo nl2br($profile['benefits'])?></span> <br />
        </div>
        <div id="contact-company" style="display:none;">
          <div class="divider"></div>
          <h6 class="center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('contact'); ?> <i class="fas fa-angle-double-left"></i></h6>
          <div class="divider"></div>
          <div class="row col s12">
            <?php echo $this->lang->line('fullname'); ?> : <span id="fullname"><?php echo $profile['create_by']['fname']." ".$profile['create_by']['lname']; ?></span> <br />
            <?php echo $this->lang->line('email'); ?> : <span id="email_contact"><?php echo $profile['create_by']['email']?></span> <br />
            <?php echo $this->lang->line('tel'); ?> : <span id="tel"><?php echo $profile['create_by']['tel']?></span> <br />
          </div>
        </div>
        <div class="divider"></div>
        <h6 class="center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('about'); ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="divider"></div>
        <div class="row col s12">
          <div class='row col s12 ' style='margin-bottom: 10px;'><span id="company_detail"><?php echo nl2br($profile['company']['company_detail'])?></span></div>
          <div class='row col s12 ' style='margin-bottom: 10px;'><?php echo $this->lang->line('address'); ?></div>
          <?php
               $address = "";
              if ($profile['company']['company_address'] != '') {
                 $address .=  $profile['company']['company_address'];
              }if ($profile['company']['tumbon_id'] != '') {
                 $address .= " ".$this->lang->line('tumbon')." ".$profile['company']['tumbon_id']['name'];
              }if ($profile['company']['aumphur_id'] != '') {
                $address .= " ".$this->lang->line('aumphur')." ".$profile['company']['aumphur_id']['name'];
              }if ($profile['company']['pro_id'] != '') {
                $address .= " ".$this->lang->line('province')." ".$profile['company']['pro_id']['name'];
              }if (($profile['company']['company_address'] != '')&&($profile['company']['tumbon_id']!= '')&&($profile['company']['aumphur_id'] != '')&&($profile['company']['pro_id'] != '')) {
                $address .= " ".$profile['company']['tumbon_id']['zipcode'];
              }
           ?>
          <div class='row col s12 ' style='margin-bottom: 10px;'><span id="company_address"><?php echo $address; ?></span></div>
          <?php $company_map = $profile['company']['map']?>
          <div class='row col s12 center' style='margin-bottom: 10px;'><img id='company_map' scr="<?php echo $company_map; ?>" ></div>
        </div>
      </div>

    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat" id="apply_job" onclick="apply_job(<?php echo "'".$profile["announce_id"]."'" ?>)" >สมัครงาน</a>
      <a class="modal-close waves-effect btn-flat closemodal">ปิด</a>
    </div>
  </div>
</div> -->

<div id="DetailDialog" class="modal">
  <div class="modal-dialog sm">

    <div class="modal-header">
      <span class="white-text"><?php echo $profile["announce_title"]?></span><br />
      <div class="divider"></div>
      <?php
      $job_description_1 = $profile['job_description_1'][0]['position_name'];
        // if (array_key_exists("job_description_2",$profile)){
        //   if( count($profile['job_description_2']) && $profile['job_description_2'][0]['job_description_code_2'] != '999') {
        //     $job_description_1 .= " (".$profile['job_description_2'][0]['job_description_name'].")";
        // }}
       ?>
      <span class="white-text"><?php echo $this->lang->line('job_description_1')." : "; ?></span><span class="white-text" id="job_description_1"><?php echo $job_description_1 ;?></span>
    </div>

    <div class="modal-content">
        <input type="hidden" id="announce_id" value="">
        <?php $logo_company = $profile['company']['company']['image'] ?>
        <div class='row col s12 center' style='margin-bottom: 10px;'><img height="50" id='logo_company' scr="<?php echo $logo_company; ?>" ></div>
        <h6 class="center"><span id="company_name"><?php echo $profile['company']['company']['company_name']?></span></h6>
        <div class='row col s12 center' style='margin-bottom: 10px;'>
        <h6>
          <i class="fas fa-building"></i>
          &nbsp
          <span id="company_kind"><?php echo $profile['company']['company_kind']['name']?></span>
          &nbsp&nbsp&nbsp&nbsp
          <i class="fas fa-map-marker-alt"></i>
          &nbsp
          <span id="location"><?php echo $profile['company']['pro_id']['name']?></span>
        </h6>
        </div>
        <div class="divider"></div>
        <h6 class="center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('qualification'); ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="divider"></div>
        <div class="row col s12 container-detail">
          <?php echo $this->lang->line('type_job'); ?> : <span id="type_job"><?php echo $profile['type_job'][0]['name']?></span> <br />
          <?php
          $income;
          if ($profile['income_max'] != $profile['income_min']) {
            $income = $profile['income_min']+" - "+$profile['income_max'];
          }else {
            $income = $this->lang->line('income');
          }
           ?>
          <?php echo $this->lang->line('income_label'); ?> : <span id="income"><?php echo $income?></span> <br />
          <?php echo $this->lang->line('gender'); ?> : <span id="gender"><?php echo $profile['gender'][0]['name']?></span> <br />
          <?php echo $this->lang->line('age'); ?> : <span id="age"><?php echo $profile['age_min']." - ".$profile['age_max']." ปี"?></span> <br />
          <?php echo $this->lang->line('education'); ?> : <span id="education"><?php echo $profile['education'][0]['name']?></span> <br />
          <?php echo $this->lang->line('license'); ?> : <span id="license"><?php echo $profile['license'][0]['driving_license_type_name']?></span> <br />
          <?php
            if ($profile['working_start'] != '00:00' && $profile['working_end'] != '00:00') {
              $working_time = "(".$profile['working_start']." - ".$profile['working_end'].")";
            }else {
              $working_time = "";
            }
          ?>
          <?php echo $this->lang->line('working_day'); ?> : <span id="working_day"><?php echo $profile['working_day'][0]['name']?></span> <span id="working_time"><?php echo $working_time?></span><br />
          <?php echo $this->lang->line('property'); ?> : <br /><dd>  <span id="property"><?php echo nl2br($profile['property'])?></span> </dd><br />
          <?php
              $work_start_date;
              if ($profile['work_start_date'] == '0000-00-00 00:00:00') {
                $work_start_date = $this->lang->line('none');
              }else {
                $work_start_date = $profile['work_start_date'];
              }
          ?>
          <?php echo $this->lang->line('working_start_date'); ?> : <span id="working_start_date"><?php echo $work_start_date?></span> <br />
        </div>
        <div class="divider"></div>
        <h6 class="center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('benefits'); ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="divider"></div>
        <div class="row col s12">
          <span id="benefits"><?php echo nl2br($profile['benefits'])?></span> <br />
        </div>
        <div id="contact-company" style="display:none;">
          <div class="divider"></div>
          <h6 class="center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('contact'); ?> <i class="fas fa-angle-double-left"></i></h6>
          <div class="divider"></div>
          <div class="row col s12">
            <?php echo $this->lang->line('fullname'); ?> : <span id="fullname"><?php echo $profile['create_by']['fname']." ".$profile['create_by']['lname']; ?></span> <br />
            <?php echo $this->lang->line('email'); ?> : <span id="email_contact"><?php echo $profile['create_by']['email']?></span> <br />
            <?php echo $this->lang->line('tel'); ?> : <span id="tel"><?php echo $profile['create_by']['tel']?></span> <br />
          </div>
        </div>
        <div class="divider"></div>
        <h6 class="center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('about'); ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="divider"></div>
        <div class="row col s12">
          <div class='row col s12 ' style='margin-bottom: 10px;'><span id="company_detail"><?php echo nl2br($profile['company']['company_detail'])?></span></div>
          <div class='row col s12 ' style='margin-bottom: 10px;'><?php echo $this->lang->line('address'); ?></div>
          <?php
               $address = "";
              if ($profile['company']['company_address'] != '') {
                 $address .=  $profile['company']['company_address'];
              }if ($profile['company']['tumbon_id'] != '') {
                 $address .= " ".$this->lang->line('tumbon')." ".$profile['company']['tumbon_id']['name'];
              }if ($profile['company']['aumphur_id'] != '') {
                $address .= " ".$this->lang->line('aumphur')." ".$profile['company']['aumphur_id']['name'];
              }if ($profile['company']['pro_id'] != '') {
                $address .= " ".$this->lang->line('province')." ".$profile['company']['pro_id']['name'];
              }if (($profile['company']['company_address'] != '')&&($profile['company']['tumbon_id']!= '')&&($profile['company']['aumphur_id'] != '')&&($profile['company']['pro_id'] != '')) {
                $address .= " ".$profile['company']['tumbon_id']['zipcode'];
              }
           ?>
          <div class='row col s12 ' style='margin-bottom: 10px;'><span id="company_address"><?php echo $address; ?></span></div>
          <?php $company_map = $profile['company']['map']?>
          <div class='row col s12 center' style='margin-bottom: 10px;'><img id='company_map' scr="<?php echo $company_map; ?>" ></div>
        </div>

        <a class="btn waves-effect" id="apply_job" onclick="apply_job(<?php echo "'".$profile["announce_id"]."'" ?>)" >สมัครงาน</a>
        <a class="btn waves-effect closemodal">ปิด</a>

      </div>

    </div>


  </div>
</div>
