<div id="InviteDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span>เพิ่มผู้ใช้งาน</span>
    </div>
    <div>
    <div class="row container">
      <div class="form-group ">
        <h6>ข้อมูลส่วนตัว</h6>
        <div class="col s6" id="fname_group">
            <input id="fname" name="fname" type="text" placeholder="<?php echo $this->lang->line('fname') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelfname" for="fname" ></small>
        </div>
        <div class="col s6" id="lname_group">
            <input id="lname" name="lname" type="text" placeholder="<?php echo $this->lang->line('lname') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labellname" for="lname" ></small>
        </div>
        <div class="col s12" id="tel_group">
            <input id="tel" name="tel" type="tel" placeholder="<?php echo $this->lang->line('tel') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labeltel" for="tel" ></small>
        </div>
        <div class="col s12" id="email_group">
            <input id="email" name="email" type="text" placeholder="<?php echo $this->lang->line('email') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelemail" for="email" ></small>
        </div>
        <div class="col s12" id="level_group">
            <select class="browser-default level" id="level" onfocus="rmErr(id);" onclick="rmErr(id);">
              <option value="" disabled selected><?php echo $this->lang->line('level') ?></option>
            </select>
            <small id="labellevel" for="level" ></small>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat" onclick="Invite()"><?php echo $this->lang->line('invite') ?></a>
      <a class="modal-close waves-effect btn-flat closemodal"><?php echo $this->lang->line('close') ?></a>
    </div>
  </div>
</div>
