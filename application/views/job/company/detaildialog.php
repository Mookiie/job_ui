<div id="DetailDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span>ข้อมูลบริษัท</span>
    </div>
    <div>
    <div class="row container">
      <div class="form-group ">
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> ข้อมูลทั่วไป <i class="fas fa-angle-double-left"></i></h6>
        <div class="col s12" id="comp_name_group">
            <input id="comp_name" name="company_name" type="text"  data-length="100" placeholder="<?php echo $this->lang->line('company_name') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelcompany_name" for="company_name" ></small>
        </div>
        <div class="col s12" id="tin_group">
            <input id="comp_tin" name="tin" type="text"  data-length="13" placeholder="<?php echo $this->lang->line('tin') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" readonly>
            <small id="labeltin" for="tin" ></small>
        </div>
        <div class="col s12" id="comp_tel_group">
            <input id="comp_tel" name="company_tel" type="text"  data-length="10" placeholder="<?php echo $this->lang->line('company_tel') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelcompany_tel" for="company_tel" ></small>
        </div>
        <div class="col s12" id="comp_web_group">
            <input id="comp_web" name="web" type="text" placeholder="<?php echo $this->lang->line('web') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelweb" for="web" ></small>
        </div>
        <div class="row col s12" id="comp_kind_group">
            <select class="browser-default company_kind" id="comp_kind" name="company_kind">
              <option value="" disabled selected><?php echo $this->lang->line('company_kind')?></option>
            </select>
            <small id="labelcomp_kind" for="company_kind_job" ></small>
        </div>
        <div class="col s12" id="comp_kind_detail_group" style="display:none">
            <input id="comp_kind_detail" name="comp_kind_detail" type="text"  data-length="100" placeholder="<?php echo $this->lang->line('company_kind_detail') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelcomp_kind_detail" for="comp_kind_detail" ></small>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i><?php echo $this->lang->line('address') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="col s12" id="comp_address_group">
            <input id="comp_address" name="company_address" type="text"  placeholder="<?php echo $this->lang->line('company_address') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelcomp_address" for="company_address" ></small>
        </div>
        <div class="row col s12" id="comp_province_group">
            <select class="browser-default province" id="comp_province" name="province">
              <option value="" disabled selected><?php echo $this->lang->line('province')?></option>
            </select>
            <small id="labelcomp_province" for="province" ></small>
        </div>
        <div class="row col s12" id="comp_aumphur_group">
            <select class="browser-default aumphur" id="comp_aumphur" name="aumphur">
              <option value="" disabled selected><?php echo $this->lang->line('aumphur')?></option>
            </select>
            <small id="labelaumphur" for="aumphur" ></small>
        </div>
        <div class="row col s12" id="comp_tumbon_group">
            <select class="browser-default tumbon" id="comp_tumbon" name="tumbon">
              <option value="" disabled selected><?php echo $this->lang->line('tumbon')?></option>
            </select>
            <small id="labeltumbon" for="tumbon_id" ></small>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i><?php echo $this->lang->line('map') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="row col s12" id="map_group">
          <div class="file-field input-field">
           <div class="btn">
             <span style="color:#ffffff;"><?php echo $this->lang->line('map')?></span>
             <input type="file" id="file_map" name="file_map">
           </div>
           <div class="file-path-wrapper">
             <input class="form-control file-path" type="text" id="nfile" name="nfile">
           </div>
          </div>
          <small id="labelmap" for="map" ></small>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i><?php echo $this->lang->line('travel') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="col s12" id="comp_travel_group">
            <textarea id="comp_travel" class="materialize-textarea" placeholder="<?php echo $this->lang->line('travel') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required></textarea>
            <small id="labelcomp_travel" for="travel" ></small>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat" onclick="add_detail()"><?php echo $this->lang->line('savedetail') ?></a>
      <a class="modal-close waves-effect btn-flat closemodal"><?php echo $this->lang->line('close') ?></a>
    </div>
  </div>
</div>
