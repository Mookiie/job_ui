<div id="countViewRegisDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span class="white-text">ข้อมูลการเข้าชม</span>
      <i class="fas fa-edit white-text right" id="edit"></i>
      <i class="fas fa-trash white-text right" id="trash"></i>
    </div>
    <div>
    <div class="row container">
      <ul id="tabs-setting" class="tabs">
        <li class="tab"><a class="active" href="#view"><?php echo $this->lang->line('view') ?></a></li>
        <li class="tab"><a  href="#apply"><?php echo $this->lang->line('apply') ?></a></li>
        <li class="tab"><a  href="#myLink"><?php echo $this->lang->line('infor') ?></a></li>
      </ul>

      <div class="row" id="view">

         <ul class="collapsible" id="content-view">

         </ul>
      </div>

      <div class="row" id="apply">
         <ul class="collapsible" id="content-apply">
         </ul>
      </div>

      <div  class="row" id="myLink">
        <div class="row col s12" id="infor_group">
            <select class="browser-default infor" id="infor" name="infor">
              <option value="" disabled selected><?php echo $this->lang->line('infor')?></option>
            </select>
            <small id="labelinfor" for="infor" ></small>
        </div>
        <div class="row col s12 link display-disable" >
          <span class="display-disable" id="link"></span>
          <a class="btn btn-j waves-effect right" onclick="copyLink('#link')" id="copy" ><?php echo $this->lang->line('copy')?></a>
        </div>
      </div>


    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat closemodal"><?php echo $this->lang->line('close') ?></a>
    </div>
  </div>
</div>
