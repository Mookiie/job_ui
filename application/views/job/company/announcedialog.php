<div id="AnnounceDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span>เพิ่มประกาศ</span>
    </div>
    <div>
    <div class="row container">
      <!-- <div class="form-group "> -->
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> ข้อมูลงาน <i class="fas fa-angle-double-left"></i></h6>
        <div class="col s12" id="announce_title_group">
            <input id="announce_title" name="announce_title" type="text" data-length="100" placeholder="<?php echo $this->lang->line('announce_title') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelannounce_title" for="announce_title" ></small>
        </div>
        <div class="row col s12" id="type_job_group">
            <select class="browser-default type_job" id="type_job" name="type_job">
              <option value="" disabled selected><?php echo $this->lang->line('type_job')?></option>
            </select>
            <small id="labeltype_job" for="type_job" ></small>
        </div>
        <!-- <div class="row col s12" style="padding: 0 !important;">
          <div class="col s6" id="job_description_1_group">
              <select class="browser-default job_description_1" id="job_description_1">
                <option value="" disabled selected><?php echo $this->lang->line('job_description_1') ?></option>
              </select>
              <small id="labeljob_description_1" for="job_description_1" ></small>
          </div>
          <div class="col s6" id="job_description_2_group">
              <select class="browser-default job_description_2" id="job_description_2" disabled="disabled">
                <option value="" disabled selected><?php echo $this->lang->line('job_description_2') ?></option>
              </select>
              <small id="labeljob_description_2" for="job_description_2" ></small>
          </div>
        </div> -->
        <div class="row col s12" id="job_position_group">
            <select class="browser-default job_position" id="job_position">
              <option value="" disabled selected><?php echo $this->lang->line('job_position') ?></option>
            </select>
            <small id="labeljob_position" for="job_position" ></small>
        </div>
        <div class="col s12" id="detail_group">
            <textarea id="detail" class="materialize-textarea" placeholder="<?php echo $this->lang->line('detail') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required></textarea>
            <small id="labeldetail" for="detail" ></small>
        </div>
        <!-- <div class="row col s12" id="zone_group">
            <select class="browser-default zone" id="zone">
              <option value="" disabled selected><?php echo $this->lang->line('zone') ?></option>
            </select>
            <small id="labelzone" for="zone" ></small>
        </div> -->
        <div class="row col s12" id="add_province_group">
            <select class="browser-default province" id="add_province" name="province">
              <option value="" disabled selected><?php echo $this->lang->line('province')?></option>
            </select>
            <small id="labeladd_province" for="province" ></small>
        </div>
        <div class="row col s12" id="add_aumphur_group">
            <select class="browser-default aumphur" id="add_aumphur" name="aumphur">
              <option value="" disabled selected><?php echo $this->lang->line('aumphur')?></option>
            </select>
            <small id="labeladd_aumphur" for="aumphur" ></small>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('working_day') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="row center">
          <div class="col s6" id="work_full_time_group" style="margin-bottom: 20px;">
              <input type="checkbox" id="work_full_time" onclick="check_work()"/><label for="work_full_time"><?php echo $this->lang->line('work_full_time') ?></label>
              <small id="labelwork_full_time" for="work_full_time" ></small>
          </div>
          <div class="col s6" id="shift_work_group" style="margin-bottom: 20px;">
              <input type="checkbox" id="shift_work" onclick="check_work()"/><label for="shift_work"><?php echo $this->lang->line('shift_work') ?></label>
              <small id="labelshift_work" for="shift_work" ></small>
          </div>
          <small id="labelworking" for="working" ></small>
        </div>
        <div id="working_group" style="display:none">
          <div class="row">
            <small class="col s4"><?php echo $this->lang->line('working_day') ?></small>
            <small class="col s4"><?php echo $this->lang->line('working_start') ?></small>
            <small class="col s4"><?php echo $this->lang->line('working_end') ?></small>
          </div>
          <div class="row">
            <div class="col s4" id="working_day_group">
                <select class="browser-default working_day" id="working_day" onfocus="rmErr(id);" onclick="rmErr(id);">
                  <option value="" disabled selected><?php echo $this->lang->line('working_day') ?></option>
                </select>
                <small id="labelworking_day" for="working_day" ></small>
            </div>
            <div class="col s4" id="working_start_group">
                <input id="working_start" name="working_start" class="" type="text" placeholder="<?php echo $this->lang->line('working_start') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                <small id="labeledit_working_start" for="working_start" ></small>
            </div>
            <div class="col s4" id="working_end_group">
                <input id="working_end" name="working_end" type="text" class="" placeholder="<?php echo $this->lang->line('working_end') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
                <small id="labelworking_end" for="working_end" ></small>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s6" id="working_start_date_group">
              <input id="working_start_date" name="working_start_date" class="" type="text" placeholder="<?php echo $this->lang->line('working_start_date') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
              <small id="labelworking_start_date" for="working_start_date" ></small>
          </div>
          <div class="col s6" id="working_end_date_group">
              <input id="working_end_date" name="working_end_date" type="text" class="" placeholder="<?php echo $this->lang->line('working_end_date') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
              <small id="labelworking_end_date" for="working_end_date" ></small>
          </div>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i><?php echo $this->lang->line('property_label') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="row">
          <small class="col s6"><?php echo $this->lang->line('age') ?></small>
          <small class="col s3"><?php echo $this->lang->line('gender') ?></small>
          <small class="col s3"><?php echo $this->lang->line('license') ?></small>
        </div>
        <div class="row">
          <div class="col s3" id="age_min_group">
              <input id="age_min" name="age_min" type="number" min="0" max="99" placeholder="<?php echo $this->lang->line('age_min') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" onblur="check_age()" required>
              <small id="labelage_min" for="age_min" ></small>
          </div>
          <div class="col s3" id="age_max_group">
              <input id="age_max" name="age_max" type="number" min="0" max="99" placeholder="<?php echo $this->lang->line('age_max') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" onblur="check_age()" required>
              <small id="labelage_max" for="age_max" ></small>
          </div>
          <div class="col s3" id="gender_group">
              <select class="browser-default gender" id="gender" onfocus="rmErr(id);" onclick="rmErr(id);">
                <option value="" disabled selected><?php echo $this->lang->line('gender') ?></option>
              </select>
              <small id="labelgender" for="gender" ></small>
          </div>
          <div class="col s3" id="license_group">
              <select class="browser-default license" id="license" onfocus="rmErr(id);" onclick="rmErr(id);">
                <option value="" disabled selected><?php echo $this->lang->line('license') ?></option>
              </select>
              <small id="labellicense" for="license" ></small>
          </div>
        </div>
        <div class="row col s12" id="education_group">
            <select class="browser-default education" id="education">
              <option value="" disabled selected><?php echo $this->lang->line('education') ?></option>
            </select>
            <small id="labeleducation" for="education" ></small>
        </div>
        <div class="col s12" id="property_group">
            <textarea id="property" class="materialize-textarea" placeholder="<?php echo $this->lang->line('property') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required></textarea>
            <small id="labelproperty" for="property" ></small>
        </div>
        <div class="row col s12" id="experience_group">
            <select class="browser-default experience" id="experience" onfocus="rmErr(id);" onclick="rmErr(id);">
              <option value="" disabled selected><?php echo $this->lang->line('experience') ?></option>
              <option value="99"> ไม่ระบุ</option>
              <option value="0"> 0 ปี </option>
              <option value="1"> 1 ปีขึ้นไป </option>
              <option value="2"> 2 ปีขึ้นไป </option>
              <option value="3"> 3 ปีขึ้นไป </option>
              <option value="5"> 5 ปีขึ้นไป </option>
              <option value="7"> 7 ปีขึ้นไป </option>
              <option value="10"> 10 ปีขึ้นไป </option>
            </select>
            <small id="labelexperience" for="experience" ></small>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('income_label') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="col s12" id="income_group" style="margin-bottom: 20px;">
            <input type="checkbox" id="income" onclick="checkIncome()"/><label for="income"><?php echo $this->lang->line('income') ?></label>
            <small id="labelincome" for="income" ></small>
        </div>
        <div class="row">
          <div class="col s6" id="income_min_group">
              <input id="income_min" name="income_min" type="text" placeholder="<?php echo $this->lang->line('income_min') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
              <small id="labelincome_min" for="income_min" ></small>
          </div>
          <div class="col s6" id="income_max_group">
              <input id="income_max" name="income_max" type="text" placeholder="<?php echo $this->lang->line('income_max') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
              <small id="labelincome_max" for="income_max" ></small>
          </div>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('file_image') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="file-field input-field">
         <div class="btn">
           <span><?php echo $this->lang->line('file')?></span>
           <input type="file" id="file_image" name="file_image">
         </div>
         <div class="file-path-wrapper">
           <input class="form-control file-path" type="text" id="announce_nfile" name="announce_nfile">
         </div>
        </div>

        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('benefits') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="col s12" id="benefits_group">
            <textarea id="benefits" class="materialize-textarea" placeholder="<?php echo $this->lang->line('benefits') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required></textarea>
            <small id="labelbenefits" for="benefits" ></small>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('remark') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="col s12" id="remark_group">
            <textarea id="remark" class="materialize-textarea" placeholder="<?php echo $this->lang->line('remark') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required></textarea>
            <small id="labelremark" for="remark" ></small>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('staff') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="row col s12" id="staff_group">
            <select class="browser-default staff" id="staff" name="staff">
              <option value="" disabled selected><?php echo $this->lang->line('staff')?></option>
            </select>
            <small id="labelstaff" for="staff" ></small>
        </div>
      </div>
    <!-- </div> -->
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat" onclick="announce()"><?php echo $this->lang->line('save') ?></a>
      <a class="modal-close waves-effect btn-flat closemodal"><?php echo $this->lang->line('close') ?></a>
    </div>
  </div>
</div>
