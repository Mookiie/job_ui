<div id="DetailEditDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span>แก้ไขข้อมูลบริษัท</span>
    </div>
    <div>
    <div class="row container">
      <div class="form-group ">
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> ข้อมูลทั่วไป <i class="fas fa-angle-double-left"></i></h6>
        <div class="col s12" id="ecompany_name_group">
            <input id="ecompany_name" name="company_name" type="text"  data-length="100" placeholder="<?php echo $this->lang->line('company_name') ?>" value="<?php echo $profile['company']['company_name'] ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelecompany_name" for="company_name" ></small>
        </div>
        <div class="col s12" id="etin_group">
            <input id="etin" name="tin" type="text"  data-length="13" placeholder="<?php echo $this->lang->line('tin') ?>" value="<?php echo $profile['company']['tin'] ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" readonly>
            <small id="labeletin" for="tin" ></small>
        </div>
        <div class="col s12" id="ecompany_tel_group">
            <input id="ecompany_tel" name="company_tel" type="text"  data-length="10" placeholder="<?php echo $this->lang->line('company_tel') ?>" value="<?php echo $profile['company_tel'] ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelecompany_tel" for="company_tel" ></small>
        </div>
        <div class="col s12" id="eweb_group">
            <input id="eweb" name="web" type="text" placeholder="<?php echo $this->lang->line('web') ?>" value="<?php echo $profile['web_site'] ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labeleweb" for="web" ></small>
        </div>
        <div class="row col s12" id="ecompany_kind_group">
            <select class="browser-default company_kind" id="ecompany_kind" name="company_kind">
              <option value="" disabled selected><?php echo $this->lang->line('company_kind')?></option>
            </select>
            <small id="labelecompany_kind" for="company_kind_job" ></small>
        </div>
        <div class="col s12" id="ecompany_kind_detail_group" style="display:none">
            <input id="ecompany_kind_detail" name="company_kind_detail" type="text"  data-length="100" placeholder="<?php echo $this->lang->line('company_kind_detail') ?>" value="<?php echo $profile['company_kind_detail'] ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelecompany_kind_detail" for="company_kind_detail" ></small>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i><?php echo $this->lang->line('address') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="col s12" id="ecompany_address_group">
            <input id="ecompany_address" name="company_address" type="text"  placeholder="<?php echo $this->lang->line('company_address') ?>" value="<?php echo $profile['company_address'] ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelecompany_address" for="company_address" ></small>
        </div>
        <div class="row col s12" id="province_group">
            <select class="browser-default province" id="province" name="province">
              <option value="" disabled selected><?php echo $this->lang->line('province')?></option>
            </select>
            <small id="labelprovince" for="province" ></small>
        </div>
        <div class="row col s12" id="aumphur_group">
            <select class="browser-default aumphur" id="aumphur" name="aumphur">
              <option value="" disabled selected><?php echo $this->lang->line('aumphur')?></option>
            </select>
            <small id="labelaumphur" for="aumphur" ></small>
        </div>
        <div class="row col s12" id="tumbon_group">
            <select class="browser-default tumbon" id="tumbon" name="tumbon">
              <option value="" disabled selected><?php echo $this->lang->line('tumbon')?></option>
            </select>
            <small id="labeltumbon" for="tumbon_id" ></small>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i><?php echo $this->lang->line('map') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="row col s12 center" id="map_group">
          <img class="map center" src="<?php echo $profile['map'] ?>" alt="" width="500px">
          <div class="file-field input-field">
           <div class="btn">
             <span style="color:#ffffff;"><?php echo $this->lang->line('map')?></span>
             <input type="file" id="file_map" name="file_map">
           </div>
           <div class="file-path-wrapper">
             <input class="form-control file-path" type="text" id="nfile" name="nfile">
           </div>
          </div>
          <small id="labelmap" for="map" ></small>
        </div>
        <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i><?php echo $this->lang->line('travel') ?> <i class="fas fa-angle-double-left"></i></h6>
        <div class="col s12" id="etravel_group">
            <textarea id="etravel" class="materialize-textarea" placeholder="<?php echo $this->lang->line('travel') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required></textarea>
            <small id="labeletravel" for="travel" ></small>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat" onclick="edit_detail()"><?php echo $this->lang->line('savedetail') ?></a>
      <a class="modal-close waves-effect btn-flat closemodal"><?php echo $this->lang->line('close') ?></a>
    </div>
  </div>
</div>
