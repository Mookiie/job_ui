<div class="main-container">
  <div class="row">
    <div class="advert">
      <!-- <div class="row btn-announce" style="display:none">
        <button class="btn waves-effect waves-light right" id="add_announce" onclick="add_announce()"><?php echo $this->lang->line('add_announce') ?></button>
      </div> -->
      <div class="row">
        <div class="col s6">
          <div class="card" onclick="">
              <div class="card-ticket  center-align">
                  <div class="row">
                     <p><span><?php echo $this->lang->line('count_announce') ?></span> </p>
                     <h3 class="count" id="count-announce" style="margin: 0;"></h3>
                  </div>
              </div>
          </div>
        </div>
        <div class="col s6">
          <div class="card" onclick="">
              <div class="card-ticket  center-align">
                  <div class="row">
                     <p><span><?php echo $this->lang->line('count_announce_me') ?></span> </p>
                     <h3 class="count" id="count-announce-me" style="margin: 0;"></h3>
                  </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col s12 m6">
              <div class="card card-tables">
                <h6 style="padding: 10px;"><i class="fas fa-angle-double-right"></i> ประกาศที่มีคนดูสูงสุด <i class="fas fa-angle-double-left"></i></h6>
                  <div class="card-content">
                      <table class="announce-tables">
                      </table>
                  </div>
              </div>
          </div>
        <div class="col s12 m6">
              <div class="card card-tables">
                <h6 style="padding: 10px;"><i class="fas fa-angle-double-right"></i> ประกาศที่มีคนสมัครสูงสุด <i class="fas fa-angle-double-left"></i></h6>
                <div class="card-content">
                    <table class="announce_me-table">
                    </table>
                </div>
              </div>
          </div>
      </div>
    </div>
    <div class="announce">
      <div class="announce-header">
        <div class="row">
          <img src="../assets/images/icon/announce_header.png" alt="announce header">
          <span class="white-text">ประกาศของฉัน <i class="fas fa-plus-square" onclick="add_announce()"></i></span>
        </div>
      </div>
      <div class="content-announce announceme">
      </div>
    </div>
  </div>
</div>
