<ul id="tabs-setting" class="tabs">
  <li class="tab col s1"><a class="active" href="#users"><?php echo $this->lang->line('users') ?></a></li>
  <li class="tab col s1"><a  href="#company"><?php echo $this->lang->line('company') ?></a></li>
</ul>

<div class="row" id="company">
 <div class="col s12">
 <div class="">
   <div class="row " id="add_detail_group" style="margin-top:10px; display:none">
     <button class="btn waves-effect waves-light right" id="add_detail" onclick="add_detailDialog()"><?php echo $this->lang->line('add_detail') ?></button>
   </div>
   <div class="row " id="edit_detail_group" style="margin-top:10px; display:none">
     <button class="btn waves-effect waves-light right" id="edit_detail" onclick="edit_detailDialog()"><?php echo $this->lang->line('edit_detail') ?></button>
   </div>
 </div>
 <div class="row" id="data_not_found" style="display:none">
   <h4>ไม่พบข้อมูล</h4>
 </div>
 <div class="container-company" id="data_found" style="display:none">
   <div class="center">
     <img class='logo center' src="<?php echo base_url("assets/images/logo/logo.png"); ?>" ><br>
   </div>
   <h5 class="col s12 center"><i class="fas fa-angle-double-right"></i> <span id = "company_name"></span> <i class="fas fa-angle-double-left"></i></h5>
   <span> <?php echo $this->lang->line('tin') ?> :</span> <span id = "tin"></span><br>
   <span> <?php echo $this->lang->line('company_kind') ?> :</span> <span id = "company_kind"></span><br>
   <span> <?php echo $this->lang->line('company_kind_detail') ?> :</span><span id = "company_kind_detail"></span><br>
   <span> <?php echo $this->lang->line('company_tel') ?> :</span><span id = "company_tel"></span><br>
   <span> <?php echo $this->lang->line('web') ?> :</span><span id = "web"></span><br>
   <span> <?php echo $this->lang->line('company_address') ?> :</span> <span id = "company_address"></span><br>
   <span> <?php echo $this->lang->line('travel') ?> :</span><br><span id = "travel"></span><br>
   <h6 class="col s12 center"><i class="fas fa-angle-double-right"></i> <?php echo $this->lang->line('map') ?> <i class="fas fa-angle-double-left"></i></h6>
   <div class="center">
     <img class='map center' id="map" ><br>
     <!-- <img class='map center' src="<?php echo base_url("assets/images/logo/logo.png"); ?>" ><br> -->
   </div>
   <div class="divider"></div>
  </div>
 </div>
</div>

<div id="users" class="row">
  <div class="col s12">
    <div class="row " id="invite_group" >
      <button class="btn waves-effect waves-light right" id="invite" style="margin-top:10px" onclick="invite_Dialog()" ><?php echo $this->lang->line('invite') ?></button>
    </div>
    <div class="row">
      <div class="col s4">
        <div class="card" onclick="">
            <div class="card-ticket  center-align">
                <div class="row">
                   <p><span><?php echo $this->lang->line('status_all') ?></span> </p>
                   <h3 class="count" id="count-all" style="margin: 0;"></h3>
                </div>
            </div>
        </div>
      </div>

      <div class="col s4">
        <div class="card" onclick="">
            <div class="card-ticket  center-align">
                <div class="row">
                   <p><span><?php echo $this->lang->line('status_online') ?></span> </p>
                   <h3 class="count" id="count-online" style="margin: 0;"></h3>
                </div>
            </div>
        </div>
      </div>

      <div class="col s4">
        <div class="card" onclick="">
            <div class="card-ticket  center-align">
                <div class="row">
                   <p><span><?php echo $this->lang->line('status_offline') ?></span> </p>
                   <h3 class="count" id="count-offline" style="margin: 0;"></h3>
                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="">
      <div class="row file-toolbar">
        <div class="col s12 m4 l3 right-align row">
          <table class='select-row'>
            <tr>
              <td>
                <label class='label-for-sm' for="member_row_show"><?php echo $this->lang->line('table_label_show'); ?> </label>
              </td>
              <td>
                <select class='browser-default' id='row_show' onchange="UserByCompany();">
                  <option value="10" selected>10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option value="50">50</option>
                </select>
              </td>
              <td class='right-align '>
                <label class='label-for-sm' for="member_row_show"><?php echo $this->lang->line('table_label_row'); ?> </label>
              </td>
            </tr>
          </table>
        </div>

        <div class="col m5 l4 row">
            <input class="search-text" placeholder="<?php echo $this->lang->line('table_search'); ?> "  type="text" id="searchTable" onkeyup="searchInTables($(this));">
        </div>

        <div class="col s5 m3 l5 right-align">
          <div class="table-pagination btn-group">
            <button type="button" name="button" class='btn btn-primary prev-page' onclick="prevpage(this);">
              <span class='fa fa-angle-left'></span>
            </button>
            <button type="button" name="button" class='btn btn-default page-num ' >
            <span id='nowpage'>1 </span><span> / </span><span class='totalpage'></span>
            </button>
            <button type="button" name="button" class='btn btn-primary next-page' onclick="nextpage(this);">
              <span class='fa fa-angle-right'></span>
            </button>
          </div>
        </div>

      </div>
      <div class="">
        <table id="tbService">
              <thead>
                <tr>
                  <th class="hide-on-small-only" width="10%"></th>
                  <th width="30%"><?php echo $this->lang->line('fnamelname') ?></th>
                  <th width="25%"><?php echo $this->lang->line('email') ?></th>
                  <th width="15%"></th>
                </tr>
              </thead>
              <tbody class="file-container">
                <!-- file-container -->
              </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
