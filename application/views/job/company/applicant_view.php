<div class="">
  <h5 class="white-text"> ข้อมูลของ
        <span class="white-text" id="h_fname"></span>
        <span class="white-text" id="h_lname"></span>
  </h5>
</div>
<ul id="tabs-setting" class="tabs">
  <li class="tab col s1"><a class="active" href="#profile"><?php echo $this->lang->line('profile') ?></a></li>
  <li class="tab col s1"><a  href="#activity"><?php echo $this->lang->line('activity') ?></a></li>
</ul>

<input type="hidden" id="uid" name="" value='<?php echo base64_decode(base64_decode(base64_decode($_GET['U']))) ?>'>

<div class="row" id="profile">
  <div class="container" style="padding: 30px 30px">
    <h5>ข้อมูลทั่วไป</h5>
    <div class="pfix disable-display">
      <div class="row reset-margin">
        <span class="col s5 text-right"><?php echo $this->lang->line('pfix') ?> </span><span class="col s7" id="pfix"></span>
      </div>
    </div>
    <div class="fullname">
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('fname') ?> : </span><span class="col s7" id="fname"></span>
      </div>
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('lname') ?> : </span><span class="col s7" id="lname"></span>
      </div>
    </div>
    <div class="birth_date disable-display">
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('birth_date') ?> : </span><span class="col s7" id="birth_date"></span>
      </div>
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('age') ?> : </span><span class="col s7" id="age"></span>
      </div>
    </div>
    <div class="sex disable-display">
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('gender') ?> : </span><span class="col s7" id="sex"></span>
      </div>
    </div>
    <div class="married_status disable-display">
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('married_status') ?> : </span><span class="col s7" id="married_status"></span>
      </div>
    </div>
    <div class="job_description_1 disable-display">
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('job_description_1') ?> : </span><span class="col s7" id="job_description_1"></span>
      </div>
    </div>
    <div class="contact">
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('mobile') ?>: </span><span class="col s7" id="mobile"></span>
      </div>
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('email') ?> : </span><span class="col s7" id="email"></span>
      </div>
    </div>
    <div class="degree disable-display">
      <div class="divider"></div>
      <h5>ข้อมูลการศึกษา</h5>
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('degree') ?> : </span><span class="col s7" id="degree"></span>
      </div>
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('institute') ?> : </span><span class="col s7" id="institute"></span>
      </div>
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('major') ?> : </span><span class="col s7" id="major"></span>
      </div>
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('finish_year') ?> : </span><span class="col s7" id="finish_year"></span>
      </div>
      <div class="row reset-margin">
        <span class="col s5"><?php echo $this->lang->line('grade') ?> : </span><span class="col s7" id="grade"></span>
      </div>
    </div>
  </div>
</div>

<div  class="row" id="activity">
  <div class="card-Dashboard">
    <div class="row">
        <!-- card1 -->
        <div class="col s6">
          <div class="card" onclick="">
              <div class="card-ticket  center-align">
                  <div class="row">
                     <p><span><?php echo $this->lang->line('login') ?></span> </p>
                     <h3 class="count" id="count-login" style="margin: 0;"></h3>
                     <small> ล็อกอินล่าสุด </small><small id="last_login"></small>
                  </div>
              </div>
          </div>
        </div>

        <!-- card2 -->
        <!-- <div class="col s5">
          <div class="card" onclick="">
              <div class="card-ticket  center-align">
                  <div class="row">
                     <p><span><?php echo $this->lang->line('search') ?></span> </p>
                     <h3 class="count" id="count-search" style="margin: 0;"></h3>
                  </div>
              </div>
          </div>
        </div> -->

        <!-- card3 -->
        <div class="col s6">
          <div class="card" onclick="">
              <div class="card-ticket  center-align">
                  <div class="row">
                     <p><span><?php echo $this->lang->line('apply_register') ?></span> </p>
                     <h3 class="count" id="count-apply" style="margin: 0;"></h3>
                  </div>
              </div>
          </div>
        </div>

    </div>
  </div>
  <div class="table_activity">
    <div class="row file-toolbar">
      <div class="col s12 m4 l3 right-align row">
        <table class='select-row'>
          <tr>
            <td>
              <span class='span-for-sm' for="member_row_show"><?php echo $this->lang->line('table_span_show'); ?> </span>
            </td>
            <td>
              <select class='browser-default' id='row_show'>
                <option value="5" selected>5</option>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="50">50</option>
              </select>
            </td>
            <td class='right-align '>
              <span class='span-for-sm' for="member_row_show"><?php echo $this->lang->line('table_span_row'); ?> </span>
            </td>
          </tr>
        </table>
      </div>

      <div class="col m5 l4 row">
          <!-- <input class="search-text" placeholder="ค้นหากิจกรรม"  type="text" id="searchTable" onkeyup="searchInTables($(this));"> -->
      </div>

      <div class="col s5 m3 l5 right-align">
        <div class="table-pagination btn-group">
          <button type="button" name="button" class='btn btn-primary prev-page' onclick="prevpage(this);">
            <span class='fa fa-angle-left'></span>
          </button>
          <button type="button" name="button" class='btn btn-default page-num ' >
          <span id='nowpage'>1 </span><span> / </span><span class='totalpage'></span>
          </button>
          <button type="button" name="button" class='btn btn-primary next-page' onclick="nextpage(this);">
            <span class='fa fa-angle-right'></span>
          </button>
        </div>
      </div>

    </div>
    <div class="">
      <table id="tbService">
            <thead>
              <tr>
                <th class="hide-on-small-only" width="10%"></th>
                <th width="30%">กิจกรรม</th>
                <th width="25%">วัน/เดือน/ปี</th>
                <!-- <th width="30%"><?php echo $this->lang->line('fnamelname') ?></th>
                <th width="25%"><?php echo $this->lang->line('email') ?></th> -->
                <th width="15%"></th>
              </tr>
            </thead>
            <tbody class="file-container">
              <!-- file-container -->
            </tbody>
      </table>
    </div>
  </div>
</div>
