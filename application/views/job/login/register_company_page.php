<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      <span>ผู้ประกอบการลงทะเบียนใช้งาน</span>

    <div>
    <div class="row container">
      <div class="form-group ">
        <h6>ข้อมูลส่วนตัว</h6>
        <div class="col s6" id="fname_group">
            <input id="fname" name="fname" type="text" placeholder="<?php echo $this->lang->line('fname') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelfname" for="fname" ></small>
        </div>
        <div class="col s6" id="lname_group">
            <input id="lname" name="lname" type="text" placeholder="<?php echo $this->lang->line('lname') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labellname" for="lname" ></small>
        </div>
        <div class="col s12" id="tel_group">
            <input id="tel" name="tel" type="tel" placeholder="<?php echo $this->lang->line('tel') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labeltel" for="tel" ></small>
        </div>
        <div class="col s12" id="email_group">
            <input id="email" name="email" type="text" placeholder="<?php echo $this->lang->line('email') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelemail" for="email" ></small>
        </div>
      </div>
      <div class="form-group ">
        <h6>ข้อมูลบริษัท</h6>
        <div class="col s12" id="cname_group">
            <input id="cname" name="cname" type="text" placeholder="<?php echo $this->lang->line('name_company') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelcname" for="cname" ></small>
        </div>
        <div class="col s12" id="tin_group">
            <input id="tin" name="tin" type="text" placeholder="<?php echo $this->lang->line('tin') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labeltin" for="name" ></small>
        </div>
      </div>
    </div>
    <div class="modal-footer center">
      <a class="btn" onclick="Register()"><?php echo $this->lang->line('signup') ?></a>
      <a class="btn" onclick="pageHome()"><?php echo $this->lang->line('nav_homepage') ?></a>
    </div>
  </div>
</div>
</div>
