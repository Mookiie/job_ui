<!-- <section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      <input type="hidden" id="id" value="<?php echo $_GET['id'] ?>">
      <input type="hidden" id="code" value="<?php echo $_GET['code'] ?>">
        <h4 class="center"><?php echo $this->lang->line('confirm') ?></h4>
        <h6 class="center"><?php echo $this->lang->line('setpassword') ?></h6>
        <div class="form-group">
          <div class="col s12" id="password_group">
            <input id="password" name="password" type="password" placeholder="<?php echo $this->lang->line('password') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelpassword" for="password" ></small>
        </div>
        <div class="form-group text-center ">
          <div class="col s12">
            <button class="btn waves-effect waves-light btn-login" id="change_confirm"><?php echo $this->lang->line('change_password') ?></button>
          </div>
        </div>
    </div>
  </div>
</section> -->

<section id="wrapper" class="login-register">
  <img class="login-register-logo" src="<?php echo  base_url("assets/images/logo/logo.png")?>" alt="logo">
  <h5 class="login-register-text">JOBS</h5>
  <h4 class="center white-text"><?php echo $this->lang->line('confirm') ?></h4>
  <h6 class="center white-text"><?php echo $this->lang->line('setpassword') ?></h6>
  <div class="login-card">
    <div id="white-card" class="white-card" style="height: 200px;padding: 40px;">
      <input type="hidden" id="id" value="<?php echo $_GET['id'] ?>">
      <input type="hidden" id="code" value="<?php echo $_GET['code'] ?>">

        <!-- <div class="form-group"> -->
        <div class="form-group">
          <div class="col s12" id="password_group">
            <input id="password" name="password" type="password" placeholder="<?php echo $this->lang->line('password') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelpassword" for="password" ></small>
        </div>
        <div class="form-group text-center ">
          <div class="col s12 center">
            <button class="btn waves-effect waves-light btn-login" id="change_confirm"><?php echo $this->lang->line('change_password') ?></button>
          </div>
        </div>
    <!-- </div> -->
  </div>
</section>
