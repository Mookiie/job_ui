<section id="wrapper" class="login-register">
  <img class="login-register-logo" src="<?php echo  base_url("assets/images/logo/logo.png")?>" alt="logo">
  <h5 class="login-register-text">JOBS</h5>
  <div class="login-card">
    <div id="white-card" class="white-card display-disable">
      <input type="hidden" id="uid" value="<?php echo $info['userId'] ?>">
      <input type="hidden" id="name" value="<?php echo $info['displayName'] ?>">
      <input type="hidden" id="img" value="<?php echo $info['pictureUrl'] ?>">
        <!-- <div class="form-group"> -->
          <div class="input-field reset-margin row" id="fname_group">
              <span class="col s2 icon-input" ><?php echo $this->lang->line('fname') ?>*</span>
              <input class="col s10 reset-margin" id="fname" name="fname" type="text"  onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
              <small id="labelfname" for="fname" ></small>
          </div>
          <div class="input-field reset-margin row" id="lname_group">
              <span class="col s2 icon-input"><?php echo $this->lang->line('lname') ?>*</span>
              <input class="col s10 reset-margin" id="lname" name="lname" type="text"  onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
              <small id="labellname" for="lname" ></small>
          </div>
            <!-- <label class="col s3"><?php echo $this->lang->line('mobile') ?></label> -->
            <!-- <span class="col s3"> <i class="material-icons">phone</i></span> -->
          <div class="input-field reset-margin row" id="tel_group">
              <span><i class="col s2 material-icons icon-input">phone</i>
              <input class="col s10 reset-margin" id="tel" name="tel" type="tel" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
              <small id="labeltel" for="tel" ></small>
          </div>
          <div class="input-field reset-margin row" id="email_group">
              <i class="col s2 icon-input material-icons ">email</i>
              <!-- <label for="email"><?php echo $this->lang->line('email') ?></label> -->
              <input class="col s10 reset-margin"  id="email" name="email" type="text" placeholder="กรอกอีเมล(ถ้ามี)" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
              <small id="labelemail" for="email" ></small>
          </div>
          <div class="input-field reset-margin row" id="citizen_group">
            <i class="col s2 icon-input far fa-id-card"></i>
              <!-- <label for="citizen"><?php echo $this->lang->line('citizen') ?></label> -->
              <input class="col s10 reset-margin"  id="citizen" placeholder="กรอกเลขบัตรประจำตัวประชาชน" name="citizen" type="text" onfocus="rmErr(id);"  onkeyup="autoTab(this);" required>
              <small id="labelcitizen" for="citizen" ></small>
          </div>
          <div class="form-group text-center ">
            <div class="col s12 center">
              <button class="btn waves-effect waves-light btn-login" id="register" onclick="register()">ลงทะเบียน</button>
            </div>
          </div>
    <!-- </div> -->
  </div>
</section>
