<div id="loginDialog"  class="modal">
  <div class="modal-dialog">
    <div class="modal-header" >
      <span>&nbsp;&nbsp;&nbsp;&nbsp;เข้าสู่ระบบสำหรับผู้ประกอบการ</span>
    </div>
    <div>
    <div class="row container">
      <form id="loginform">
        <div class="form-group ">
          <div class="col s12" id="username_group">
            <input id="email" name="email" type="text" placeholder="<?php echo $this->lang->line('email') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
            <small id="labelemail" for="email" ></small>
            <!-- <input class="form-control" type="text" name="username" id='username' onkeypress="return onremove_validate(this);" required="" placeholder="<?php echo $this->lang->line('username') ?>">
            <div class='feedback' id="username_feedback"></div> -->
          </div>
          <div class="col s12" id="password_group">
              <!-- <input class="form-control" type="password" name="password" id='password' onkeypress="return onremove_validate(this);" required="" placeholder="<?php echo $this->lang->line('password') ?>">
              <div class='feedback' id="password_feedback"></div> -->
              <input id="password" name="password" type="password" placeholder="<?php echo $this->lang->line('password') ?>" onfocus="rmErr(id);" onkeypress="rmErr(id);" required>
              <small id="labelpassword" for="password" ></small>
          </div>
          <div class="col s12">
            <!-- <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"><?php echo $this->lang->line('remember') ?></label>
            </div> -->
            <!-- <a id="to-recover" class="pull-right pointer"><i class="fa fa-lock m-r-5"></i> <?php echo $this->lang->line('forget') ?></a> -->
          </div>
        </div>
      </form>
      <div class="form-group m-b-0">
          <div class="col s12 text-center">
            <p><?php echo $this->lang->line('noaccount') ?><a class="text-primary m-l-5" onclick="register_company()"><b class="pink-text pointer"> <?php echo $this->lang->line('signup') ?></b></a></p>
          </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect btn-flat" onclick="login_company()"><?php echo $this->lang->line('login') ?></a>
      <a class="modal-close waves-effect btn-flat closemodal"><?php echo $this->lang->line('close') ?></a>
    </div>
  </div>
</div>
