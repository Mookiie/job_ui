<div class="main-container">
  <div class="row">
    <div class="advert">
      <div class="carousel-hidden">
          <i class="fas fa-times"></i>
      </div>
      <div class="carousel carousel-slider">
      </div>
      <div class="row reset-margin">
        <div class="col s6 l6">
          <div class="card-advert">
            <div class="card-advert-content" onclick="getAnnounceTable()">
              <h3 class="center white-text reset-margin ">ประกาศทั้งหมด</h3>
              <h2 class="center white-text advert-count reset-margin advert_count_all"></h2>
            </div>
          </div>
        </div>
        <div class="col s6 l6">
          <div class="card-advert">
            <div class="card-advert-content" onclick="myfavorite()">
              <h3 class="center white-text reset-margin">ประกาศที่สนใจ</h3>
              <h2 class="center white-text advert-count reset-margin advert_count_favorite"></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="select-table row">
        <div class="input-field col s12">
          <select id="select-table" onchange="getAnnounceTable()">
            <option value="9" selected><span class="fas fa-angle-down white-text"></span> ประกาศทั้งหมด</option>
            <option value="0"><span class="fas fa-angle-down white-text"></span> ประกาศที่มีคนดูสูงสุด</option>
            <option value="1"><span class="fas fa-angle-down white-text"></span> ประกาศที่มีคนสนใจสูงสุด</option>
            <option value="2"><span class="fas fa-angle-down white-text"></span> ประกาศที่มีเงินเดือนสูงสุด</option>
            <option value="3"><span class="fas fa-angle-down white-text"></span> ประกาศที่ท่านสนใจ</option>
            <option value="4"><span class="fas fa-angle-down white-text"></span> ประกาศของเดือนนี้</option>
          </select>
        </div>
      </div>
      <div class="announce-table">
        <table>
          <thead>
            <tr>
                <th class="center" width="5%"></th>
                <th class="center" width="5%"></th>
                <th class="center" width="15%">รายการ</th>
                <th class="center" width="15%">ตำแหน่ง</th>
                <th class="center" width="15%">ผู้รับสมัคร</th>
                <th class="center" width="15%">เงินเดือน</th>
                <th class="center" width="15%">ที่ตั้ง</th>
                <th class="center" width="15%">วันที่ลงประกาศ</th>
            </tr>
          </thead>

          <tbody id="table_content">
            <!-- <tr>
              <td class="center">
                <img src="assets/images/icon/star-regular.svg" alt="">
                <img class="display-disable" src="assets/images/icon/star-solid.svg" alt="">
              </td>
              <td class="center">1</td>
              <td>รายการ</td>
              <td>ตำแหน่ง</td>
              <td>ผู้รับสมัคร</td>
              <td>เงินเดือน</td>
              <td>ที่ตั้ง</td>
              <td>วันที่ลงประกาศ</td>
            </tr> -->
          </tbody>
        </table>
      </div>
      <div class="announce-table-mobile">
        <!-- <table>
          <tbody id="table_content_mobile">
          </tbody>
        </table> -->
        <div id="table_content_mobile" >
          <!-- <div class="table-mobile-header">
            <div class="row reset-magin">
              <img src="assets/images/icon/star-regular.svg" alt="">
              <img class="display-disable" src="assets/images/icon/star-solid.svg" alt="">
              <span>header</span>
            </div>
          </div>
          <div class="table-mobile-content">
            <div class="row">
                <span class="data-content"><i class="fas fa-building"></i>สยามราชธานี</span>
                <span class="data-content"><i class="fas fa-map-marker-alt"></i> สยามราชธานี</span>
                <span class="data-content pull-right">สยามราชธานี</span>
                <span class="data-content"><i class="fas fa-calendar-alt"></i>สยามราชธานี</span>
            </div>
          </div> -->
        </div>
      </div>
    </div>
    <div class="announce">
      <div class="announce-header">
        <div class="row">
          <img src="assets/images/icon/announce_header.png" alt="announce header">
          <span class="white-text"><?php echo $this->lang->line('announce_header');?></span>
        </div>
      </div>
      <div class="row" id="data_not_found" style="display:none">
        <h4>ไม่พบข้อมูล</h4>
      </div>
      <div class="row content-announce">
      </div>
    </div>
    </div>
  </div>
</div>
