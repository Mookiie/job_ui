<div id="citizenDialog" class="modal">
  <div class="modal-dialog sm">
    <div class="modal-header">
      <span class="white-text">แก้ไขข้อมูล</span><br />
    </div>
    <div class="modal-content ">
      <h6 id="head_citizen">หมายเลขบัตรประจำตัวประชาชน</h6>
      <div class="input-field row" id="citizen_group">
          <i class="col s2 icon-input far fa-id-card j-text"></i>
          <input class="col s10 reset-margin"  id="citizen" name="citizen" type="text" placeholder="x-xxxx-xxxxx-xx-x" onfocus="rmErr(id);">
          <small id="labelcitizen" for="citizen" ></small>
      </div>
      <h6 id="birth_date_head">วัน/เดือน/ปีเกิด</h6>
      <div class="input-field row" id="birth_date_group">
          <i class="col s2 icon-input fas fa-birthday-cake j-text"></i>
          <input class="col s10 reset-margin datepicker"  id="birth_date" name="birth_date" type="text" placeholder="" data-toggle="datepicker" onfocus="rmErr(id);">
          <small id="labelbirth_date" for="birth_date" ></small>
      </div>
      <h6 id="tel_head">ข้อมูลติดต่อ</h6>
      <div class="input-field row" id="tel_group">
          <i class="col s2 icon-input fas fa-phone j-text"></i>
          <input class="col s10 reset-margin"  id="tel" name="tel" type="text" placeholder="" onfocus="rmErr(id);">
          <small id="labeltel" for="tel" ></small>
      </div>
      <div class="input-field row" id="email_group">
          <i class="col s2 icon-input fas fa-envelope j-text"></i>
          <input class="col s10 reset-margin"  id="email" name="email" type="text" placeholder="" onfocus="rmErr(id);">
          <small id="labelemail" for="email" ></small>
      </div>
      <h6 id="address_head">ที่อยู่ปัจจุบัน</h6>
      <div class="row col s12" id="user_province_group">
          <select class="browser-default province" id="user_province" name="user_province">
            <option value="" disabled selected><?php echo $this->lang->line('province')?></option>
          </select>
          <small id="labeluser_province" for="user_province" ></small>
      </div>
      <div class="row col s12" id="user_aumphur_group">
          <select class="browser-default aumphur" id="user_aumphur" name="user_aumphur">
            <option value="" disabled selected><?php echo $this->lang->line('aumphur')?></option>
          </select>
          <small id="labeluser_aumphur" for="user_aumphur" ></small>
      </div>
    </div>
    <div class="modal-footer">
      <a class="btn-flat waves-effect" id="EditApplicant" onclick="EditApplicant()">บันทึก</a>
      <a class="btn-flat waves-effect closemodal">ปิด</a>

    </div>
    </div>

  </div>
</div>
