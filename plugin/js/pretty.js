/*
 * JavaScript Pretty Date
 * Copyright (c) 2011 John Resig (ejohn.org)
 * Licensed under the MIT and GPL licenses.
 */

// Takes an ISO time and returns a string representing how
// long ago the date represents.

function getReadableFileSizeString(fileSizeInBytes) {
		if (fileSizeInBytes != 0 ) {
			var i = -1;
			var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
			do {
					fileSizeInBytes = fileSizeInBytes / 1024;
					i++;
			} while (fileSizeInBytes > 1024);

			return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
		}else{
			return "-";
		}

};

function manyDay(time){
	var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
			diff = (((new Date()).getTime() - date.getTime()) / 1000),
			day_diff = Math.floor(diff / 86400);
			var many = -1;
			if (day_diff <= 0) {
					many = Math.abs(day_diff)
			}
			return many;
}

function prettyDate(time){
	var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
			diff = (((new Date()).getTime() - date.getTime()) / 1000),
			day_diff = Math.floor(diff / 86400);
	if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
		return;


	return day_diff == 0 && (
			diff < 60 && " "+lang.date.justnow ||
			diff < 120 && " 1 "+lang.date.oneminutesago ||
			diff < 3600 && Math.floor( diff / 60 ) + " "+lang.date.minutesago ||
			diff < 7200 && " "+lang.date.onehourago ||
			diff < 86400 && Math.floor( diff / 3600 ) + " "+lang.date.hourago) ||
			day_diff == 1 && " "+lang.date.yesterday ||
			day_diff < 7 && day_diff +" "+ lang.date.dayago ||
			day_diff < 31 && Math.ceil( day_diff / 7 ) + " "+lang.date.weekago;
}

// If jQuery is included in the page, adds a jQuery plugin to handle it as well
if ( typeof jQuery != "undefined" )
	jQuery.fn.prettyDate = function(){
		return this.each(function(){
			var date = prettyDate(this.title);
			if ( date )
				jQuery(this).text( date );
		});
	};

	function convertFormDbDate(date){
		var dateSplit = date.split("-");
		var yy = dateSplit[0];
		var mm = dateSplit[1];
		var dd = dateSplit[2].substr(0,2);
		return dd+"/"+mm+"/"+yy;
	}

function getNowDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd = '0'+dd
	}

	if(mm<10) {
	    mm = '0'+mm
	}

	today = dd + '/' + mm + '/' + yyyy;
	return today;
}
