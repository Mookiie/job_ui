$(document).ready(function() {
  chk_register()
})

function chk_register() {
  console.log("chk_register");
  var connectURL = getApi('auth/Chk_applicant');
  var uid = $("#uid").val();
  var fname = $("#fname").val();
  var lname = $("#lname").val();
  var tel = $("#tel").val();
  var email = $("#email").val();
  var img = $("#img").val();
  $.ajax({
    url: connectURL,
    type: 'POST',
    data:{"uid":uid,"img":img}
  })
  .done(function(data) {
    console.log(data);
    closeloader();
    var obj = JSON.parse(data);
    if (obj.status == 200) {
      $("#white-card").removeClass("display-disable")
    }
    else if (obj.status == 202) {
      $("#white-card").addClass("display-disable")
      var connectURL = getApi("login/user");
      var uid = $("#uid").val();
      var img = $("#img").val();
      var formData = {"uid":uid,
                      "img":img,
                      "citizen":obj.data[0]['citizen_id']}
      $.ajax({
        url: connectURL,
        type: 'POST',
        data: formData
      }).done(function(data) {
        var obj = JSON.parse(data);
        console.log(obj);
         if (obj.status==200 || obj.status==201) {
           $(".loader").show();
           var autoken = obj["token"];
           var connect = getUrl('');
           localStorage.setItem('data',"applicant");
           localStorage.setItem("token",autoken);
           localStorage.setItem('channel',obj.channel);
           log_user('0','','Home','','','','','','','','')
           setTimeout(function(){
               window.location = connect
           }, 2000);
         }
         // else if (obj.status==201) {
         //   $(".white-box").css({"display":""})
         //   $("#citizen").val(obj.citizen);
         //   // Swal({
         //   //        type: 'error',
         //   //        title: 'Oops...',
         //   //        text: 'กรุณาส่งประวัติของคุณที่ www.....',
         //   //        confirmButtonText: 'Yes',
         //   //      }).then((result) => {
         //   //        if (result.value) {
         //   //          window.location.href = 'http://www.google.com';
         //   //        }
         //   //      })
         // }
       })
    }
  })
}

function autoTab(obj){
    /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย
    หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น  รูปแบบเลขที่บัตรประชาชน
    4-2215-54125-6-12 ก็สามารถกำหนดเป็น  _-____-_____-_-__
    รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____
    หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__
    ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบเลขบัตรประชาชน
    */
            var pattern=new String("_-____-_____-__-_"); // กำหนดรูปแบบในนี้
            var pattern_ex=new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้
            var returnText=new String("");
            var obj_l=obj.value.length;
            var obj_l2=obj_l-1;
            for(i=0;i<pattern.length;i++){
                if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){
                    returnText+=obj.value+pattern_ex;
                    obj.value=returnText;
                }
            }
            if(obj_l>=pattern.length){
                obj.value=obj.value.substr(0,pattern.length);
            }
}

function register() {
  console.log("register");
  var connectURL = getApi("auth/register");
  var uid = $("#uid").val();
  var img = $("#img").val();
  var fname = $("#fname").val();
  var lname = $("#lname").val();
  var tel = $("#tel").val();
  var email = $("#email").val();
  var result = true;
  if ($("#citizen").val() != '') {
  var str = $("#citizen").val();
  var regex = /-/gi;
  var res = str.replace(regex, "");
  result = checkID(res)
  }
      console.log(result);
  var formData = {"uid":uid,
                  "img":img,
                  "fname":fname,
                  "lname":lname,
                  "tel":tel,
                  "email":email,
                  "citizen":$("#citizen").val()
                 }
  if (fname == "") {
    addErr("fname",lang.error.input_blank)
  }
  if (lname == "") {
    addErr("lname",lang.error.input_blank)
  }
  if (tel == "") {
    addErr("tel",lang.error.input_blank)
  }
  if (!phonenumber(tel)) {
    addErr("tel",lang.error.data_worng)
  }
  if (!result) {
    addErr("citizen",lang.error.data_worng)
  }

  if (fname != "" && lname != "" && tel != "" && phonenumber(tel) && result) {
    console.log(formData);
    $.ajax({
      url: connectURL,
      type: 'POST',
      data: formData
    }).done(function(data) {
    //   closeloader()
      var obj = JSON.parse(data);
      console.log(obj);
       if (obj.status==200 || obj.status==201) {
         $(".loader").show();
        var autoken = obj["token"];
        var connect_reload = getUrl('');
        var connect = "https://line.me/R/ti/p/%40siamrajdriver";
         localStorage.setItem("token",autoken);
         localStorage.setItem('channel',obj.channel);
         localStorage.setItem('data',"applicant");
         log_user('0','','Home','','','','','','','','')
         setTimeout(function(){
             window.open(connect)
             window.location = connect_reload
         }, 2000);
       }
       // else if (obj.status==201) {
       //   Swal({
       //          type: 'error',
       //          title: 'Oops...',
       //          text: 'กรุณาส่งประวัติของคุณที่ iRecruit',
       //          confirmButtonText: 'Yes',
       //        }).then((result) => {
       //          if (result.value) {
       //            window.location.href = 'http://203.151.43.169:888/RegisterOnline?link_name=MDE0MDAwMDAwMDAwMA%3D%3D&link_infor=Mw%3D%3D';
       //          }
       //        })
       // }
     })
  }


}
