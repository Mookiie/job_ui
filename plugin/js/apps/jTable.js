// guide
// function genRowTicket(index,data,display) {
//   var table = "";
//   table += "<tr class='tr-ticket' id='tr_file_"+index+"' style='display:"+display+";' data-id='"+data.ticket_id+"' data-row='"+index+"'>";
//   table += "<td>"+ticketstatus(data.ticket_status)+"</td>";
//   table += "<td class='sort'>";
//   table += "["+data.ticket_id+"] "+data.ticket_title+"<br><small> ขอโดย : "+data.fname+" "+data.lname+"</small>";
//   table += "</td>";
//   table += "<td>"+formatDate(data.ticket_date)+"</td>";
//   table += "<td>"+ticketProgress(data.ticket_status)+"</td>";
//   table += "</tr>";
//   return table;
// }

function genRowUser(index,data,display) {
    var table = "";
    var status = getStatusGroup(data.status);
    table += "<tr class='tr-ticket' id='tr_file_"+index+"' style='display:"+display+";' data-id='"+data.uid+"' data-row='"+index+"'>";
    table += "<td class='hide-on-small-only'>"+"<img class='circle img-profiles' src='"+data.image+"' width='40' > <span class='profile-status fa fa-circle "+status.profile+"'></span>"+"</td>";
    table += "<td class='sort'>";
    table += data.fname+" "+data.lname;
    table += "</td>";
    table += "<td>"+data.email+"</td>";
    table += "<td class='center'>";
    table += " &nbsp;<i class='fas fa-eye' onclick=\"view(\'"+data.uid+"\')\"></i>";
    if (data.status == 0) {
      table += " &nbsp;<i class='fas fa-recycle' onclick=\"restore(\'"+data.uid+"\')\"></i>";
    }else if (data.status == 1){
      table += " &nbsp;<i class='fas fa-trash' onclick=\"trash(\'"+data.uid+"\')\"></i>";
    }else if (data.status == 9){
      table += " &nbsp;<i class='fas fa-envelope' onclick=\"resend(\'"+data.uid+"\')\"></i>";
    }
    // table += " &nbsp;<i class='fas fa-caret-square-down'></i>";
    table += "</td>";
    table += "</tr>";
    return table;

}

function genSelect(data) {
  var row = "";
  row += "<option value="+data.id+">"+data.name+"</option>"
  return row;
}


function formatDatetime(date) {
  var datetime = new Date(date);
  var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
  var hours = datetime.getHours();
      hours = hours < 10 ? '0'+hours : hours;
  var minutes = datetime.getMinutes();
      minutes = minutes < 10 ? '0'+minutes : minutes;
  var day = datetime.getDate();
  var monthIndex = datetime.getMonth();
  var year = datetime.getFullYear();
  var times = hours+':'+minutes
  return day + ' ' + monthNames[monthIndex] + ' ' + year+' '+times;
}
function formatDate(date) {
  var datetime = new Date(date);
  var monthNames = ["ม.ค.", "ก.พ.", "มี.ค.","เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.","ส.ค.", "ก.ย.", "ต.ค.","พ.ย.", "ธ.ค."];
  // var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
  var hours = datetime.getHours();
      hours = hours < 10 ? '0'+hours : hours;
  var minutes = datetime.getMinutes();
      minutes = minutes < 10 ? '0'+minutes : minutes;
  var day = datetime.getDate();
  var monthIndex = datetime.getMonth();
  var year = datetime.getFullYear();
  var times = hours+':'+minutes
  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

// function genCardAnnounce(data) {
//   var card = "";
//   card += "<div class='col s12'>"
//   // card +=   "<div class='card' onclick=\"count_view(\'"+data.announce_id+"\')\" style='padding:10px'>"
//   card +=   "<div class='card'>"
//   card +=       "<div class='card-announce-id'>"
//   card +=           "<div class='row'>"
//   card +=           "<div class='col s8'>"
//   card +=             "<div class='row' style='margin-bottom: 10px;'>"+data.company.company.company_name+"</div>"
//   card +=             data.announce_title
//   card +=             "<br><i class='fas fa-user-plus'></i> "+lang.announce.job_description_1+" : "+data.job_description_1[0]['job_description_name']
//     if (data.job_description_2.length > 0 && data.job_description_2[0]['job_description_code_2'] != '999') {
//       card +=           " ("+data.job_description_2[0]['job_description_name']+")"
//     }
//   if (data.experience == 99) {
//     card +=             "<br><i class='fas fa-briefcase'></i> "+lang.announce.experience+" : -"
//   }else {
//     card +=             "<br><i class='fas fa-briefcase'></i> "+lang.announce.experience+" : "+data.experience+" ปี"
//   }
//   card +=             "<br> <i class='fas fa-graduation-cap'></i> "+lang.announce.education+" : "+data.education[0]['name']+" "+lang.announce.more
//   if (data.income_max == data.income_min) {
//     card +=             "<br> <i class='fas fa-donate'></i> "+lang.announce.income+""
//   }else {
//     card +=             "<br> <i class='fas fa-donate'></i> "+lang.announce.income_label+" : "+data.income_min+"-"+data.income_max+" บาท"
//   }
//   card +=           "</div>"
//   card +=           "<div class='col s4'>"
//   card +=           "<div class='row col s12' style='margin-bottom: 10px;'><img class='right' height='46' id='map' src='"+data.company.company.image+"'></div>"
//   card +=             "<span class='icon fas fa-cart-plus tooltipped right' data-position='bottom' data-tooltip='Register' onclick=\"apply_job(\'"+data.announce_id+"\')\")'></span>"
//   card +=             "<span class='icon fas fa-star tooltipped right' data-position='bottom' data-tooltip='Favorite' onclick=\"favorite_job(\'"+data.announce_id+"\')\"></span>"
//   card +=             "<span class='icon fas fa-eye tooltipped right' data-position='bottom' data-tooltip='View' onclick=\"count_view(\'"+data.announce_id+"\')\"></span>"
//   card +=             "<div class='col s12' style='padding: 0;'><span class='right'>"+formatDate(data.create_at)+"</span></div>"
//   if (data.percent != '') {
//     card +=             "<div class='col s12' style='padding: 0;'><span class='right'>"+lang.announce.percent+" "+data.percent+"%</span></div>"
//   }
//   card +=           "</div>"
//   card +=       "</div>"
//   card +=   "</div>"
//   card += "</div>"
// return card;
//
// }

function genCardAnnounce(data){
  var card = "";
  // onclick=\"count_view(\'"+data.announce_id+"\')\"
  card += ""
  card +=  "<div class='col s12' onclick=\"count_view(\'"+data.announce_id+"\',\'"+data.staff_id+"\')\">"
  card +=   "<div class='card-announce-id'>"
  card +=     "<div class='card-header text-wrap-head'>"
  card +=        data.company.company.company_name
  card +=     "</div>"
  card +=     "<div class='title-content'>"
  card +=        "<span >"+data.announce_title+"</span>"
  card +=        "<div class='divider dashed'></div>"
  card +=        "<table class='card-content'>"
  card +=          "<tbody>"
  card +=            "<tr>"
  card +=              "<td width='5%'><img class='item-card' src='assets/images/icon/position.png' alt='position'></td>"
  card +=              "<td width='35%'>ตำแหน่ง</td>"
  card +=              "<td>"
  card +=              data.job_description_1[0]['position_name']
  card +=              "</td>"
  card +=            "</tr>"
  card +=            "<tr>"
  card +=              "<td><img class='item-card' src='assets/images/icon/experience.png' alt='experience'></td></td>"
  card +=              "<td>ประสบการณ์</td>"
  card +=              "<td>"
  if (data.experience == 99 ||data.experience == "") {
    card +=               "ไม่ระบุ"
  }else {
    card +=               data.experience+" ปี"
  }
  card +=              "</td>"
  card +=            "</tr>"
  card +=            "<tr>"
  card +=              "<td><img class='item-card' src='assets/images/icon/degree.png' alt='degree'></td>"
  card +=              "<td>วุฒิการศึกษา</td>"
  card +=              "<td>"
  if (data.education[0]['id'] == '01') {
    card +=               data.education[0]['name']
  }else {
    card +=               data.education[0]['name']+" "+lang.announce.more
  }
  card +=              "</td>"
  card +=            "</tr>"
  card +=            "<tr>"
  card +=              "<td><img class='item-card' src='assets/images/icon/salary.png' alt='salary'></td>"
  card +=              "<td>เงินเดือน</td>"
  card +=              "<td>"
  if (data.income_max == data.income_min) {
    card +=               lang.announce.income
  }else {
    card +=               numberWithCommas(data.income_min)+" - "+numberWithCommas(data.income_max)+" THB"
  }
  card +=              "</td>"
  card +=            "</tr>"
  card +=          "</tbody>"
  card +=        "</table>"
  card +=     "</div>"
  card +=     "<div class='card-footer'>"
  card +=       "<div class='data-content'>"
  card +=         "<span>"+"จำนวนผู้เข้าดู: "+"</span><span class='j-text count'>"+data.count_view+"</span>"
  card +=         "<span>"+"จำนวนผู้สมัคร: "+"</span><span class='j-text count'>"+data.count_apply+"</span>"
  card +=         "<span class='date pull-right'> "
  if (data.update_at == null || data.update_at == "0000-00-00 00:00:00") {
  card +=  formatDate(data.create_at)
  }else {
  card +=  formatDate(data.update_at)
  }
  card +=         "</span>"
  card +=       "</div>"
  card +=    "</div>"
  card +=  "</div>"

  return card;
}
function genCardAnnounceMe(data){
  var card = "";
  card += ""
  card +=  "<div class='col s12' onclick=\"Applicant_announce(\'"+data.announce_id+"\')\")'>"
  card +=   "<div class='card-announce-id'>"
  card +=     "<div class='card-header'>"
  card +=        data.company.company.company_name
  card +=     "</div>"
  card +=     "<div class='title-content'>"
  card +=        "<span >"+data.announce_title+"</span>"
  card +=        "<div class='divider dashed'></div>"
  card +=        "<table class='card-content'>"
  card +=          "<tbody>"
  card +=            "<tr>"
  card +=              "<td width='5%'><img class='item-card' src='../assets/images/icon/position.png' alt='position'></td>"
  card +=              "<td width='35%'>ตำแหน่ง</td>"
  card +=              "<td>"
  card +=              data.job_description_1[0]['position_name']
  card +=              "</td>"
  card +=            "</tr>"
  card +=            "<tr>"
  card +=              "<td><img class='item-card' src='../assets/images/icon/experience.png' alt='experience'></td></td>"
  card +=              "<td>ประสบการณ์</td>"
  card +=              "<td>"
  if (data.experience == 99 ||data.experience == "") {
    card +=               "ไม่ระบุ"
  }else {
    card +=               data.experience+" ปี"
  }
  card +=              "</td>"
  card +=            "</tr>"
  card +=            "<tr>"
  card +=              "<td><img class='item-card' src='../assets/images/icon/degree.png' alt='degree'></td>"
  card +=              "<td>วุฒิการศึกษา</td>"
  card +=              "<td>"
  if (data.education[0]['id'] == '01') {
    card +=               data.education[0]['name']
  }else {
    card +=               data.education[0]['name']+" "+lang.announce.more
  }
  card +=              "</td>"
  card +=            "</tr>"
  card +=            "<tr>"
  card +=              "<td><img class='item-card' src='../assets/images/icon/salary.png' alt='salary'></td>"
  card +=              "<td>เงินเดือน</td>"
  card +=              "<td>"
  if (data.income_max == data.income_min) {
    card +=               lang.announce.income
  }else {
    card +=               numberWithCommas(data.income_min)+" - "+numberWithCommas(data.income_max)+" THB"
  }
  card +=              "</td>"
  card +=            "</tr>"
  card +=          "</tbody>"
  card +=        "</table>"
  card +=     "</div>"
  card +=     "<div class='card-footer'>"
  card +=       "<div class='data-content'>"
  card +=         "<span>"+"จำนวนผู้เข้าดู: "+"</span><span class='j-text count'>"+data.count_view+"</span>"
  card +=         "<span>"+"จำนวนผู้สมัคร: "+"</span><span class='j-text count'>"+data.count_apply+"</span>"
  card +=         "<span class='date pull-right'> "
  if (data.update_at == null || data.update_at == "0000-00-00 00:00:00") {
  card +=  formatDate(data.create_at)
  }else {
  card +=  formatDate(data.update_at)
  }
  card +=         "</span>"
  card +=       "</div>"
  card +=    "</div>"
  card +=  "</div>"

  return card;
}
function genTableAnnounce(data,index) {
  var table = "";
  table += "<tr data-id='"+data.announce_id+"'>"
  table +=   "<td class='center'>"
  if (data.favorite_job) {
    table +=     "<img class='favorite-icon' src='assets/images/icon/star-solid.svg' id='favorite-y"+data.announce_id+"' onclick=\"favorite_job(\'"+data.announce_id+"\')\">"
  }else {
    table +=     "<img class='favorite-icon' src='assets/images/icon/star-regular.svg' id='favorite-n"+data.announce_id+"' onclick=\"favorite_job(\'"+data.announce_id+"\')\">"
  }
  table +=   "</td>"
  table +=   "<td class='center j-text' onclick=\"count_view(\'"+data.announce_id+"\',\'"+data.staff_id+"\')\"> "+index+"</td>"
  table +=   "<td  onclick=\"count_view(\'"+data.announce_id+"\',\'"+data.staff_id+"\')\">"+data.announce_title+"</td>"
  table +=   "<td onclick=\"count_view(\'"+data.announce_id+"\',\'"+data.staff_id+"\')\"><u class='j-text'>"+data.job_description_1[0]['position_name']+"</u></td>"
  table +=   "<td class='j-text'  onclick=\"count_view(\'"+data.announce_id+"\',\'"+data.staff_id+"\')\">"+data.company.company.company_name+"</td>"
  table +=   "<td class='j-text'  onclick=\"count_view(\'"+data.announce_id+"\',\'"+data.staff_id+"\')\">"
  if (data.income_max == data.income_min) {
    table += lang.announce.income
  }else {
    table += numberWithCommas(data.income_min)+" - "+numberWithCommas(data.income_max)+" THB"
  }
  table +=   "</td>"
  table +=   "<td class='j-text'>"+data.zone.data[0].pro_thname+"</td>"
  table +=   "<td>"
  if (data.update_at == null || data.update_at == "0000-00-00 00:00:00") {
  table +=  formatDate(data.create_at)
  }else {
  table +=  formatDate(data.update_at)
  }
  table +=   "</td>"
  table += "</tr>"
  return table;

}
function genAnnounce_table(data,index) {
  var table = "";
  table += "<tr onclick=\"Applicant_announce(\'"+data.announce_id+"\')\">"
  table +=   "<td>"
  table +=   index
  table +=   "</td>"
  table +=   "<td>"
  table +=   data.announce_title
  table +=   "</td>"
  table +=   "<td >"
  table +=     data.count_view
  table +=   "</td>"
  table += "</tr>"
return table;
}
function genAnnounce_table_mobile(data) {
  var table = "";
  table +="<div class='table-mobile'>"
  table +=  "<div class='table-mobile-header'>"
  table +=    "<div class='row reset-margin'>"
  if (data.favorite_job) {
    table +=     "<img class='favorite-icon' src='assets/images/icon/star-solid.svg' id='favorite-y"+data.announce_id+"' onclick=\"favorite_job(\'"+data.announce_id+"\')\">"
  }else {
    table +=     "<img class='favorite-icon' src='assets/images/icon/star-regular.svg' id='favorite-n"+data.announce_id+"' onclick=\"favorite_job(\'"+data.announce_id+"\')\">"
  }
  table +=     "<span  onclick=\"count_view(\'"+data.announce_id+"\',\'"+data.staff_id+"\')\">"+data.announce_title+"</span>"
  table +=    "</div>"
  table +=  "</div>"
  table +=  "<div class='table-mobile-content' onclick=\"count_view(\'"+data.announce_id+"\',\'"+data.staff_id+"\')\">"
  table +=    "<div class='row'>"
  table +=        "<span class='data-content'><i class='fas fa-crosshairs'></i>"+data.job_description_1[0]['position_name']+"</span>"
  table +=        "<span class='data-content'><i class='fas fa-building'></i>"+data.company.company.company_name+"</span>"
  table +=        "<span class='data-content'><i class='fas fa-map-marker-alt'></i>"+data.zone.data[0].pro_thname+"</span>"
  table +=        "<span class='data-content pull-right price'>"
  if (data.income_max == data.income_min) {
  table += lang.announce.income
  }else {
  table += numberWithCommas(data.income_min)+" - "+numberWithCommas(data.income_max)+" THB"
  }
  table +=        "</span>"
  table +=        "<span class='data-content date'><i class='fas fa-calendar-alt'></i>"
  if (data.update_at == null || data.update_at == "0000-00-00 00:00:00") {
  table +=  formatDate(data.create_at)
  }else {
  table +=  formatDate(data.update_at)
  }
  table +=        "</span>"
  table +=    "</div>"
  table +=  "</div>"
  table +="</div>"
  return table;
}
function genAnnounce_me_table(data,index) {
  var table = "";
  table += "<tr onclick=\"Applicant_announce(\'"+data.announce_id+"\')\">"
  table +=   "<td>"
  table +=   index
  table +=   "</td>"
  table +=   "<td>"
  table +=   data.announce_title
  table +=   "</td>"
  table +=   "<td >"
  table +=     data.count_apply
  table +=   "</td>"
  table += "</tr>"
return table;
}

// function genCardAnnounceMe(data) {
//   var card = "";
//   card += "<div class='col s12'>"
//   // card +=   "<div class='card' onclick=\"count_view(\'"+data.announce_id+"\')\" style='padding:10px'>"
//   card +=   "<div class='card' style='padding:10px'>"
//   card +=       "<div class='card-announce-id'>"
//   card +=           "<div class='row'>"
//   card +=           "<div class='col s12'>"
//   card +=             "<div class='row' style='margin-bottom: 10px;'>"+data.company.company.company_name
//   card +=             "<span class='icon fas fa-shopping-basket right' data-position='bottom' data-tooltip='Applicant' onclick=\"Applicant_announce(\'"+data.announce_id+"\')\")'></span>"
//   card +=             "<span class='icon fas fa-trash tooltipped right' data-position='bottom' data-tooltip='Trash' onclick=\"Trash_announce(\'"+data.announce_id+"\')\")'></span>"
//   card +=             "<span class='icon fas fa-edit tooltipped right' data-position='bottom' data-tooltip='Edit' onclick=\"Edit_announce(\'"+data.announce_id+"\')\"></span>"
//   card +=             "<span class='icon fas fa-eye tooltipped right' data-position='bottom' data-tooltip='View' onclick=\"View_announce(\'"+data.announce_id+"\')\"></span>"
//   card +=             "</div>"
//   card +=             data.announce_title
//   card +=             "<br><i class='fas fa-user-plus'></i> "+lang.announce.job_description_1+" : "+data.job_description_1[0]['job_description_name']
//     if (data.job_description_2.length > 0 && data.job_description_2[0]['job_description_code_2'] != '999') {
//       card +=           " ("+data.job_description_2[0]['job_description_name']+")"
//     }
//   if (data.experience == 99) {
//     card +=             "<br><i class='fas fa-briefcase'></i> "+lang.announce.experience+" : -"
//   }else {
//     card +=             "<br><i class='fas fa-briefcase'></i> "+lang.announce.experience+" : "+data.experience+" ปี"
//   }
//   card +=             "<br> <i class='fas fa-graduation-cap'></i> "+lang.announce.education+" : "+data.education[0]['name']+" "+lang.announce.more
//   if (data.income_max == data.income_min) {
//     card +=             "<br> <i class='fas fa-donate'></i> "+lang.announce.income+""
//   }else {
//     card +=             "<br> <i class='fas fa-donate'></i> "+lang.announce.income_label+" : "+numberWithCommas(data.income_min)+" - "+numberWithCommas(data.income_max)+" บาท"
//   }
//   card +=             "<div class='col s12' style='padding: 0;'>"+lang.announce.count_apply+" : "+data.count_apply+" "+lang.announce.count_view+" : "+data.count_view+"<span class='right'>"+formatDate(data.create_at)+"</span></div>"
//   card +=           "</div>"
//   card +=       "</div>"
//   card +=   "</div>"
//   card += "</div>"
// return card;
//
// }

// function genCardAnnounceMeFooter(data) {
//   var card = "";
//   card += "<div class='col s12'>"
//   // card +=   "<div class='card' onclick=\"count_view(\'"+data.announce_id+"\')\" style='padding:10px'>"
//   card +=   "<div class='card' >"
//   card +=       "<div class='card-announce-id'>"
//   card +=           "<div class='row'>"
//   card +=           "<div class='col s12'>"
//   card +=             "<div class='row' style='margin-bottom: 10px;'>"+data.company.company.company_name
//   card +=             "<span class='icon fas fa-shopping-basket tooltipped right' data-position='bottom' data-tooltip='Applicant' onclick=\"Applicant_announce(\'"+data.announce_id+"\')\")'></span>"
//   card +=             "<span class='icon fas fa-trash tooltipped right' data-position='bottom' data-tooltip='Trash' onclick=\"Trash_announce(\'"+data.announce_id+"\')\")'></span>"
//   card +=             "<span class='icon fas fa-edit tooltipped right' data-position='bottom' data-tooltip='Edit' onclick=\"Edit_announce(\'"+data.announce_id+"\')\"></span>"
//   card +=             "<span class='icon fas fa-eye tooltipped right' data-position='bottom' data-tooltip='View' onclick=\"View_announce(\'"+data.announce_id+"\')\"></span>"
//   card +=             "</div>"
//   card +=             data.announce_title
//   card +=             "<br><i class='fas fa-user-plus'></i> "+lang.announce.job_description_1+" : "+data.job_description_1[0]['job_description_name']
//     if (data.job_description_2.length > 0 && data.job_description_2[0]['job_description_code_2'] != '999') {
//       card +=           " ("+data.job_description_2[0]['job_description_name']+")"
//     }
//   if (data.experience == 99) {
//     card +=             "<br><i class='fas fa-briefcase'></i> "+lang.announce.experience+" : -"
//   }else {
//     card +=             "<br><i class='fas fa-briefcase'></i> "+lang.announce.experience+" : "+data.experience+" ปี"
//   }
//   card +=             "<br> <i class='fas fa-graduation-cap'></i> "+lang.announce.education+" : "+data.education[0]['name']+" "+lang.announce.more
//   if (data.income_max == data.income_min) {
//     card +=             "<br> <i class='fas fa-donate'></i> "+lang.announce.income+""
//   }else {
//     card +=             "<br> <i class='fas fa-donate'></i> "+lang.announce.income_label+" : "+numberWithCommas(data.income_min)+" - "+numberWithCommas(data.income_max)+" บาท"
//   }
//   card +=             "<div class='col s12' style='padding: 0;'>"+lang.announce.count_apply+" : "+data.count_apply+" "+lang.announce.count_view+" : "+data.count_view+"<span class='right'>"+formatDate(data.create_at)+"</span></div>"
//   card +=           "</div>"
//   card +=       "</div>"
//   card +=   "</div>"
//   card += "</div>"
// return card;
//
// }

function getStatusGroup(status){
         var obj = {};
         if (status == 1) {
             obj = {"profile":"in-active","enable":true};
        }else if (status == 0){
             obj = {"profile":"in-disable","enable":false};
        }else if (status == 9){
             obj = {"profile":"in-confirm","enable":false};
        }
        return obj;
}

function getApplicantView(data) {
  console.log(data);
  var li = "";
  li +="<li>"
  li +=   "<div class='collapsible-header'>"
  li +=   "<img class='img-card' src="+data.data.image+">"
  li +=   "<span style='padding-left: 10px;'>"+data.data.fname+" "+data.data.lname+"</span>"
  li +=   "<span class=''> ("+data.view+" ครั้ง)</span>"
  li +=   "</div>"
  li +=   "<div class='collapsible-body'>"
  li +=   "<span>Email : "+data.data.email+"</span>"
  li +=   "<br>"
  li +=   "<span>เบอร์โทร : "+data.data.tel+"</span>"
  li +=   "<br>"
  if ("birth_date" in data.data) {
    li +=   "<span>วัน/เดือน/ปี เกิด :"+formatDate(data.data.birth_date)+" (อายุ "+data.data.ages+" ปี)</span>"
    li +=   "<br>"
  }
  if (data.data.job_description_1 != undefined) {
    li +=   "<span>ตำแหน่งที่สมัครไว้ : "+data.data.job_description_1[0]['job_description_name']+"</span>"
    li +=   "<br>"
  }
  li += "<a onclick=\"view_applicant(\'"+data.data.uid+"\')\")'>ดูข้อมูลเพิ่มเติม >> </a>"
  li +=   "</div>"
  li += "</li>"
  return li;
}
function getApplicantApply(data) {
  console.log(data);
  var li = "";
  li +="<li>"
  li +=   "<div class='collapsible-header'>"
  li +=   "<img class='img-card' src="+data.data.image+">"
  li +=   "<span style='padding-left: 10px;'>"+data.data.fname+" "+data.data.lname+"</span></div>"
  li +=   "<div class='collapsible-body'>"
  li +=   "<span>Email : "+data.data.email+"</span>"
  li +=   "<br>"
  li +=   "<span>เบอร์โทร : "+data.data.tel+"</span>"
  li +=   "<br>"
  if ("birth_date" in data.data) {
    li +=   "<span>วัน/เดือน/ปี เกิด :"+formatDate(data.data.birth_date)+" (อายุ "+data.data.ages+" ปี)</span>"
    li +=   "<br>"
  }
  if (data.data.job_description_1 != undefined) {
    li +=   "<span>ตำแหน่งที่สมัครไว้ : "+data.data.job_description_1[0]['job_description_name']+"</span>"
    li +=   "<br>"
  }
  li += "<a onclick=\"view_applicant(\'"+data.data.uid+"\')\")'>ดูข้อมูลเพิ่มเติม >> </a>"
  li += "</div>"
  li += "</li>"
  return li;
}
function getListNone() {
  var li = "";
  li +="<li>"
  li +=   "<div class='collapsible-header'><i class='fas fa-exclamation-triangle'></i>ไม่พบข้อมูล</div>"
  li += "</li>"
  return li;
}

function genActivity_table(index,data,display){
  var table = "";
  table += "<tr class='tr-ticket' id='tr_file_"+index+"' style='display:"+display+";' data-row='"+index+"'>";
  table +=   "<td>"
  // table +=   index
  table +=   "</td>"
  table +=   "<td>"
  table +=   data.log_msg + " " + data.passive
  table +=   "</td>"
  table +=   "<td>"
  table +=     formatDateFull(data.log_date)
  table +=   "</td>"
  table += "</tr>"
return table;
}

function genCarouselList(data) {
  var row = "<a class='carousel-item'  onclick=\"count_view(\'"+data.announce_id+"\',\'"+data.staff_id+"\')\"><img src="+data.image+"></a>";
  return row;
}
