var lang = new Lang();
var Interval;
var profile_me;
function Lang (){
    var jsonLang = JSON.parse($("#jsonLang").html());
    this.error = jsonLang.error_message;
    this.data = jsonLang.data_message;
    this.announce = jsonLang.announce_message;
}

function getUrl(path){
    var config = 'JOB_UI';
    var origin = window.location.origin;
    return origin+"/"+config+"/"+path
}

function getApi(path){
    var config = 'JOB_API';
    var origin = window.location.origin;
    return origin+"/"+config+"/"+path
}

// function

function Getprofile() {
  var position = localStorage.getItem('data');
  if (position == 'applicant') {
    $(".get-profile-dropdown").css({"display":"none"});
      // applicant/getprofile
      var profile = getApi('applicant/getprofile');
      var token = localStorage.getItem('token');
      $.ajax({
        url: profile,
        type: 'GET',
        headers:{"token":token},
      })
      .done(function(data) {
          profile_me = JSON.parse(data);
          console.log(profile_me.data);
          $("#nav-search").removeClass("display-disable");
          $("#auth-editApplicant").removeClass("display-disable");
          $("#uid_log").val(profile_me.data.uid);
          $(".auth_profile").attr("src",profile_me.data.image);
          $(".name").append(profile_me.data.fname+" "+profile_me.data.lname);
          if (profile_me.data.AP_status == '9') {
            console.log(profile_me.data.AP_status);
            $("#contact-company").css({"display":""});
          }
        })
  }else {
    $(".auth-editApplicant").addClass("display-disable");
    var profile = getApi('users/getprofile');
    var token = localStorage.getItem('token');
    $.ajax({
      url: profile,
      type: 'GET',
      headers:{"token":token},
    })
    .done(function(data) {
        profile_me = JSON.parse(data);
        console.log(profile_me.data);
        $("#nav-search").addClass("display-disable");
        $(".auth_profile").attr("src",profile_me.data.image);
        $(".name").append(profile_me.data.fname+" "+profile_me.data.lname);
        $(".email").append(profile_me.data.email);
        console.log(profile_me.data.uid);
        $(".by-footer").css({"display":"none"})
        $("#uid_log").val(profile_me.data.uid);
        if (profile_me.data.level.id == 1 || profile_me.data.level.id == 2) {
          $(".administrator").css({"display":""});
          $(".announce").css({"display":""});
          $(".AnnounceMe").css({"display":""});
          $(".home").css({"display":"none"});
        }
        if (profile_me.data.level.id == 3) {
          $(".announce").css({"display":""});
          $(".AnnounceMe").css({"display":""});
          $(".home").css({"display":"none"});
        }
        if (profile_me.data.level.id == 0) {
          $(".administrator").css({"display":""});
          $(".announce").css({"display":""});
          $(".AnnounceMe").css({"display":""});
          $(".system").css({"display":""});
          $(".company").css({"display":""});
          $(".home").css({"display":"none"});
        }
    })
  }
}

function filter_Dialog() {
  console.log("filter_Dialog");
  var connectURL = getUrl("filterDialog");
  var formData = {"ajax":true};
      formData["data"] = "" ;
  $.ajax({
          url: connectURL,
          type: 'POST',
          data: formData
        }).done(function(data) {
          closeloader()
          var obj_modal = JSON.parse(data);
          $(".modal-area").append(obj_modal["modal"]);
          $("#filterDialog").Modal({"backdrop":true,"static":true});
          ms_job_type_get();
          ms_job_description_1_get();
          ms_zone_get();
          ms_education_get();
          ms_company_name_get();
          ms_position();
          $("#job_description_1").change(function() {
            ms_job_description_2_get($("#job_description_1").val());
            $("#job_description_2").removeAttr("disabled","");
            })

          $("#filter_search").click(function() {
            getannounce_search($('#type_job').val(),$('#job_description_1').val(),$('#job_description_2').val(),$('#zone').val(),$('#education').val(),$('#company_name').val())
          })

          ms_province_get('')
          $("#search_province").change(function() {
            ms_aumphur_get($("#search_province").val(),'');
          })
          $("#search_aumphur").change(function() {
            ms_tumbon_get($("#search_province").val(),$("#search_aumphur").val(),'')
          })

        })

}

function filter() {
  console.log("filter");
  log_user('3','','',$('#type_job').val(),$('#job_position').val(),'',
  $('#zone').val(),$('#education').val(),$('#company_name').val(),'');
  $(".closemodal").trigger('click');
  getannounce_search($('#type_job').val(),$('#job_position').val(),'',$('#zone').val(),$('#education').val(),$('#company_name').val())
}

function getannounce_search(type_job,job_position,job_description_2,zone,education,company_name) {
  console.log("getannounce_search");
  // log_user('3','','',$('#type_job').val(),$('#job_position').val(),$('#job_description_2').val(),
  // $('#zone').val(),$('#education').val(),$('#company_name').val(),'');
  $("#table_content").html('')
  $("#table_content_mobile").html('')
  var token = localStorage.getItem('token');
  var api_url = getApi('getannounce_search');
  var formData = {
                    "type_job":$('#type_job').val(),
                    "job_description_1":$('#job_position').val(),
                    "job_description_2":'',
                    "education":$('#education').val(),
                    "zone":$('#search_province').val(),
                    "zone_detail":$('#search_aumphur').val(),
                    "company_name":$('#company_name').val(),
                    "offset":'0'
                  };
  $.ajax({
    url: api_url,
    type: 'GET',
    headers:{"token":token},
    data:formData,
    beforeSend:function(){
      preloader();
    }
  })
  .done(function(data) {
      closeloader()
      $(".closemodal").trigger('click');
      var obj = JSON.parse(data);
      console.log(obj);
      if (obj.status == 200) {
        if (obj.count > 0) {
        for (var i = 0; i < obj.data.length; i++) {
            var row = genTableAnnounce(obj.data[i],(i+1));
            $('.carousel-hidden').trigger('click');
            $('.drag-target').trigger('click');
            $("#table_content").append(row)
            var row_mobile = genAnnounce_table_mobile(obj.data[i]);
            $("#table_content_mobile").append(row_mobile)
          }
        }else {
          console.log("ไม่พบข้อมูล");
          $('.carousel-hidden').trigger('click');
          $("#table_content").append("<tr><td colspan='8'>ไม่พบข้อมูล</td></tr>");
          $("#table_content_mobile").append("<div class='table-mobile-header'><div class='row reset-magin'><span><h5 class='center'>ไม่พบข้อมูล</h5></span></div></div>");
        }
    }

  })

}

function getCountAnnounce() {
  var authtoken = localStorage.getItem('token');
  if(authtoken == null){
   formData  = {"token":""}
  }else {
   formData  = {"token":authtoken}
  }
  $.ajax({
    url: getApi('getcountannounce'),
    type: 'POST'
  })
  .done(function(data) {
    console.log(data);
    var obj = JSON.parse(data);
    $(".advert_count_all").text(obj.count)
  })

  $.ajax({
    url: getApi('getcountannouncefavorite'),
    type: 'POST',
    data:formData
  })
  .done(function(data) {
    console.log(data);
    var obj = JSON.parse(data);
    $(".advert_count_favorite").text(obj.count)
  })
}

//check type
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function isType(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
    case 'jpg':
    case 'gif':
    case 'bmp':
    case 'png':
    case 'pdf':
    case '':
        //etc
        return true;
    }
    return false;
}

// date
function formatDate(date) {
    var d = new Date(date);
        month = '' + (d.getMonth() + 1);
        day = '' + d.getDate();
        year = d.getFullYear()+543;
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join('/');
}
function formatDateFull(date) {
  var datetime = new Date(date);
  var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
  var hours = datetime.getHours();
      hours = hours < 10 ? '0'+hours : hours;
  var minutes = datetime.getMinutes();
      minutes = minutes < 10 ? '0'+minutes : minutes;
  var day = datetime.getDate();
  var monthIndex = datetime.getMonth();
  var year = datetime.getFullYear()+543;
  var times = hours+':'+minutes
  return day + ' ' + monthNames[monthIndex] + ' ' + year+' '+times;
}

function formatDateToYmd(date) {
  var newdate = date.split("/").reverse().join("-");
  return newdate;
}
function formatDateToDmy(date){
var p = date.split(/\D/g)
return [p[2],p[1],p[0] ].join("/")
}

// nl2br
function nl2br (str, is_xhtml) {
     var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
     return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}
// numberWithCommas
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
//
function phonenumber(phonenumber) {
  var regExp = /^0[0-9]{8,9}$/i;
  return regExp.test(phonenumber);
}

function number13(num) {
  var regExp = /^[0-9]{13}$/i;
  return regExp.test(num);
}

function checkID(id)
{
// //ตัดข้อความ - ออก
// var str = $("#citizen").val();
// var regex = /-/gi;
// var res = str.replace(regex, "");
if(id.length != 13) return false;
for(i=0, sum=0; i < 12; i++)
sum += parseFloat(id.charAt(i))*(13-i); if((11-sum%11)%10!=parseFloat(id.charAt(12)))
return false; return true;
}

function chkpassword(password) {
  var regExp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!"#$%&'()*+,-.\/:;<=>?\\@[\]^_`{|}~]).{8,64}$/;
  return regExp.test(password);
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function autoTab(obj) {
    /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย
    หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น  รูปแบบเลขที่บัตรประชาชน
    4-2215-54125-6-12 ก็สามารถกำหนดเป็น  _-____-_____-_-__
    รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____
    หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__
    ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบเลขบัตรประชาชน
    */
        var pattern=new String("_-____-_____-__-_"); // กำหนดรูปแบบในนี้
        var pattern_ex=new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้
        var returnText=new String("");
        var obj_l=obj.value.length;
        var obj_l2=obj_l-1;
        for(i=0;i<pattern.length;i++){
            if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){
                returnText+=obj.value+pattern_ex;
                obj.value=returnText;
            }
        }
        if(obj_l>=pattern.length){
            obj.value=obj.value.substr(0,pattern.length);
        }
}

// validate
function addErr(type,msg) {
  $('#'+type).addClass("invalid");
  $('#label'+type).addClass("error");
  $('#label'+type).html(msg);
}
function rmErr(type) {
  $('#'+type).removeClass("invalid");
  $('#label'+type).removeClass("error");
  $('#label'+type).html('');
}
function addErrSelect(type,msg) {
  $('#'+type).parent('.select-wrapper').parent('.select-dropdown').addClass("error-select");
  $('#label'+type).addClass("error");
  $('#label'+type).html(msg);
}
function rmErrSelect(type) {
  $('#'+type).parent('.select-wrapper').parent('.select-dropdown').removeClass("error-select");
  $('#label'+type).removeClass("error");
  $('#label'+type).html('');
}

$.fn.validate_err = function (msg){
    var id = $(this)[0].id;
    $("#"+id+"_group").addClass('error');
    $("#"+id+"_feedback").addClass('error').html(msg);
    $(this).blur(function(event) {
      $("#"+id+"_group").removeClass('error');
      $("#"+id+"_feedback").removeClass('error').html("");
    });
}

$.fn.validateWarningBlank = function (){
    var input = $(this);
    for (var i = 0; i < input.length; i++) {
         if (input[i].value == "") {
              var id = input[i].id;
              $("#"+id+"_group").addClass('warning');
              $("#"+id+"_feedback").html("<span class='fa fa-exclamation-triangle'></span> "+lang.error.input_blank).addClass('warning');
              $("#"+id+"").blur(function(event) {
              $("#"+id+"_group").removeClass('warning');
              $("#"+id+"_feedback").html("").removeClass('warning');
              });
              return false;
         }
    }
    return true;
  }

$.fn.validateWarning = function(msg){
    $(this).parent('.select-wrapper').parent('.input-field').addClass('warning');
    $(this).parent('.select-wrapper').parent('.input-field').children('.feedback').html(msg);

    $(this).parent('.input-field').addClass('warning');
    $(this).parent('.input-field').children('.feedback').html(msg);
    $(this).focus();
}

function onremove_validate(el){
   if ($(el).parent('.input-field').hasClass('error')) {
       $(el).parent('.input-field').removeClass('error');
       $(el).parent('.input-field').children('.feedback').html("");
   }
   $(el).parent('.select-wrapper').parent('.input-field').removeClass('error');
   $(el).parent('.select-wrapper').parent('.input-field').children('.feedback').html("");

   if ($(el).parent('.input-field').hasClass('warning')) {
       $(el).parent('.input-field').removeClass('warning');
       $(el).parent('.input-field').children('.feedback').html("");

   }
   $(el).parent('.select-wrapper').parent('.input-field').removeClass('warning');
   $(el).parent('.select-wrapper').parent('.input-field').children('.feedback').html("");
}

// loader
function preloader() {
  $(".loader").show();
  $(".preload-backdrop").css({"display":"block"});
  Interval = setInterval(function(){hour()},2500);
}
function closeloader() {
  $(".loader").hide();
  $(".preload-backdrop").css({"display":"none"})
  clearInterval(Interval);
}
function hour(){
    setTimeout(function(){
          // $(".page-load").removeClass('fa-hourglass-start').addClass('fa-hourglass-half').css({"transform":"rotate(0deg"});
          $(".point-wait").html("..");
    },500);
    setTimeout(function(){
          // $(".page-load").removeClass('fa-hourglass-half').addClass('fa-hourglass-end');
          $(".point-wait").html("...");
    },1000);
    setTimeout(function(){
          // $(".page-load").removeClass('fa-hourglass-end').css({"transform":"rotate(360deg"});
          $(".point-wait").html("");
    },1500);
    setTimeout(function(){
          // $(".page-load").addClass('fa-hourglass-start');
          $(".point-wait").html(".");
          // .addClass('fa-hourglass-start');
    },2000);
  }

// var formData = new FormData(this);
// console.log(formData);
// for (var value of formData.values()) {
//     console.log(value);
// }

// toast
// Materialize.toast("<span class='success fas fa-check-circle'></span> &nbsp;"+lang.error.request_success, 40000);
// Materialize.toast("<span class='warning fas fa-exclamation-circle'></span> &nbsp;"+lang.error.request_warning, 40000);
// Materialize.toast("<span class='error fas fa-times-circle'></span> &nbsp;"+lang.error.request_error, 40000);
